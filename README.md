# What is this?!
Hey there! I'm Spyeedy, the creator and maintainer of this project, Idk Mod. It's a mod where I add whatever things I want to do, and to test any experimental features I wish to test first before it goes into any mods I'm developing for a release.

In summary, this project is basically R&D and whatever the heck I want to do!

## Can you use my codes?
Yes! You can use my code! This project is open source for a reason! Anyone can look and take my code. It's public ¯\\\_(ツ)_/¯ Also, I'd really love if you DMed me or something that you used my code for your projects/mods! I would :heart: to see how my codes were used in your creations!
