package com.spyeedy.idkmod.common.utils;

import net.minecraft.util.Direction;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;

public class BlockShapesHelper {

	public static VoxelShape combineShape(VoxelShape shape1, VoxelShape shape2) {
		return VoxelShapes.or(shape1, shape2);
	}

	/**
	 * @facing Default is {@link Direction#NORTH}
	 */
	public static VoxelShape rotateShape(double x1, double y1, double z1, double x2, double y2, double z2, Direction facing) {
		if (facing == Direction.NORTH) {
			return VoxelShapes.box(x1, y1, z1, x2, y2, z2);
		} else if (facing == Direction.SOUTH) {
			double realX1 = 1 - x2;
			double realX2 = 1 - x1;
			double realZ1 = 1 - z2;
			double realZ2 = 1 - z1;

			return VoxelShapes.box(realX1, y1, realZ1, realX2, y2, realZ2);
		} else if (facing == Direction.WEST) {
			double realZ1 = 1 - x2;
			double realZ2 = 1 - x1;

			return VoxelShapes.box(z1, y1, realZ1, z2, y2, realZ2);
		} else {
			double realX1 = 1 - z2;
			double realX2 = 1 - z1;

			return VoxelShapes.box(realX1, y1, x1, realX2, y2, x2);
		}
	}
}