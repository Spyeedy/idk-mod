package com.spyeedy.idkmod.common.utils;

import net.minecraft.client.renderer.model.*;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.client.renderer.vertex.VertexFormatElement;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.model.SimpleModelTransform;
import net.minecraftforge.client.model.pipeline.BakedQuadBuilder;

public class BakedQuadHelper {

	public static BakedQuad makeQuad(FaceBakery faceBakery, Direction face, float[] coords, float[] dimensions, float[] uv, ResourceLocation blockTexRL) {
		return makeQuad(faceBakery, face, coords, dimensions, uv, ModelLoader.instance().getSpriteMap().getAtlas(AtlasTexture.LOCATION_BLOCKS).getSprite(blockTexRL));
	}

	/**
	 *
	 * @param faceBakery
	 * @param face The side to make the quad on
	 * @param coords x,y,z coordinates
	 * @param dimensions x,y,z lengths
	 * @param uv Minimum and Maximum coordinates. (minU, minV, maxU, maxV)
	 * @param textureSprite
	 * @return
	 */
	public static BakedQuad makeQuad(FaceBakery faceBakery, Direction face, float[] coords, float[] dimensions, float[] uv, TextureAtlasSprite textureSprite) {
		Vector3f from = new Vector3f(coords[0], coords[1], coords[2]);
		Vector3f to = new Vector3f(from.x() + dimensions[0], from.y() + dimensions[1], from.z() + dimensions[2]);

		final int ROTATION_NONE = 0;
		BlockFaceUV blockFaceUV = new BlockFaceUV(uv, ROTATION_NONE);

		final Direction NO_FACE_CULLING = null;
		final int TINT_INDEX = -1;
		final String DUMMY_TEXTURE_NAME = ""; // texture name only for loading from json files
		BlockPartFace blockPartFace = new BlockPartFace(NO_FACE_CULLING, TINT_INDEX, DUMMY_TEXTURE_NAME, blockFaceUV);

		final IModelTransform NO_TRANSFORMATION = SimpleModelTransform.IDENTITY;
		final BlockPartRotation DEFAULT_ROTATION = null;
		final boolean APPLY_SHADING = true;
		final ResourceLocation DUMMY_RL = new ResourceLocation("dummy_name"); // for error msg

		return faceBakery.bakeQuad(from, to, blockPartFace, textureSprite, face, NO_TRANSFORMATION, DEFAULT_ROTATION, APPLY_SHADING, DUMMY_RL);
	}

	/**
	 * Can only be called 4 times, before calling BakedQuadBuilder#build, to end the vertex sequence for the BakedQuad
	 * @param builder
	 * @param texture
	 * @param drawingFace
	 * @param x
	 * @param y
	 * @param z
	 * @param u x-coordinate on texture, maximum value, 16 pixels
	 * @param v y-coordinate on texture, maximum value, 16 pixels
	 */
	public static void addVertex(BakedQuadBuilder builder, TextureAtlasSprite texture, Direction orientation, Direction drawingFace, float x, float y, float z, float u, float v) {
		float[] rotatedCoord = rotate(orientation, x, y, z);
		x = rotatedCoord[0];
		y = rotatedCoord[1];
		z = rotatedCoord[2];
		VertexFormat format = builder.getVertexFormat();
		for (int e = 0; e < format.getElements().size(); e++) {
			VertexFormatElement element = format.getElements().get(e);

			switch (format.getElements().get(e).getUsage()) {
				case POSITION:
					builder.put(e, x, y, z, 1f);
					break;
				case COLOR:
					builder.put(e, 1f, 1f, 1f, 1f);
					break;
				case NORMAL:
					builder.put(e, drawingFace.getStepX(), drawingFace.getStepY(), drawingFace.getStepZ(), 0f);
					break;
				case UV:
					if (element.getIndex() == 0) {
						builder.put(e, texture.getU(u), texture.getV(v), 0f, 1f);
						break;
					} else if (element.getIndex() == 2) {
						builder.put(e, (short)0, (short)0, 0, 1);
						break;
					}
				default:
					builder.put(e);
					break;
			}
		}
	}

	private static float[] rotate(Direction side, float x, float y, float z) {
		if (side != null) {
			switch (side) {
				case SOUTH:
					z = 1 - z;
					x = 1 - x;
					break;
				case WEST:
					float oldZ = z;
					z = 1 - x;
					x = oldZ;
					break;
				case EAST:
					z = x;
					x = 1 - x;
					break;
				default:
					break;
			}
		}
		return new float[]{x, y, z};
	}
}
