package com.spyeedy.idkmod.common.block;

import com.spyeedy.idkmod.common.tileentity.ReactorCoreTileEntity;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;

import javax.annotation.Nullable;

public class ReactorCoreBlock extends Block {

	public ReactorCoreBlock() {
		super(Properties.of(Material.METAL).noOcclusion());
		this.setRegistryName("reactor_core");
	}

	@Override
	public boolean hasTileEntity(BlockState state) {
		return true;
	}

	@Nullable
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world) {
		return new ReactorCoreTileEntity();
	}

	@Override
	public BlockRenderType getRenderShape(BlockState state) {
		return BlockRenderType.MODEL;
	}
}
