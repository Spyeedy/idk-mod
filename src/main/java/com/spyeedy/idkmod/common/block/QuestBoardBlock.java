package com.spyeedy.idkmod.common.block;

import com.spyeedy.idkmod.common.network.OpenScreenMessage;
import com.spyeedy.idkmod.common.network.PacketHandler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.EnumProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.PacketDistributor;

import javax.annotation.Nullable;

public class QuestBoardBlock extends Block {

	private static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;
	public static final EnumProperty<Placement> PLACEMENT = EnumProperty.create("placement", Placement.class);

	public QuestBoardBlock() {
		super(Properties.of(Material.METAL).harvestLevel(3).noOcclusion());
		this.setRegistryName("quest_board");
		this.registerDefaultState(this.getStateDefinition().any().setValue(FACING, Direction.NORTH).setValue(PLACEMENT, Placement.BOTTOM_LEFT));
	}

	@Override
	protected void createBlockStateDefinition(StateContainer.Builder<Block, BlockState> builder) {
		builder.add(FACING, PLACEMENT);
	}

	@Nullable
	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		return this.defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite());
	}

	@Override
	public BlockState mirror(BlockState state, Mirror mirrorIn) {
		return state.rotate(mirrorIn.getRotation(state.getValue(FACING)));
	}

	@Override
	public BlockState rotate(BlockState state, Rotation rot) {
		return state.setValue(FACING, rot.rotate(state.getValue(FACING)));
	}

	@Override
	public ActionResultType use(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		if (!worldIn.isClientSide) // checks if on client, sends packet to server.
			if (player instanceof ServerPlayerEntity) {
				PacketHandler.NETWORK.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity) player), new OpenScreenMessage(OpenScreenMessage.GuiIDs.QUEST_BOARD));
				return ActionResultType.SUCCESS;
			}
		return super.use(state, worldIn, pos, player, handIn, hit);
	}

	@Override
	public void setPlacedBy(World worldIn, BlockPos pos, BlockState state, @Nullable LivingEntity placer, ItemStack stack) {
		Direction playerFacing = placer.getMotionDirection();
		worldIn.setBlockAndUpdate(pos.above(), state.setValue(PLACEMENT, Placement.TOP_LEFT));
		worldIn.setBlockAndUpdate(pos.relative(playerFacing.getClockWise()), state.setValue(PLACEMENT, Placement.BOTTOM_RIGHT));
		worldIn.setBlockAndUpdate(pos.relative(playerFacing.getClockWise()).above(), state.setValue(PLACEMENT, Placement.TOP_RIGHT));
	}

	@Override
	public void playerWillDestroy(World worldIn, BlockPos pos, BlockState state, PlayerEntity player) {
		super.playerWillDestroy(worldIn, pos, state, player);

		Placement placement = worldIn.getBlockState(pos).getValue(PLACEMENT);
		Direction worldFacing = worldIn.getBlockState(pos).getValue(FACING).getOpposite(); // we rely on block's facing to get world facing, cannot rely on player facing
		BlockState airState = Blocks.AIR.defaultBlockState();

		switch(placement) {
			case TOP_LEFT:
				worldIn.setBlockAndUpdate(pos.below(), airState); // bottom left
				worldIn.setBlockAndUpdate(pos.relative(worldFacing.getClockWise()), airState); // top right
				worldIn.setBlockAndUpdate(pos.relative(worldFacing.getClockWise()).below(), airState); // bottom right
				break;
			case BOTTOM_LEFT:
				worldIn.setBlockAndUpdate(pos.above(), airState); // top left
				worldIn.setBlockAndUpdate(pos.relative(worldFacing.getClockWise()), airState); // bottom right
				worldIn.setBlockAndUpdate(pos.relative(worldFacing.getClockWise()).above(), airState); // top right
				break;
			case TOP_RIGHT:
				worldIn.setBlockAndUpdate(pos.below(), airState); // bottom right
				worldIn.setBlockAndUpdate(pos.relative(worldFacing.getCounterClockWise()), airState); // top left
				worldIn.setBlockAndUpdate(pos.relative(worldFacing.getCounterClockWise()).below(), airState); // bottom left
				break;
			case BOTTOM_RIGHT:
				worldIn.setBlockAndUpdate(pos.above(), airState); // top right
				worldIn.setBlockAndUpdate(pos.relative(worldFacing.getCounterClockWise()), airState); // bottom left
				worldIn.setBlockAndUpdate(pos.relative(worldFacing.getCounterClockWise()).above(), airState); // top left
				break;
		}
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		Direction facing = state.getValue(FACING);
		Placement placement = state.getValue(PLACEMENT);

		double i = 0.0625; // 1 divide by 16.

		VoxelShape shape = null;
		if (facing == Direction.NORTH) {
			switch (placement) {
				case TOP_LEFT:
					shape = VoxelShapes.box(10 * i, 0, 7 * i, 12 * i, 12 * i, 9 * i); // pillar on the left
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(0, 0, 0.5, 10 * i, 12 * i, 0.5 + i), IBooleanFunction.OR); // back bone
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(0, 10 * i, 7 * i, 10 * i, 12 * i, 0.5), IBooleanFunction.OR); // top cuboid ledge
					break;
				case BOTTOM_LEFT:
					shape = VoxelShapes.box(10 * i, 0, 7 * i, 12 * i, 1, 9 * i);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(0, 13 * i, 0.5, 10 * i, 1, 0.5 + i), IBooleanFunction.OR);
					break;
				case TOP_RIGHT:
					shape = VoxelShapes.box(4 * i, 0, 7 * i, 6 * i, 12 * i, 9 * i);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(6 * i, 0, 0.5, 1, 12 * i, 0.5 + i), IBooleanFunction.OR);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(6 * i, 10 * i, 7 * i, 1, 12 * i, 0.5), IBooleanFunction.OR);
					break;
				case BOTTOM_RIGHT:
					shape = VoxelShapes.box(4 * i, 0, 7 * i, 6 * i, 1, 9 * i);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(6 * i, 13 * i, 0.5, 1, 1, 0.5 + i), IBooleanFunction.OR);
					break;
			}
		} else if (facing == Direction.SOUTH) {
			switch (placement) {
				case TOP_LEFT:
					shape = VoxelShapes.box(4 * i, 0, 7 * i, 6 * i, 12 * i, 9 * i);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(6 * i, 0, 7 * i, 1, 12 * i, 0.5), IBooleanFunction.OR);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(6 * i, 10 * i, 0.5, 1, 12 * i, 0.5 + i), IBooleanFunction.OR);
					break;
				case BOTTOM_LEFT:
					shape = VoxelShapes.box(4 * i, 0, 7 * i, 6 * i, 1, 9 * i);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(6 * i, 13 * i, 7 * i, 1, 1, 0.5), IBooleanFunction.OR);
					break;
				case TOP_RIGHT:
					shape = VoxelShapes.box(10 * i, 0, 7 * i, 12 * i, 12 * i, 9 * i);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(0, 0, 7 * i, 10 * i, 12 * i, 0.5), IBooleanFunction.OR);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(0, 10 * i, 0.5, 10 * i, 12 * i, 0.5 + i), IBooleanFunction.OR);
					break;
				case BOTTOM_RIGHT:
					shape = VoxelShapes.box(10 * i, 0, 7 * i, 12 * i, 1, 9 * i);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(0, 13 * i, 7 * i, 10 * i, 1, 0.5), IBooleanFunction.OR);
					break;
			}

		} else if (facing == Direction.EAST) {
			switch (placement) {
				case TOP_LEFT:
					shape = VoxelShapes.box(7 * i, 0, 10 * i, 9 * i, 12 * i, 12 * i);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(7 * i, 0, 0, 0.5, 12 * i, 10 * i), IBooleanFunction.OR);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(8 * i, 10 * i, 0, 0.5 + i, 12 * i, 10 * i), IBooleanFunction.OR);
					break;
				case BOTTOM_LEFT:
					shape = VoxelShapes.box(7 * i, 0, 10 * i, 9 * i, 1, 12 * i);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(7 * i, 13 * i, 0, 0.5, 1, 10 * i), IBooleanFunction.OR);
					break;
				case TOP_RIGHT:
					shape = VoxelShapes.box(7 * i, 0, 4 * i, 9 * i, 12 * i, 6 * i);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(7 * i, 0, 6 * i, 0.5, 12 * i, 1), IBooleanFunction.OR);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(8 * i, 10 * i, 6 * i, 0.5 + i, 12 * i, 1), IBooleanFunction.OR);
					break;
				case BOTTOM_RIGHT:
					shape = VoxelShapes.box(7 * i, 0, 4 * i, 9 * i, 1, 6 * i);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(7 * i, 13 * i, 6 * i, 0.5, 1, 1), IBooleanFunction.OR);
					break;
			}
		} else {
			switch (placement) {
				case TOP_LEFT:
					shape = VoxelShapes.box(7 * i, 0, 4 * i, 9 * i, 12 * i, 6 * i);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(0.5, 0, 6 * i, 0.5 + i, 12 * i, 1), IBooleanFunction.OR);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(7 * i, 10 * i, 6 * i, 0.5, 12 * i, 1), IBooleanFunction.OR);
					break;
				case BOTTOM_LEFT:
					shape = VoxelShapes.box(7 * i, 0, 4 * i, 9 * i, 1, 6 * i);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(0.5, 13 * i, 6 * i, 0.5 + i, 1, 1), IBooleanFunction.OR);
					break;
				case TOP_RIGHT:
					shape = VoxelShapes.box(7 * i, 0, 10 * i, 9 * i, 12 * i, 12 * i);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(0.5, 0, 0, 0.5 + i, 12 * i, 10 * i), IBooleanFunction.OR);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(7 * i, 10 * i, 0, 0.5, 12 * i, 10 * i), IBooleanFunction.OR);
					break;
				case BOTTOM_RIGHT:
					shape = VoxelShapes.box(7 * i, 0, 10 * i, 9 * i, 1, 12 * i);
					shape = VoxelShapes.joinUnoptimized(shape, VoxelShapes.box(0.5, 13 * i, 0, 0.5 + i, 1, 10 * i), IBooleanFunction.OR);
					break;
			}
		}
		return shape != null ? shape : super.getShape(state, worldIn, pos, context);
	}

	public enum Placement implements IStringSerializable {
		TOP_LEFT("top_left"), BOTTOM_LEFT("bot_left"), TOP_RIGHT("top_right"), BOTTOM_RIGHT("bot_right");

		private String modelSuffix;

		Placement(String modelSuffix) {
			this.modelSuffix = modelSuffix;
		}

		@Override
		public String getSerializedName() {
			return toString().toLowerCase();
		}

		public String getModelSuffix() {
			return modelSuffix;
		}
	}
}