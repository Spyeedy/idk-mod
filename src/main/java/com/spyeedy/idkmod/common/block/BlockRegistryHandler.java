package com.spyeedy.idkmod.common.block;

import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.item.QuestBoardBlockItem;
import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = IdkMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class BlockRegistryHandler {

	public static final Block DNA_SEQUENCER_BLOCK = new DNASequencerBlock();
	public static final Block QUEST_BOARD_BLOCK = new QuestBoardBlock();
	public static final Block REACTOR_CORE_BLOCK = new ReactorCoreBlock();
	public static final Block CLIENT_DATA_BLOCK = new ClientDataBlock();

	@SubscribeEvent
	public static void registerBlock(RegistryEvent.Register<Block> e) {
		e.getRegistry().registerAll(DNA_SEQUENCER_BLOCK, QUEST_BOARD_BLOCK, REACTOR_CORE_BLOCK, CLIENT_DATA_BLOCK);
	}

	@SubscribeEvent
	public static void registerItemBlock(RegistryEvent.Register<Item> e) {
		e.getRegistry().registerAll(createBlockItem(DNA_SEQUENCER_BLOCK), new QuestBoardBlockItem(), createBlockItem(REACTOR_CORE_BLOCK), createBlockItem(CLIENT_DATA_BLOCK));
	}

	private static Item createBlockItem(Block block) {
		Item.Properties itemProps = new Item.Properties();

		itemProps.tab(IdkMod.GROUP_IDKMOD);

		return new BlockItem(block, itemProps).setRegistryName(block.getRegistryName());
	}
}