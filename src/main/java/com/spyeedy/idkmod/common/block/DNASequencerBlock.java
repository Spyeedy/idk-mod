package com.spyeedy.idkmod.common.block;

import com.spyeedy.idkmod.client.ClientUtils;
import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.container.DNASequencerContainer;
import com.spyeedy.idkmod.common.network.OpenScreenMessage;
import com.spyeedy.idkmod.common.tileentity.DNASequencerTileEntity;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

import javax.annotation.Nullable;

public class DNASequencerBlock extends Block {

	private static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;

	public DNASequencerBlock() {
		super(Properties.of(Material.METAL).harvestLevel(3).noOcclusion());
		this.setRegistryName("dna_sequencer");
		this.registerDefaultState(this.getStateDefinition().any().setValue(FACING, Direction.NORTH));
	}

	@Override
	protected void createBlockStateDefinition(StateContainer.Builder<Block, BlockState> builder) {
		builder.add(FACING);
	}

	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		return this.defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite());
	}

	@Override
	public BlockState mirror(BlockState state, Mirror mirrorIn) {
		return state.rotate(mirrorIn.getRotation(state.getValue(FACING)));
	}

	@Override
	public BlockState rotate(BlockState state, Rotation rot) {
		return state.setValue(FACING, rot.rotate(state.getValue(FACING)));
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		float specialNo = 1/16f;
		return VoxelShapes.box(1 * specialNo, 0, 1 * specialNo, 15 * specialNo, 1, 15 * specialNo);
	}

	@Override
	public ActionResultType use(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		Direction blockFacing = state.getBlockState().getValue(FACING);
		if (!worldIn.isClientSide) {
			TileEntity te = worldIn.getBlockEntity(pos);
			if (blockFacing == hit.getDirection()) {
				if (te instanceof DNASequencerTileEntity) {
					INamedContainerProvider containerProvider = new INamedContainerProvider() {
						@Override
						public ITextComponent getDisplayName() {
							return new TranslationTextComponent("screen." + IdkMod.MODID + ".dna_sequencer");
						}

						@Override
						public Container createMenu(int i, PlayerInventory playerInventory, PlayerEntity playerEntity) {
							return new DNASequencerContainer(i, worldIn, pos, playerInventory);
						}
					};
					NetworkHooks.openGui((ServerPlayerEntity) player, containerProvider, pos);
				}
			}
		} else {
			 if (blockFacing.getClockWise() == hit.getDirection()) { // left side of placed block
				ClientUtils.openGui(OpenScreenMessage.GuiIDs.DRAGGABLE_ITEM);
			} else if (blockFacing.getCounterClockWise() == hit.getDirection()) { // right side of placed block
				ClientUtils.openGui(OpenScreenMessage.GuiIDs.QUEST_MAKER);
			}
		}
		return super.use(state, worldIn, pos, player, handIn, hit);
	}

	@Override
	public boolean hasTileEntity(BlockState state) {
		return true;
	}

	@Nullable
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world) {
		return new DNASequencerTileEntity();
	}
}
