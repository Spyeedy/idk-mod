package com.spyeedy.idkmod.common;

import com.spyeedy.idkmod.client.ClientHandler;
import com.spyeedy.idkmod.common.block.BlockRegistryHandler;
import com.spyeedy.idkmod.common.capability.CapabilitiesRegistryHandler;
import com.spyeedy.idkmod.common.capability.journal.QuestReader;
import com.spyeedy.idkmod.common.handler.QuestEventHandler;
import com.spyeedy.idkmod.common.network.PacketHandler;
import com.spyeedy.idkmod.common.tileentity.ClientDataTileEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(IdkMod.MODID)
public class IdkMod {

	public static final String MODID = "idkmod";

	public static ItemGroup GROUP_IDKMOD = new ItemGroup(MODID) {
		@Override
		public ItemStack makeIcon() {
			return new ItemStack(Item.byBlock(BlockRegistryHandler.DNA_SEQUENCER_BLOCK));
		}
	};

	// Directly reference a log4j logger.
	public static final Logger LOGGER = LogManager.getLogger();

	public IdkMod() {
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::doClientStuff);

		MinecraftForge.EVENT_BUS.register(new QuestEventHandler());
		MinecraftForge.EVENT_BUS.addListener(ClientDataTileEntity::onEntityJoinWorld);
		DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> {
			MinecraftForge.EVENT_BUS.register(new ClientHandler());
			FMLJavaModLoadingContext.get().getModEventBus().addListener(ClientHandler::onModelBake);
		});

		PacketHandler.registerPackets();
	}

	private void setup(final FMLCommonSetupEvent event) {
		CapabilitiesRegistryHandler.registerCapabilities();
		QuestReader.readFiles();
	}

	private void doClientStuff(final FMLClientSetupEvent event) {
	}
}
