package com.spyeedy.idkmod.common.threedata;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.spyeedy.idkmod.common.capability.chakra.Jutsu;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.util.JSONUtils;
import net.minecraftforge.common.util.Constants;
import net.threetag.threecore.util.threedata.ThreeData;

public class JutsuArrayThreeData extends ThreeData<Jutsu[]> {

	public JutsuArrayThreeData(String key) {
		super(key);
	}

	@Override
	public Jutsu[] parseValue(JsonObject jsonObject, Jutsu[] defaultValue) {
		if (!JSONUtils.isValidNode(jsonObject, this.jsonKey))
			return defaultValue;

		JsonElement element = jsonObject.get(this.jsonKey);

		if (element.isJsonObject())
			return new Jutsu[]{ Jutsu.deserializeFromJson(element.getAsJsonObject()) };
		else {
			JsonArray jsonArray = JSONUtils.getAsJsonArray(jsonObject, this.jsonKey);
			Jutsu[] array = new Jutsu[jsonArray.size()];
			for (int i = 0; i < jsonArray.size(); i++) {
				array[i] = Jutsu.deserializeFromJson(jsonArray.get(i).getAsJsonObject());
			}
			return array;
		}
	}

	@Override
	public void writeToNBT(CompoundNBT nbt, Jutsu[] value) {
		ListNBT list = new ListNBT();
		for (Jutsu jutsu : value) {
			list.add(Jutsu.writeToNBT(jutsu));
		}
		nbt.put(this.key, list);
	}

	@Override
	public Jutsu[] readFromNBT(CompoundNBT nbt, Jutsu[] defaultValue) {
		if (!nbt.contains(key)) return defaultValue;

		ListNBT listNBT = nbt.getList(this.key, Constants.NBT.TAG_COMPOUND);
		Jutsu[] array = new Jutsu[listNBT.size()];
		for (int i = 0; i < listNBT.size(); i++) {
			array[i] = Jutsu.readFromNBT(listNBT.getCompound(i));
		}

		return array;
	}

	@Override
	public JsonElement serializeJson(Jutsu[] value) {
		if (value.length == 1) return Jutsu.serializeJson(value[0]);

		JsonArray jsonArray = new JsonArray();
		for (Jutsu jutsu : value)
			jsonArray.add(Jutsu.serializeJson(jutsu));
		return jsonArray;
	}
}
