package com.spyeedy.idkmod.common.threedata;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.spyeedy.idkmod.common.capability.chakra.Jutsu;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.JSONUtils;
import net.threetag.threecore.util.threedata.ThreeData;

public class JutsuThreeData extends ThreeData<Jutsu> {

	public JutsuThreeData(String key) {
		super(key);
	}

	@Override
	public Jutsu parseValue(JsonObject jsonObject, Jutsu defaultValue) {
		if (!JSONUtils.isValidNode(jsonObject, jsonKey)) return defaultValue;

		JsonObject jutsuJson = JSONUtils.getAsJsonObject(jsonObject, jsonKey);

		if (!JSONUtils.isValidNode(jutsuJson, "name") || !JSONUtils.isValidNode(jutsuJson, "cost")) return defaultValue;

		return Jutsu.deserializeFromJson(jutsuJson);
	}

	@Override
	public void writeToNBT(CompoundNBT nbt, Jutsu value) {
		nbt.put(this.key, Jutsu.writeToNBT(value));
	}

	@Override
	public Jutsu readFromNBT(CompoundNBT nbt, Jutsu defaultValue) {
		if (!nbt.contains(this.key)) return defaultValue;

		CompoundNBT jutsuNbt = nbt.getCompound(this.key);

		if (!jutsuNbt.contains("name") || !jutsuNbt.contains("cost")) return defaultValue;

		return Jutsu.readFromNBT(jutsuNbt);
	}

	@Override
	public JsonElement serializeJson(Jutsu value) {
		return Jutsu.serializeJson(value);
	}
}
