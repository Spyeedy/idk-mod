package com.spyeedy.idkmod.common.mixin;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.spyeedy.idkmod.common.ability.GlideAbility;
import net.minecraft.client.renderer.entity.LivingRenderer;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.vector.Vector3f;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.ability.AbilityHelper;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(LivingRenderer.class)
public abstract class LivingRendererMixin {

	@Inject(at = @At("TAIL"), method = "setupRotations(Lnet/minecraft/entity/LivingEntity;Lcom/mojang/blaze3d/matrix/MatrixStack;FFF)V")
	protected void idkmod_setupRotations(LivingEntity livingEntity, MatrixStack matrixStack, float bobbing, float renderYawLerp, float partialRenderTick, CallbackInfo callbackInfo) {
		/*if (ClientUtils.LivingRendererSetupRotations(livingEntity, matrixStack, bobbing, renderYawLerp, partialRenderTick)) {
			return;
		}*/

		for (Ability ability : AbilityHelper.getAbilitiesFromClass(livingEntity, GlideAbility.class)) {
			if (!ability.getConditionManager().isEnabled() || livingEntity.isOnGround() || !ability.get(GlideAbility.SHOULD_GLIDE_POSE)
				|| (livingEntity instanceof PlayerEntity && ((PlayerEntity) livingEntity).isCreative() && ((PlayerEntity) livingEntity).abilities.flying)) continue;

			matrixStack.popPose(); // pop all other changes to the stack

			matrixStack.pushPose();
			matrixStack.mulPose(Vector3f.YP.rotationDegrees(180.0f-renderYawLerp));
			matrixStack.mulPose(Vector3f.XP.rotationDegrees(-67.5f));

			return;
		}
	}

}
