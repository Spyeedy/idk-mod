package com.spyeedy.idkmod.common.handler;

import com.spyeedy.idkmod.common.capability.journal.CapabilityJournal;
import com.spyeedy.idkmod.common.capability.journal.Quest;
import com.spyeedy.idkmod.common.capability.journal.Step;
import com.spyeedy.idkmod.common.capability.journal.objectives.KillCountObjective;
import com.spyeedy.idkmod.common.capability.journal.objectives.Objective;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.List;

public class QuestEventHandler {

	@SubscribeEvent
	public void onKillEntityEvent(LivingDeathEvent event) {
		Entity killedEntity = event.getEntityLiving();

		if (event.getSource().getEntity() instanceof PlayerEntity) {
			PlayerEntity player = (PlayerEntity) event.getSource().getEntity();
			player.getCapability(CapabilityJournal.JOURNAL).ifPresent(iJournal -> {
				for (Quest quest : iJournal.getOngoingQuests()) {
					Step step = quest.getStep(quest.getCurrentStep());

					List<Objective> objectives = step.getObjectives();
					for (int i = 0; i < objectives.size(); i++) {
						Objective objective = objectives.get(i);
						if (objective instanceof KillCountObjective) {
							EntityType<?> entityType = ForgeRegistries.ENTITIES.getValue(((KillCountObjective) objective).getTargetEntityRegName());

							if (killedEntity.getType().equals(entityType)) {
								((KillCountObjective) objective).setCount(((KillCountObjective) objective).getCount() + 1);
								iJournal.updateObjective(quest.getId(), quest.getCurrentStep(), i, objective);
							}
						}
					}
				}
			});
		}
	}
}
