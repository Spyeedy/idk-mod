package com.spyeedy.idkmod.common.capability;

import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.capability.chakra.CapabilityChakra;
import com.spyeedy.idkmod.common.capability.chakra.IChakra;
import com.spyeedy.idkmod.common.capability.journal.CapabilityJournal;
import com.spyeedy.idkmod.common.capability.journal.IJournal;
import com.spyeedy.idkmod.common.capability.journal.QuestReader;
import com.spyeedy.idkmod.common.network.CheckIfServerQuestMatchClientMessage;
import com.spyeedy.idkmod.common.network.PacketHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.network.PacketDistributor;

import javax.annotation.Nullable;

@Mod.EventBusSubscriber(modid = IdkMod.MODID)
public class CapabilitiesRegistryHandler {

	public static void registerCapabilities() {
		CapabilityManager.INSTANCE.register(IJournal.class, new Capability.IStorage<IJournal>() {
			@Nullable
			@Override
			public INBT writeNBT(Capability<IJournal> capability, IJournal instance, Direction side) {
				if (instance instanceof INBTSerializable)
					return ((INBTSerializable) instance).serializeNBT();
				throw new IllegalArgumentException("Can not serialize an instance that isn't an instance of INBTSerializable");

			}

			@Override
			public void readNBT(Capability<IJournal> capability, IJournal instance, Direction side, INBT nbt) {
				if (instance instanceof INBTSerializable)
					((INBTSerializable) instance).deserializeNBT(nbt);
				else
					throw new IllegalArgumentException("Can not serialize to an instance that isn't an instance of INBTSerializable");

			}
		}, () -> new CapabilityJournal(null));

		CapabilityManager.INSTANCE.register(IChakra.class, new CapabilityChakra.Storage(), () -> new CapabilityChakra(null));
	}

	@SubscribeEvent
	public static void onAttachPlayerCap(AttachCapabilitiesEvent<Entity> e) {
		if (e.getObject() instanceof PlayerEntity) {
			e.addCapability(new ResourceLocation(IdkMod.MODID, "journal"), new CapabilityJournal.Provider((PlayerEntity) e.getObject()));
			e.addCapability(new ResourceLocation(IdkMod.MODID, "chakra"), new CapabilityChakra.Provider((PlayerEntity) e.getObject()));
		}
	}

	@SubscribeEvent
	public static void onPlayerJoin(EntityJoinWorldEvent e) {
		if (e.getEntity() instanceof PlayerEntity) {
			PlayerEntity player = (PlayerEntity) e.getEntity();

			player.getCapability(CapabilityJournal.JOURNAL).ifPresent(iJournal -> {
				if (iJournal instanceof CapabilityJournal)
					((CapabilityJournal) iJournal).syncToClient();
				IdkMod.LOGGER.info("Available Quests:");
				QuestReader.getQuests().values().forEach(quest -> IdkMod.LOGGER.info(String.format("- %s", quest.getId())));
			});

			player.getCapability(CapabilityChakra.CHAKRA_CAP).ifPresent(iChakra -> {
				if (iChakra instanceof CapabilityChakra)
					((CapabilityChakra) iChakra).syncToClient();
			});

			DistExecutor.unsafeRunWhenOn(Dist.DEDICATED_SERVER, () -> () -> {
				IdkMod.LOGGER.info("This code is running on server when player joins");
				if (player instanceof ServerPlayerEntity) {
					PacketHandler.NETWORK.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity) player), new CheckIfServerQuestMatchClientMessage(QuestReader.getQuests().keySet()));
				}
			});

			DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> {
				if (QuestReader.shouldUseServerQuests) QuestReader.shouldUseServerQuests = false;
			});
		}
	}

	@SubscribeEvent
	public static void onPlayerClone(PlayerEvent.Clone event) {
		event.getOriginal().getCapability(CapabilityJournal.JOURNAL).ifPresent(utility1 -> {
			event.getPlayer().getCapability(CapabilityJournal.JOURNAL).ifPresent(utility2 -> {
				if (utility2 instanceof CapabilityJournal) {
					utility2.deserializeNBT(utility1.serializeNBT());
					((CapabilityJournal) utility2).syncToClient(event.getPlayer(), utility1.serializeNBT());
				}
			});
		});
	}
}
