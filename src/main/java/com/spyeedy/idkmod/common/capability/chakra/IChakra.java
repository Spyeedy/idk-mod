package com.spyeedy.idkmod.common.capability.chakra;

import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.common.util.INBTSerializable;

public interface IChakra extends INBTSerializable<CompoundNBT> {

	int getMaxChakra();
	void setMaxChakra(int maxChakraReserves);

	int getChakra();
	void setChakra(int chakra);
	void useChakra(int amount);

	float getChakraControlRate();
	void setChakraControlRate(float chakraControlRate);
}