package com.spyeedy.idkmod.common.capability.journal.objectives;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.mojang.serialization.JsonOps;
import com.spyeedy.idkmod.common.IdkMod;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.crafting.CraftingHelper;

import javax.annotation.Nullable;

public class CollectCountObjective extends Objective {

	protected int count;
	protected int maxCount;
	protected ItemStack stackToCollect;

	public CollectCountObjective() {
		this(0, ItemStack.EMPTY);
	}

	public CollectCountObjective(int maxCount, ItemStack stackToCollect) {
		super(Type.COLLECT_COUNT);
		this.count = 0;
		this.maxCount = maxCount;
		this.stackToCollect = stackToCollect;
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT nbt = super.serializeNBT();

		nbt.putInt("count", count);
		nbt.putInt("maxCount", maxCount);

		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		super.deserializeNBT(nbt);

		this.count = nbt.getInt("count");
		this.maxCount = nbt.getInt("maxCount");
	}

	public Objective setMaxCount(int maxCount) {
		this.maxCount = maxCount;

		return this;
	}

	public Objective setCount(int count) {
		this.count = count;
		if (this.count > maxCount) this.count = maxCount;

		return this;
	}

	public int getCount() {
		return count;
	}

	public int getMaxCount() {
		return maxCount;
	}

	@Nullable
	public ItemStack getStackToCollect() {
		return stackToCollect;
	}

	@Override
	public IFormattableTextComponent getDescription() {
		return new TranslationTextComponent(IdkMod.MODID + ".objective.collect_count", this.count, this.maxCount);
	}

	@Override
	public boolean isComplete() {
		return count == maxCount;
	}

	@Override
	public Objective deserialize(JsonObject jsonObject) throws JsonParseException {
		this.maxCount = JSONUtils.getAsInt(jsonObject, "max_count");
		this.stackToCollect = CraftingHelper.getItemStack(JSONUtils.getAsJsonObject(jsonObject, "collect_item"), true);

		return this;
	}

	@Override
	public JsonElement serialize() {
		JsonObject jsonObject = super.serialize().getAsJsonObject();

		jsonObject.addProperty("max_count", maxCount);

		// See for JsonOps and Codec: https://gist.github.com/Drullkus/1bca3f2d7f048b1fe03be97c28f87910#mapped-codec-pairs
		JsonOps.INSTANCE
				.withEncoder(ItemStack.CODEC)
				.apply(this.stackToCollect)
				.map(JsonElement::getAsJsonObject)
				.result()
				.ifPresent(jsonObject1 -> jsonObject.add("collect_item", jsonObject1));

		return jsonObject;
	}

	@Override
	public String toString() {
		return "CollectCountObjective{" +
				"type=" + getType() +
				"count=" + count +
				", maxCount=" + maxCount +
				'}';
	}
}
