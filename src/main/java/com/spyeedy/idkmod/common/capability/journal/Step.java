package com.spyeedy.idkmod.common.capability.journal;

import com.google.gson.*;
import com.spyeedy.idkmod.common.capability.journal.objectives.Objective;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class Step {

	private ITextComponent description;
	private List<Objective> objectives;

	public Step() {
		this("");
	}

	public Step(String description) {
		this.description = new StringTextComponent(description);
		this.objectives = new ArrayList<>();
	}

	public Step setDescription(String description) {
		this.description = new StringTextComponent(description);

		return this;
	}

	@Nullable
	public ITextComponent getDescription() {
		return description;
	}

	public Step addObjective(Objective objective) {
		this.objectives.add(objective);

		return this;
	}

	public List<Objective> getObjectives() {
		return objectives;
	}

	public int getObjectiveCount() {
		return objectives.size();
	}

	public Objective getObjective(int index) {
		return objectives.get(index);
	}

	public Step deserialize(JsonObject jsonObject) throws JsonParseException {
		//String name = ITextComponent.Serializer.getComponentFromJson(JSONUtils.getJsonObject(jsonObject, "name").toString());
		this.description = new StringTextComponent(JSONUtils.getAsString(jsonObject, "description"));
		JsonArray objs = JSONUtils.getAsJsonArray(jsonObject, "objectives", new JsonArray());
		objs.forEach(jsonElement -> this.objectives.add(Objective.deserializer(jsonElement.getAsJsonObject())));

		return this;
	}

	public JsonElement serialize() {
		JsonObject jsonObject = new JsonObject();

		jsonObject.addProperty("description", description.getContents());

		JsonArray objs = new JsonArray();
		this.objectives.forEach(objective -> objs.add(objective.serialize()));
		jsonObject.add("objectives", objs);

		return jsonObject;
	}

	@Override
	public String toString() {
		return "Step{" +
				"objectives=" + objectives +
				'}';
	}
}
