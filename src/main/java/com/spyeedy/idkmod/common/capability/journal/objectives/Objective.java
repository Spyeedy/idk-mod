package com.spyeedy.idkmod.common.capability.journal.objectives;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.spyeedy.idkmod.common.IdkMod;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.util.INBTSerializable;

import javax.annotation.Nonnull;

public abstract class Objective implements INBTSerializable<CompoundNBT> {

	private Type type;

	public Objective(Type type) {
		this.type = type;
	}

	@Nonnull
	public Type getType() { return type; }

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT nbt = new CompoundNBT();

		nbt.putInt("type", getType().ordinal()); // We only serialise type so that we can identify what kind of objective when deserialising

		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.type = Type.values()[nbt.getInt("type")];
	}

	public abstract IFormattableTextComponent getDescription();
	public abstract boolean isComplete();

	public static Objective deserializer(JsonObject jsonObject) throws JsonParseException {
		Objective objective = null;

		StringBuilder validTypes = new StringBuilder();
		String typeName = JSONUtils.getAsString(jsonObject, "type");

		for (Type type : Type.values()) {
			// to populate validTypes string
			switch (type) {
				case KILL_COUNT:
					validTypes = new StringBuilder(type.getSerializedName());
					break;
				case COLLECT_COUNT:
					validTypes.append(", ").append(type.getSerializedName());
					break;
			}

			if (type.getSerializedName().equals(typeName)) {
				switch (type) {
					case KILL_COUNT:
						objective = new KillCountObjective();
						break;
					case COLLECT_COUNT:
						objective = new CollectCountObjective();
						break;
				}
			}
		}

		if (objective == null)
			throw new JsonParseException(String.format("Invalid 'type' %s for Objective. Valid types are: %s", typeName, validTypes));

		objective.deserialize(jsonObject);

		return objective;
	}

	// for objective implementations to use
	public abstract Objective deserialize(JsonObject jsonObject) throws JsonParseException;

	public JsonElement serialize() {
		JsonObject jsonObject = new JsonObject();

		jsonObject.addProperty("type", this.getType().getSerializedName());

		return jsonObject;
	}

	public enum Type implements IStringSerializable {
		KILL_COUNT, COLLECT_COUNT;

		@Override
		public String getSerializedName() {
			return toString().toLowerCase();
		}

		public TranslationTextComponent getLocalizedName() {
			return new TranslationTextComponent(IdkMod.MODID + ".objective." + getSerializedName() + ".title");
		}

		public static TranslationTextComponent[] getNames() {
			TranslationTextComponent[] names = new TranslationTextComponent[values().length];
			for (Type type : values()) {
				names[type.ordinal()] = type.getLocalizedName();
			}

			return names;
		}
	}
}