package com.spyeedy.idkmod.common.capability.journal;

import com.spyeedy.idkmod.common.capability.journal.objectives.Objective;
import com.spyeedy.idkmod.common.network.PacketHandler;
import com.spyeedy.idkmod.common.network.SyncJournalMessage;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fml.network.PacketDistributor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class CapabilityJournal implements IJournal {

	// TODO Work on multi-step quest, how to progress from one step to another, eventually "completing" a quest

	@CapabilityInject(IJournal.class)
	public static Capability<IJournal> JOURNAL;

	private ArrayList<Quest> completedQuests;
	private ArrayList<Quest> ongoingQuests;
	private String trackedQuest;

	private final PlayerEntity player;

	public CapabilityJournal(PlayerEntity player) {
		this.player = player;
		this.completedQuests = new ArrayList<>();
		this.ongoingQuests = new ArrayList<>();
		this.trackedQuest = "";
	}

	@Override
	public List<Quest> getCompletedQuests() {
		return completedQuests;
	}

	@Override
	public List<Quest> getOngoingQuests() {
		return ongoingQuests;
	}

	@Override
	public List<Quest> getQuests() {
		List<Quest> questList = new ArrayList<>();
		questList.addAll(ongoingQuests);
		questList.addAll(completedQuests);
		return questList;
	}

	@Override
	public void addNewQuest(Quest quest) {
		this.ongoingQuests.add(quest);
		syncToClient();
	}

	@Override
	public void dropQuest(String questId) {
		this.ongoingQuests.removeIf(quest -> quest.getId().equals(questId));
		if (this.trackedQuest.equals(questId)) this.trackedQuest = "";
	}

	@Override
	public void markQuestCompleted(int questIdx) {
		Quest quest = this.ongoingQuests.remove(questIdx);
		this.completedQuests.add(quest);
		syncToClient();
	}

	@Override
	public void trackQuest(String questId) {
		this.trackedQuest = questId;
		syncToClient();
	}

	@Override
	public void untrackQuest() {
		this.trackedQuest = "";
		syncToClient();
	}

	@Override
	public Quest getTrackedQuest() {
		if (trackedQuest.equals("")) return null;
		for (Quest quest : ongoingQuests)
			if (quest.getId().equals(this.trackedQuest)) return quest;
		return null;
	}

	@Override
	public void updateObjective(String questId, int stepIdx, int objIdx, Objective objective) {
		for (Quest quest : ongoingQuests) {
			if (quest.getId().equals(questId)) {
				quest.getStep(stepIdx).getObjectives().set(objIdx, objective);
			}
		}
		syncToClient();
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT nbt = new CompoundNBT();

		ListNBT completedQuestsNBT = new ListNBT();
		for (Quest quest : completedQuests) {
			completedQuestsNBT.add(quest.serializeNBT());
		}
		nbt.put("completedQuests", completedQuestsNBT);

		ListNBT ongoingQuestsNBT = new ListNBT();
		for (Quest quest : ongoingQuests) {
			ongoingQuestsNBT.add(quest.serializeNBT());
		}
		nbt.put("ongoingQuests", ongoingQuestsNBT);

		nbt.putString("trackedQuest", trackedQuest);

		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.completedQuests = new ArrayList<>();
		ListNBT completedQuestsNBT = nbt.getList("completedQuests", Constants.NBT.TAG_COMPOUND);
		for (int i = 0; i < completedQuestsNBT.size(); i++) {
			Quest quest = new Quest();
			quest.deserializeNBT(completedQuestsNBT.getCompound(i));

			this.completedQuests.add(quest);
		}

		this.ongoingQuests = new ArrayList<>();
		ListNBT ongoingQuestsNBT = nbt.getList("ongoingQuests", Constants.NBT.TAG_COMPOUND);
		for (int i = 0; i < ongoingQuestsNBT.size(); i++) {
			Quest quest = new Quest();
			quest.deserializeNBT(ongoingQuestsNBT.getCompound(i));

			this.ongoingQuests.add(quest);
		}

		this.trackedQuest = nbt.getString("trackedQuest");
	}


	public void syncToClient() {
		syncToClient(this.player, serializeNBT());
	}

	public void syncToClient(PlayerEntity player, CompoundNBT nbt) {
		if (player instanceof ServerPlayerEntity)
			PacketHandler.NETWORK.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity) player), new SyncJournalMessage(nbt));
	}

	public static class Provider implements ICapabilitySerializable<CompoundNBT> {
		private IJournal journal;

		public Provider(PlayerEntity playerEntity) {
			this.journal = new CapabilityJournal(playerEntity);
		}

		@Nonnull
		@Override
		public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
			if (cap == CapabilityJournal.JOURNAL)
				return LazyOptional.of(() -> journal).cast();
			return LazyOptional.empty();
		}

		@Override
		public CompoundNBT serializeNBT() {
			if (this.journal instanceof INBTSerializable)
				return ((INBTSerializable<CompoundNBT>) this.journal).serializeNBT();
			return new CompoundNBT();
		}

		@Override
		public void deserializeNBT(CompoundNBT nbt) {
			if (this.journal instanceof INBTSerializable)
				((INBTSerializable<CompoundNBT>) this.journal).deserializeNBT(nbt);
		}
	}
}
