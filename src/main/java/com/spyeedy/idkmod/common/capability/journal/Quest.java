package com.spyeedy.idkmod.common.capability.journal;

import com.google.gson.*;
import com.spyeedy.idkmod.common.capability.journal.objectives.CollectCountObjective;
import com.spyeedy.idkmod.common.capability.journal.objectives.KillCountObjective;
import com.spyeedy.idkmod.common.capability.journal.objectives.Objective;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.INBTSerializable;

import javax.annotation.Nullable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Quest implements INBTSerializable<CompoundNBT> {

	private ItemStack reward;
	private String id;
	private String title;
	private List<Step> steps; // Each step contains a List of Objectives, so it's like a book, complete 1 step to move to next one
	private List<ITextComponent> description;
	private int currentStep;

	public Quest() {
		this(ItemStack.EMPTY, "", new ArrayList<>());
	}

	public Quest(ItemStack reward, String title, List<ITextComponent> description) {
		this.reward = reward;
		this.title = title;
		this.description = description;
		this.steps = new ArrayList<>();
		this.currentStep = 0;
	}

	public Quest addStep(Step step) {
		this.steps.add(step);

		return this;
	}

	public Quest setId(String id) {
		this.id = id;
		return this;
	}

	public Quest setTitle(String title) {
		this.title = title;
		return this;
	}

	public Quest setDescription(List<ITextComponent> description) {
		this.description = description;
		return this;
	}

	public Quest setCurrentStep(int currentStep) {
		this.currentStep = currentStep;
		return this;
	}

	public String getId() {
		return id;
	}

	@Nullable
	public String getTitle() {
		return title;
	}

	@Nullable
	public List<ITextComponent> getDescription() {
		return description;
	}

	public List<Step> getSteps() {
		return steps;
	}

	public int getStepSize() {
		return steps.size();
	}

	public Step getStep(int index) {
		return this.steps.get(index);
	}

	@Nullable
	public ItemStack getReward() {
		return reward;
	}

	public int getCurrentStep() {
		return currentStep;
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT nbt = new CompoundNBT();

		nbt.putString("id", this.id);

		ListNBT stepsNBT = new ListNBT();
		for(Step step : steps) { // We loop through each step to get List of Objectives
			ListNBT objectivesNBT = new ListNBT();
			for(Objective objective : step.getObjectives()) { // Loop through Objectives list
				objectivesNBT.add(objective.serializeNBT());
			}
			stepsNBT.add(objectivesNBT); // Once objectives list is complete, add it to the step NBT
		}
		nbt.put("steps", stepsNBT);
		nbt.putInt("currentStep", currentStep);

		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.id = nbt.getString("id");

		ListNBT stepsNBT = nbt.getList("steps", Constants.NBT.TAG_LIST);
		this.steps = new ArrayList<>(stepsNBT.size()); // Initialise steps multi-dim array with number of steps, to allocate size

		for (int i = 0; i < stepsNBT.size(); i++) {
			ListNBT objectivesNBT = stepsNBT.getList(i);
			Step step = new Step();

			// Populate objectives list to add inside Steps list
			for (int j = 0; j < objectivesNBT.size(); j++) {
				CompoundNBT objectiveNBT = objectivesNBT.getCompound(j);

				switch(Objective.Type.values()[objectiveNBT.getInt("type")]) {
					case KILL_COUNT:
						KillCountObjective killCountObjective = new KillCountObjective();
						killCountObjective.deserializeNBT(objectiveNBT);

						step.addObjective(killCountObjective);
						break;
					case COLLECT_COUNT:
						CollectCountObjective collectCountObjective = new CollectCountObjective();
						collectCountObjective.deserializeNBT(objectiveNBT);

						step.addObjective(collectCountObjective);
						break;
				}
			}
			this.steps.add(step);
		}

		this.currentStep = nbt.getInt("currentStep");
	}

	@Override
	public String toString() {
		return "Quest{" +
				"id='" + id + '\'' +
				", steps=" + steps +
				", currentStep=" + currentStep +
				'}';
	}

	public static class QuestJsonAdapter implements JsonDeserializer<Quest>, JsonSerializer<Quest>  {

		@Override
		public Quest deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
			JsonObject jsonObject = json.getAsJsonObject();
			Quest quest = new Quest();

			quest.title = JSONUtils.getAsString(jsonObject, "title");
			JSONUtils.getAsJsonArray(jsonObject, "description")
					.forEach(jsonElement -> quest.description.add(new StringTextComponent(jsonElement.getAsString())));
			quest.reward = CraftingHelper.getItemStack(JSONUtils.getAsJsonObject(jsonObject, "reward"), true);
			JSONUtils.getAsJsonArray(jsonObject, "steps", new JsonArray())
					.forEach(jsonElement -> quest.addStep(new Step().deserialize(jsonElement.getAsJsonObject())));

			return quest;
		}

		@Override
		public JsonElement serialize(Quest src, Type typeOfSrc, JsonSerializationContext context) {
			JsonObject jsonObject = new JsonObject();

			jsonObject.addProperty("title", src.title);

			JsonArray desc = new JsonArray();
			for (ITextComponent text : src.description)
				desc.add(text.getContents());
			jsonObject.add("description", desc);

			JsonArray steps = new JsonArray();
			for (Step step : src.steps)
				steps.add(step.serialize());
			jsonObject.add("steps", steps);

			return jsonObject;
		}
	}
}
