package com.spyeedy.idkmod.common.capability.journal;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.capability.journal.objectives.CollectCountObjective;
import com.spyeedy.idkmod.common.capability.journal.objectives.KillCountObjective;
import com.spyeedy.idkmod.common.capability.journal.objectives.Objective;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;

import javax.annotation.Nullable;
import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

public class QuestReader {
	public static final Gson GSON = new GsonBuilder()
			.registerTypeAdapter(Quest.class, new Quest.QuestJsonAdapter())
			.setPrettyPrinting()
			.create();

	private static File questFolder = new File("config/quests");
	private static File serverQuestFolder = new File("config/quests/server");
	private static File exportFolder = new File(questFolder, "export");
	private static Pattern unacceptedRegex = Pattern.compile("[^_a-zA-Z0-9&]", Pattern.CASE_INSENSITIVE); // look for unaccepted characters not in the regex

	private static final Map<String, Quest> QUESTS = Maps.newHashMap();
	private static final Map<String, Quest> SERVER_QUESTS = Maps.newHashMap();
	public static boolean shouldUseServerQuests = false; // if on server-side, should be true

	public static void readFiles() {
		if (questFolder != null && !questFolder.exists())
			questFolder.mkdir();

		recursiveReadFiles(questFolder, true);
	}

	public static void writeQuest(Quest quest, String filename) {
		if (!exportFolder.exists()) exportFolder.mkdir();

		File file = new File(exportFolder,  filename + (filename.endsWith(".json") ? "" : ".json"));
		try(Writer writer = new FileWriter(file)) {
			GSON.toJson(quest, writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void readServerQuests() {
		if (!serverQuestFolder.exists()) serverQuestFolder.mkdir();

		SERVER_QUESTS.clear();
		recursiveReadFiles(serverQuestFolder, false);
	}

	// getting file from sub folders: https://stackoverflow.com/a/14676464
	private static void recursiveReadFiles(File file, boolean addToClient) {
		for (File f1 : Objects.requireNonNull(file.listFiles())) {
			if (f1.isDirectory()) {
				if (f1.getName().equals("export") || f1.getName().equals("server")) continue;
				recursiveReadFiles(f1, addToClient);
			} else {
				try {
					String questId = f1.getName().substring(0, f1.getName().length() - 5);
					if (unacceptedRegex.matcher(questId).find()) { // if unacceptable characters were found
						IdkMod.LOGGER.error(String.format("Quest file '%s' has invalid characters in the filename. Acceptable characters are underscores, numbers and alphabets", f1.getName()));
						continue;
					}
					Quest quest = GSON.fromJson(new JsonReader(new FileReader(f1)), Quest.class);

					if (quest == null)
						IdkMod.LOGGER.error(String.format("Failed to parse quest file '%s'", f1.getName()));
					else {
						quest.setId(questId);
						IdkMod.LOGGER.info(String.format("Quest file '%s' was successfully read", f1.getName()));
						if (addToClient) QUESTS.put(questId, quest);
						else SERVER_QUESTS.put(questId, quest);
					}
				} catch (Exception e) {
					IdkMod.LOGGER.error(String.format("Failed to parse quest file '%s'", f1.getName()));
					e.printStackTrace();
				}
			}
		}
	}

	public static Map<String, Quest> getQuests() {
		return shouldUseServerQuests ? SERVER_QUESTS : QUESTS;
	}

	public static class Utils {

		public static Quest copyQuest(Quest quest) {
			Quest quest1 = new Quest();

			quest1.setId(quest.getId());
			quest1.setCurrentStep(0);

			for (Step step : quest.getSteps()) {
				Step step1 = new Step();

				for (Objective objective : step.getObjectives()) {
					Objective obj1 = null;
					switch (objective.getType()) {
						case KILL_COUNT:
							obj1 = new KillCountObjective(((KillCountObjective)objective).getMaxCount());
							((KillCountObjective) obj1).setTargetEntityRegName(((KillCountObjective) objective).getTargetEntityRegName());
							break;
						case COLLECT_COUNT:
							obj1 = new CollectCountObjective(((CollectCountObjective)objective).getMaxCount(), ((CollectCountObjective)objective).getStackToCollect());
							break;
					}
					step1.addObjective(obj1);
				}
				quest1.addStep(step1);
			}

			return quest1;
		}

		public static String getQuestTitle(String id) {
			return getQuests().get(id).getTitle();
		}

		public static List<ITextComponent> getQuestDesc(String id) {
			return getQuests().get(id).getDescription();
		}

		public static ItemStack getQuestReward(String id) {
			return getQuests().get(id).getReward();
		}

		public static ITextComponent getStepDesc(String questId, int stepIdx) {
			return getQuests().get(questId).getStep(stepIdx).getDescription();
		}

		public static ITextComponent formatObjectiveDescription(Objective objective) {
			return formatObjectiveDescription(objective, false, null, 0, 0);
		}

		public static ITextComponent formatObjectiveDescription(Objective objective, boolean shouldUseQuestList, String questId, int stepIdx, int objIdx) {
			IFormattableTextComponent desc = objective.getDescription();
			if (objective instanceof CollectCountObjective) {
				if (shouldUseQuestList)
					objective = getQuests().get(questId).getStep(stepIdx).getObjective(objIdx);
				IFormattableTextComponent itemText = (IFormattableTextComponent) ((CollectCountObjective) objective).getStackToCollect().getDisplayName();
				desc = desc.append(itemText.withStyle(TextFormatting.UNDERLINE));
			} else if (objective instanceof KillCountObjective) {
				IFormattableTextComponent entityText = new StringTextComponent(((KillCountObjective) objective).getTargetEntityRegName().getPath());
				desc = desc.append(entityText.withStyle(TextFormatting.UNDERLINE));
			}
			return desc;
		}

		@Nullable
		public static ResourceLocation getTargetEntityRegName(int questId, int stepIdx, int objIdx) {
			Objective objective = getQuests().get(questId).getStep(stepIdx).getObjective(objIdx);
			if (objective instanceof KillCountObjective) return ((KillCountObjective) objective).getTargetEntityRegName();
			return null;
		}

		public static ItemStack getStackToCollect(int questId, int stepIdx, int objIdx) {
			Objective objective = getQuests().get(questId).getStep(stepIdx).getObjective(objIdx);
			if (objective instanceof CollectCountObjective) return ((CollectCountObjective) objective).getStackToCollect();
			return ItemStack.EMPTY;
		}
	}

}
