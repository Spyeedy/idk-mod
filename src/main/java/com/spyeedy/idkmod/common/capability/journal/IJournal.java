package com.spyeedy.idkmod.common.capability.journal;

import com.spyeedy.idkmod.common.capability.journal.objectives.Objective;
import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.common.util.INBTSerializable;

import javax.annotation.Nullable;
import java.util.List;

public interface IJournal extends INBTSerializable<CompoundNBT> {

	List<Quest> getCompletedQuests();
	List<Quest> getOngoingQuests();
	List<Quest> getQuests();

	void addNewQuest(Quest quest);
	void dropQuest(String questId);

	void markQuestCompleted(int questIdx);

	void trackQuest(String questId);
	void untrackQuest();
	@Nullable Quest getTrackedQuest();

	void updateObjective(String questId, int stepIdx, int objIdx, Objective objective);
}
