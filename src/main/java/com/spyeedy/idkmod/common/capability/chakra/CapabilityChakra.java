package com.spyeedy.idkmod.common.capability.chakra;

import com.spyeedy.idkmod.common.network.PacketHandler;
import com.spyeedy.idkmod.common.network.SyncChakraMessage;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fml.network.PacketDistributor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class CapabilityChakra implements IChakra {

	@CapabilityInject(IChakra.class)
	public static Capability<IChakra> CHAKRA_CAP;

	private int maxChakra;
	private int chakra;
	private float chakraControlRate; // is a %, so max to be 100. Calculate chakra cost is: base chakra cost + (rate * cost)
	private PlayerEntity player;

	public CapabilityChakra(PlayerEntity player) {
		this.player = player;
		this.chakra = this.maxChakra = 100;
		this.chakraControlRate = 100f;
	}

	@Override
	public int getMaxChakra() {
		return maxChakra;
	}

	@Override
	public void setMaxChakra(int maxChakraReserves) {
		this.maxChakra = maxChakraReserves;
		syncToClient();
	}

	@Override
	public int getChakra() {
		return chakra;
	}

	@Override
	public void setChakra(int chakra) {
		this.chakra = Math.max(chakra, 0); // chakra is returned if is >= to 0
		syncToClient();
	}

	@Override
	public void useChakra(int amount) {
		amount += amount * (chakraControlRate/100f); // add on additional cost, so if amount is 10, and control rate is 50 (meaning 50% of the cost). New amount is 10 + (10 * 50/100), meaning the amount is 150% of the cost
		this.setChakra(getChakra() - amount);
	}

	@Override
	public float getChakraControlRate() {
		return chakraControlRate;
	}

	@Override
	public void setChakraControlRate(float chakraControlRate) {
		this.chakraControlRate = chakraControlRate;
		syncToClient();
	}

	public void syncToClient() {
		if (player instanceof ServerPlayerEntity)
			PacketHandler.NETWORK.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity) player), new SyncChakraMessage(serializeNBT()));
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT nbt = new CompoundNBT();

		nbt.putInt("maxChakra", maxChakra);
		nbt.putInt("chakra", chakra);
		nbt.putFloat("chakraControlRate", chakraControlRate);

		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.maxChakra = nbt.getInt("maxChakra");
		this.chakra = nbt.getInt("chakra");
		this.chakraControlRate = nbt.getFloat("chakraControlRate");
	}

	public static class Provider implements ICapabilitySerializable<CompoundNBT> {

		private final CapabilityChakra chakra;

		public Provider(PlayerEntity player) {
			this.chakra = new CapabilityChakra(player);
		}

		@Nonnull
		@Override
		public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
			if (cap == CHAKRA_CAP)
				return LazyOptional.of(() -> chakra).cast();
			return LazyOptional.empty();
		}

		@Override
		public CompoundNBT serializeNBT() {
			return chakra.serializeNBT();
		}

		@Override
		public void deserializeNBT(CompoundNBT nbt) {
			chakra.deserializeNBT(nbt);
		}
	}

	public static class Storage implements Capability.IStorage<IChakra> {
		@Nullable
		@Override
		public INBT writeNBT(Capability<IChakra> capability, IChakra instance, Direction side) {
			return instance.serializeNBT();
		}

		@Override
		public void readNBT(Capability<IChakra> capability, IChakra instance, Direction side, INBT nbt) {
			if (nbt instanceof CompoundNBT)
				instance.deserializeNBT((CompoundNBT) nbt);
		}
	}

}
