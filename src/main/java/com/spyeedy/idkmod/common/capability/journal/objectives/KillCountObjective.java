package com.spyeedy.idkmod.common.capability.journal.objectives;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.spyeedy.idkmod.common.IdkMod;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.*;
import net.minecraftforge.registries.ForgeRegistries;

import javax.annotation.Nullable;

public class KillCountObjective extends Objective {

	protected int count;
	protected int maxCount;
	protected ResourceLocation targetEntityRegName;

	public KillCountObjective() {
		this(1);
	}

	public KillCountObjective(int maxCount) {
		super(Type.KILL_COUNT);
		this.count = 0;
		this.maxCount = maxCount;
		this.targetEntityRegName = new ResourceLocation("pig");
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT nbt = super.serializeNBT();

		nbt.putInt("count", count);
		nbt.putInt("maxCount", maxCount);
		nbt.putString("targetEntity", targetEntityRegName.toString());

		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		super.deserializeNBT(nbt);

		this.count = nbt.getInt("count");
		this.maxCount = nbt.getInt("maxCount");
		this.targetEntityRegName = new ResourceLocation(nbt.getString("targetEntity"));
	}

	public Objective setCount(int count) {
		this.count = count;
		if (this.count > maxCount) this.count = maxCount;

		return this;
	}

	public Objective setMaxCount(int maxCount) {
		this.maxCount = maxCount;

		return this;
	}

	public Objective setTargetEntityRegName(ResourceLocation targetEntityRegName) {
		this.targetEntityRegName = targetEntityRegName;

		return this;
	}

	public int getCount() {
		return count;
	}

	public int getMaxCount() {
		return maxCount;
	}

	@Nullable
	public ResourceLocation getTargetEntityRegName() {
		return targetEntityRegName;
	}

	@Override
	public IFormattableTextComponent getDescription() {
		return new TranslationTextComponent(IdkMod.MODID + ".objective.kill_count", this.count, this.maxCount);
	}

	@Override
	public boolean isComplete() {
		return count == maxCount;
	}

	@Override
	public Objective deserialize(JsonObject jsonObject) throws JsonParseException {
		this.maxCount = JSONUtils.getAsInt(jsonObject, "max_count");
		this.targetEntityRegName = new ResourceLocation(JSONUtils.getAsString(jsonObject, "target_entity"));
		if (!ForgeRegistries.ENTITIES.containsKey(this.targetEntityRegName))
			throw new JsonParseException(String.format("Objective '%s' target_entity '%s' is not a valid Entity registry name.", this.getType().getSerializedName(), this.targetEntityRegName));

		return this;
	}

	@Override
	public JsonElement serialize() {
		JsonObject jsonObject = super.serialize().getAsJsonObject();

		jsonObject.addProperty("max_count", maxCount);
		jsonObject.addProperty("target_entity", targetEntityRegName.toString());

		return jsonObject;
	}

	@Override
	public String toString() {
		return "KillCountObjective{" +
				"type=" + getType() +
				"count=" + count +
				", maxCount=" + maxCount +
				", targetEntityRegName=" + targetEntityRegName +
				'}';
	}
}
