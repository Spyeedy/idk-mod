package com.spyeedy.idkmod.common.capability.chakra;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.text.ITextComponent;

public class Jutsu {

	private ITextComponent name;
	private int cost;

	public Jutsu(ITextComponent name, int cost) {
		this.name = name;
		this.cost = cost;
	}

	public ITextComponent getName() {
		return name;
	}

	public int getCost() {
		return cost;
	}

	public static JsonElement serializeJson(Jutsu jutsu) {
		JsonObject jsonObject = new JsonObject();
		jsonObject.add("name", ITextComponent.Serializer.toJsonTree(jutsu.getName()));
		jsonObject.addProperty("cost", jutsu.getCost());

		return jsonObject;
	}

	public static Jutsu deserializeFromJson(JsonObject jsonObject) {
		ITextComponent name = ITextComponent.Serializer.fromJson(JSONUtils.getAsJsonObject(jsonObject, "name").toString());
		int cost = JSONUtils.getAsInt(jsonObject, "cost");

		return new Jutsu(name, cost);
	}

	public static CompoundNBT writeToNBT(Jutsu jutsu) {
		CompoundNBT nbt = new CompoundNBT();

		nbt.putString("name", ITextComponent.Serializer.toJson(jutsu.getName()));
		nbt.putInt("cost", jutsu.getCost());

		return nbt;
	}

	public static Jutsu readFromNBT(CompoundNBT jutsuNBT) {
		return new Jutsu(ITextComponent.Serializer.fromJson (jutsuNBT.getString("name")), jutsuNBT.getInt("cost"));
	}
}