package com.spyeedy.idkmod.common.ability;

import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.ability.condition.IsGlidingCondition;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.threetag.threecore.ability.AbilityType;
import net.threetag.threecore.ability.condition.ConditionType;

@Mod.EventBusSubscriber(modid = IdkMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class AbilityRegistryHandler {

	public static final AbilityType PULL = new AbilityType(PullAbility::new).setRegistryName("pull");
	public static final AbilityType ENABLED_HOLD = new AbilityType(TargetEnemyAbility::new).setRegistryName("enabled_hold");
	public static final AbilityType UTILITY_BELT = new AbilityType(UtilityBeltInventoryAbility::new).setRegistryName("utility_belt_inventory");
	public static final AbilityType USE_CHAKRA = new AbilityType(UseChakraAbility::new).setRegistryName("use_chakra");
	public static final AbilityType RECHARGE_CHAKRA = new AbilityType(RechargeChakraAbility::new).setRegistryName("recharge_chakra");
	public static final AbilityType GLIDE = new AbilityType(GlideAbility::new).setRegistryName("glide");

	public static final ConditionType IS_GLIDING_CONDITION = new ConditionType(IsGlidingCondition::new, IdkMod.MODID, "is_gliding");

	@SubscribeEvent
	public static void registerAbilityTypes(RegistryEvent.Register<AbilityType> e) {
		e.getRegistry().register(PULL);
		e.getRegistry().register(UTILITY_BELT);
		e.getRegistry().register(USE_CHAKRA);
		e.getRegistry().register(RECHARGE_CHAKRA);
		e.getRegistry().register(GLIDE);
	}

	@SubscribeEvent
	public static void registerConditionTypes(RegistryEvent.Register<ConditionType> e) {
		e.getRegistry().register(IS_GLIDING_CONDITION);
	}
}
