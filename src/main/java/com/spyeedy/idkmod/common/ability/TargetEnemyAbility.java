package com.spyeedy.idkmod.common.ability;

import net.minecraft.entity.LivingEntity;
import net.threetag.threecore.ability.Ability;

public class TargetEnemyAbility extends Ability {

	public TargetEnemyAbility() {
		super(AbilityRegistryHandler.ENABLED_HOLD);
	}

	@Override
	public void action(LivingEntity entity) {
		super.action(entity);

		if (conditionManager.isEnabled()) {
		}
	}
}
