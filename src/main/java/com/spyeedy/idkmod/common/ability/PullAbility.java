package com.spyeedy.idkmod.common.ability;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceResult;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.util.EntityUtil;

public class PullAbility extends Ability {

	public PullAbility() {
		super(AbilityRegistryHandler.PULL);
	}

	@Override
	public void action(LivingEntity entity) {
		RayTraceResult rayTraceResult = EntityUtil.rayTraceWithEntities(entity, 10, RayTraceContext.BlockMode.COLLIDER, RayTraceContext.FluidMode.ANY);

		if (entity.level.isClientSide) return;

		if (rayTraceResult.getType() == RayTraceResult.Type.ENTITY) {
			EntityRayTraceResult entityRayTraceResult = (EntityRayTraceResult) rayTraceResult;
			Entity rayTracedEntity = entityRayTraceResult.getEntity();

			if (entity.distanceTo(rayTracedEntity) > 2)
				rayTracedEntity.setDeltaMovement(rayTracedEntity.getDeltaMovement().add((entity.getX() - rayTracedEntity.getX()) / 40, (entity.getY() - rayTracedEntity.getY()) / 40, (entity.getZ() - rayTracedEntity.getZ()) / 40));
		}
	}
}