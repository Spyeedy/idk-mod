package com.spyeedy.idkmod.common.ability;

import com.spyeedy.idkmod.common.capability.chakra.CapabilityChakra;
import com.spyeedy.idkmod.common.capability.chakra.Jutsu;
import com.spyeedy.idkmod.common.threedata.JutsuThreeData;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.util.threedata.EnumSync;
import net.threetag.threecore.util.threedata.ThreeData;

public class UseChakraAbility extends Ability {

	public static ThreeData<Jutsu> JUTSU = new JutsuThreeData("jutsu").setSyncType(EnumSync.NONE).enableSetting("The jutsu for this ability: name and cost. It will overwrite the ability's title.");

	public UseChakraAbility() {
		super(AbilityRegistryHandler.USE_CHAKRA);
	}

	@Override
	public void registerData() {
		super.registerData();
		this.dataManager.register(JUTSU, new Jutsu(new StringTextComponent("Fireball Jutsu"), 20));
	}

	@Override
	public void tick(LivingEntity entity) {
		super.tick(entity);

		ITextComponent jutsuName = this.dataManager.get(JUTSU).getName();
		if (!this.dataManager.get(TITLE).equals(jutsuName))
			this.dataManager.set(TITLE, jutsuName);
	}

	@Override
	public void action(LivingEntity entity) {
		if (entity.level.isClientSide || !(entity instanceof PlayerEntity)) return;

		entity.getCapability(CapabilityChakra.CHAKRA_CAP).ifPresent(iChakra -> iChakra.useChakra(this.dataManager.get(JUTSU).getCost()));
	}
}
