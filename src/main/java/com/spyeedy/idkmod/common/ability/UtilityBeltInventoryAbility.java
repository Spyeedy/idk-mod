package com.spyeedy.idkmod.common.ability;

import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.container.UtilityBeltContainer;
import com.spyeedy.idkmod.common.network.OpenScreenMessage;
import com.spyeedy.idkmod.common.network.PacketHandler;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.SimpleNamedContainerProvider;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemStackHandler;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.item.PredicateItemHandlerWrapper;
import net.threetag.threecore.util.threedata.ItemHandlerThreeData;
import net.threetag.threecore.util.threedata.ThreeData;

public class UtilityBeltInventoryAbility extends Ability {

	public static final ThreeData<ItemStackHandler> INV = new ItemHandlerThreeData("inventory").enableSetting("Inventory for i", "");

	public UtilityBeltInventoryAbility() {
		super(AbilityRegistryHandler.UTILITY_BELT);
	}

	@Override
	public void registerData() {
		super.registerData();

		this.dataManager.register(INV, new ItemStackHandler(12));
	}

	@Override
	public void action(LivingEntity entity) {
		if (!(entity instanceof PlayerEntity) || entity.level.isClientSide) return;

		if (entity.isCrouching()) { // bring up utility belt with player inventory, to slot in items
			if (!(entity instanceof ServerPlayerEntity)) return;

			NetworkHooks.openGui((ServerPlayerEntity) entity, new SimpleNamedContainerProvider((windowId, playerInv, player) -> new UtilityBeltContainer(windowId, this, playerInv), new TranslationTextComponent("screen." + IdkMod.MODID + ".utility_belt_ability")), packetBuffer -> {
				packetBuffer.writeResourceLocation(this.container.getId()); // ability container id
				packetBuffer.writeUtf(this.getId()); // ability id
			});
		} else { // bring up utility belt radial menu to quickly get an item
			PacketHandler.NETWORK.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity) entity), new OpenScreenMessage(OpenScreenMessage.GuiIDs.UTILITY_BELT_SELECTOR, BlockPos.ZERO, this.container.getId(), this.getId()));
		}
	}

	public IItemHandlerModifiable getItemHandler() {
		return new PredicateItemHandlerWrapper(this.get(INV), stack -> true).setChangedCallback((itemHandler, slot) -> this.dataManager.markChanged(INV));
	}
}