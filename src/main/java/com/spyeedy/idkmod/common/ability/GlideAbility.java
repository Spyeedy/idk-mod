package com.spyeedy.idkmod.common.ability;

import net.minecraft.block.AirBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraftforge.common.ForgeMod;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.util.threedata.BooleanThreeData;
import net.threetag.threecore.util.threedata.FloatThreeData;
import net.threetag.threecore.util.threedata.ThreeData;

public class GlideAbility extends Ability {

	public static final ThreeData<Boolean> SHOULD_GLIDE_POSE = new BooleanThreeData("should_glide_pose").enableSetting("should_glide_pose", "If enabled the players will glide in a pose like Batman");
	public static final ThreeData<Float> SPEED = new FloatThreeData("speed").enableSetting("speed", "Sets how fast you move horizontally");
	public static final ThreeData<Float> GRAVITY = new FloatThreeData("gravity").enableSetting("gravity", "Sets the descending speed");

	public GlideAbility() {
		super(AbilityRegistryHandler.GLIDE);
	}

	@Override
	public void registerData() {
		super.registerData();

		this.dataManager.register(SHOULD_GLIDE_POSE, true);
		this.dataManager.register(SPEED, 0.9f);
		this.dataManager.register(GRAVITY, 0.4f);
	}

	@Override
	public void action(LivingEntity entity) {
		if (entity.isOnGround() || (entity instanceof PlayerEntity && ((PlayerEntity) entity).isCreative() && ((PlayerEntity) entity).abilities.flying)) return;

		// prevent gliding if below the player is
		BlockPos blockPos = new BlockPos(entity.position()).below();
		if (!(entity.level.getBlockState(blockPos).getBlock() instanceof AirBlock)) {
			return;
		}

		// Taken from LivingEntity#travel
		double d0 = entity.getAttribute(ForgeMod.ENTITY_GRAVITY.get()).getValue();
		Vector3d vector3d = entity.getDeltaMovement();

		Vector3d vector3d1 = entity.getLookAngle();
		float f = entity.xRot * ((float)Math.PI / 180F);
		double d1 = Math.sqrt(vector3d1.x * vector3d1.x + vector3d1.z * vector3d1.z);
		double d3 = Math.sqrt(Entity.getHorizontalDistanceSqr(vector3d));
		double d4 = vector3d1.length();
		float f1 = MathHelper.cos(f);
		f1 = (float)((double)f1 * (double)f1 * Math.min(1.0D, d4 / 0.4D));
		vector3d = entity.getDeltaMovement().add(0.0D, d0 * (-1.0D + (double)f1 * 0.75D), 0.0D);
		if (vector3d.y < 0.0D && d1 > 0.0D) {
			double d5 = vector3d.y * -0.1D * (double)f1;
			vector3d = vector3d.add(vector3d1.x * d5 / d1, d5, vector3d1.z * d5 / d1);
		}

		if (f < 0.0F && d1 > 0.0D) {
			double d9 = d3 * (double)(-MathHelper.sin(f)) * 0.04D;
			vector3d = vector3d.add(-vector3d1.x * d9 / d1, d9 * 3.2D, -vector3d1.z * d9 / d1);
		}

		if (d1 > 0.0D) {
			vector3d = vector3d.add((vector3d1.x / d1 * d3 - vector3d.x) * 0.1D, 0.0D, (vector3d1.z / d1 * d3 - vector3d.z) * 0.1D);
		}

		entity.setDeltaMovement(vector3d.multiply(this.dataManager.get(SPEED), this.dataManager.get(GRAVITY), this.dataManager.get(SPEED)));
		entity.move(MoverType.SELF, entity.getDeltaMovement());

		if (!entity.level.isClientSide) {
			entity.fallDistance = 0.0F;
		}
	}
}
