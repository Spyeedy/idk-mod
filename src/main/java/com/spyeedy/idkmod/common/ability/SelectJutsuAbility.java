package com.spyeedy.idkmod.common.ability;

import com.spyeedy.idkmod.common.capability.chakra.Jutsu;
import com.spyeedy.idkmod.common.threedata.JutsuArrayThreeData;
import net.minecraft.util.text.StringTextComponent;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.ability.AbilityType;
import net.threetag.threecore.util.threedata.EnumSync;
import net.threetag.threecore.util.threedata.ThreeData;

public class SelectJutsuAbility extends Ability {

	public static final ThreeData<Jutsu[]> JUTSUS = new JutsuArrayThreeData("jutsus").setSyncType(EnumSync.NONE).enableSetting("Accepts an array of jutsus, or a single jutsu as a json object");

	public SelectJutsuAbility(AbilityType type) {
		super(type);
	}

	@Override
	public void registerData() {
		super.registerData();
		this.dataManager.register(JUTSUS, new Jutsu[]{ new Jutsu(new StringTextComponent("Shadow Clone"), 50), new Jutsu(new StringTextComponent("Water Dragon"), 10) });
	}
}
