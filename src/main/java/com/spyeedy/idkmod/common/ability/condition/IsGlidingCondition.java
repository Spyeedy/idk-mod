package com.spyeedy.idkmod.common.ability.condition;

import com.spyeedy.idkmod.common.ability.AbilityRegistryHandler;
import com.spyeedy.idkmod.common.ability.GlideAbility;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.ability.AbilityHelper;
import net.threetag.threecore.ability.condition.Condition;
import net.threetag.threecore.util.threedata.EnumSync;
import net.threetag.threecore.util.threedata.StringThreeData;
import net.threetag.threecore.util.threedata.ThreeData;

public class IsGlidingCondition extends Condition {

	public static final ThreeData<String> ABILITY_ID = new StringThreeData("ability_id").setSyncType(EnumSync.SELF).enableSetting("ability_id", "The id for the gliding ability this condition is dependent on.");

	public IsGlidingCondition(Ability ability) {
		super(AbilityRegistryHandler.IS_GLIDING_CONDITION, ability);
	}

	@Override
	public void registerData() {
		super.registerData();
		this.dataManager.register(ABILITY_ID, "");
	}

	@Override
	public boolean test(LivingEntity entity) {
		Ability dependentAbility = AbilityHelper.getAbilityById(entity, this.dataManager.get(ABILITY_ID), ability.container);

		if (dependentAbility != null && dependentAbility != this.ability && dependentAbility.getConditionManager().isEnabled() && dependentAbility instanceof GlideAbility) {
			if (!entity.isOnGround()) {
				// if entity is not player, fine
				// otherwise if player is entity, they better not be creative mode
				// else player is in creative mode, they better not be flying
				return !(entity instanceof PlayerEntity) || !((PlayerEntity) entity).isCreative() || !((PlayerEntity) entity).abilities.flying;
			}
		}
		return false;
	}
}
