package com.spyeedy.idkmod.common.ability;

import com.spyeedy.idkmod.common.capability.chakra.CapabilityChakra;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.threetag.threecore.ability.Ability;

public class RechargeChakraAbility extends Ability {

	public RechargeChakraAbility() {
		super(AbilityRegistryHandler.RECHARGE_CHAKRA);
	}

	@Override
	public void action(LivingEntity entity) {
		if (entity.level.isClientSide || !(entity instanceof PlayerEntity)) return;

		entity.getCapability(CapabilityChakra.CHAKRA_CAP).ifPresent(iChakra -> iChakra.setChakra(iChakra.getMaxChakra()));
	}
}
