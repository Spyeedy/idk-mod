package com.spyeedy.idkmod.common.network;

import com.spyeedy.idkmod.common.tileentity.ClientDataTileEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class SaveClientDataBooleanMessage implements IdkMessage {

	private BlockPos tePos;
	private boolean shouldHideColorBox;

	public SaveClientDataBooleanMessage(BlockPos tePos, boolean shouldHideColorBox) {
		this.tePos = tePos;
		this.shouldHideColorBox = shouldHideColorBox;
	}

	public SaveClientDataBooleanMessage(PacketBuffer buffer) {
		this.tePos = buffer.readBlockPos();
		this.shouldHideColorBox = buffer.readBoolean();
	}

	@Override
	public void toBytes(PacketBuffer packetBuffer) {
		packetBuffer.writeBlockPos(tePos);
		packetBuffer.writeBoolean(shouldHideColorBox);
	}

	@Override
	public void handle(Supplier<NetworkEvent.Context> ctx) {
		ctx.get().enqueueWork(() -> {
			ServerPlayerEntity playerEntity = ctx.get().getSender();
			if (playerEntity != null && playerEntity.level != null) {
				TileEntity te = playerEntity.level.getBlockEntity(this.tePos);

				if (te instanceof ClientDataTileEntity)
					((ClientDataTileEntity) te).setShouldHideColorBox(this.shouldHideColorBox);
			}
		});
		ctx.get().setPacketHandled(true);
	}
}
