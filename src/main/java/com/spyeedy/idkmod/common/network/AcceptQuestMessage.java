package com.spyeedy.idkmod.common.network;

import com.spyeedy.idkmod.common.capability.journal.CapabilityJournal;
import com.spyeedy.idkmod.common.capability.journal.Quest;
import com.spyeedy.idkmod.common.capability.journal.QuestReader;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class AcceptQuestMessage implements IdkMessage {

	 private String questId;

	 public AcceptQuestMessage(String questId) {
	 	this.questId = questId;
	 }

	 public AcceptQuestMessage(PacketBuffer packetBuffer) {
	 	this.questId = packetBuffer.readUtf(32767);
	 }

	@Override
	 public void toBytes(PacketBuffer buffer) {
	 	buffer.writeUtf(questId);
	 }

	@Override
	 public void handle(Supplier<NetworkEvent.Context> ctx) {
	 	ctx.get().enqueueWork(() -> {
		    PlayerEntity player = ctx.get().getSender();
		    if (player != null) {
		    	player.getCapability(CapabilityJournal.JOURNAL).ifPresent(iJournal -> {
		    		Quest quest = QuestReader.Utils.copyQuest(QuestReader.getQuests().get(this.questId));
		    		iJournal.addNewQuest(quest);
			    });
		    }
	    });
	 	ctx.get().setPacketHandled(true);
	 }
}