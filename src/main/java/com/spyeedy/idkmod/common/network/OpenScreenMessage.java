package com.spyeedy.idkmod.common.network;

import com.spyeedy.idkmod.client.ClientUtils;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class OpenScreenMessage implements IdkMessage {

	private GuiIDs guiId;
	private BlockPos pos;
	private ResourceLocation abilityContainerId;
	private String abilityId;

	public OpenScreenMessage(GuiIDs guiId) {
		this(guiId, BlockPos.ZERO);
	}

	public OpenScreenMessage(GuiIDs guiId, BlockPos pos) {
		this(guiId, pos, new ResourceLocation(""), "");
	}

	public OpenScreenMessage(GuiIDs guiId, BlockPos pos, ResourceLocation abilityContainerId, String abilityId) {
		this.guiId = guiId;
		this.pos = pos;

		this.abilityContainerId = abilityContainerId;
		this.abilityId = abilityId;
	}

	public OpenScreenMessage(PacketBuffer packetBuffer) {
		this.guiId = GuiIDs.values()[packetBuffer.readInt()];
		this.pos = BlockPos.of(packetBuffer.readLong());

		this.abilityContainerId = packetBuffer.readResourceLocation();
		this.abilityId = packetBuffer.readUtf(32767);
	}

	@Override
	public void toBytes(PacketBuffer packetBuffer) {
		packetBuffer.writeInt(this.guiId.ordinal());
		packetBuffer.writeLong(pos.asLong());

		packetBuffer.writeResourceLocation(this.abilityContainerId);
		packetBuffer.writeUtf(this.abilityId);
	}

	@Override
	public void handle(Supplier<NetworkEvent.Context> ctx) {
		ctx.get().enqueueWork(() -> ClientUtils.openGui(guiId, pos, abilityContainerId, abilityId));
		ctx.get().setPacketHandled(true);
	}

	public enum GuiIDs {
		DRAGGABLE_ITEM, QUEST_MAKER, QUEST_BOARD, CLIENT_DATA, UTILITY_BELT_SELECTOR
	}
}
