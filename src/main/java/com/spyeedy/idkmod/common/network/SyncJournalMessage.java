package com.spyeedy.idkmod.common.network;

import com.spyeedy.idkmod.client.gui.QuestJournalScreen;
import com.spyeedy.idkmod.common.capability.journal.CapabilityJournal;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class SyncJournalMessage implements IdkMessage {

	private CompoundNBT dataTag;
	private UpdateClientType updateType;

	public SyncJournalMessage(CompoundNBT dataTag) {
		this(dataTag, UpdateClientType.NONE);
	}

	public SyncJournalMessage(CompoundNBT dataTag, UpdateClientType updateType) {
		this.dataTag = dataTag;
		this.updateType = updateType;
	}

	public SyncJournalMessage(PacketBuffer buf) {
		this.dataTag = buf.readNbt();
		this.updateType = UpdateClientType.values()[buf.readInt()];
	}

	@Override
	public void toBytes(PacketBuffer buf) {
		buf.writeNbt(dataTag);
		buf.writeInt(updateType.ordinal());
	}

	@Override
	public void handle(Supplier<NetworkEvent.Context> ctx) {
		ctx.get().enqueueWork(() -> {
			Minecraft minecraft = Minecraft.getInstance();
			ClientPlayerEntity entity = minecraft.player;

			if (entity != null) {
				entity.getCapability(CapabilityJournal.JOURNAL).ifPresent(iJournal -> {
					if (iJournal instanceof CapabilityJournal)
						((CapabilityJournal) iJournal).deserializeNBT(this.dataTag);

					if (updateType == UpdateClientType.QUEST_JOURNAL) {
						if (minecraft.screen instanceof QuestJournalScreen) {
							((QuestJournalScreen) minecraft.screen).setPropertiesDefault(false, iJournal.getOngoingQuests());
						}
					}
				});
			}
		});
		ctx.get().setPacketHandled(true);
	}

	public enum UpdateClientType {
		NONE, QUEST_JOURNAL
	}
}
