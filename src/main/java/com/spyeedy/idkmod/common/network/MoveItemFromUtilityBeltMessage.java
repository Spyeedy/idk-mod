package com.spyeedy.idkmod.common.network;

import com.spyeedy.idkmod.common.ability.UtilityBeltInventoryAbility;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.ability.AbilityHelper;
import net.threetag.threecore.ability.container.IAbilityContainer;

import java.util.function.Supplier;

public class MoveItemFromUtilityBeltMessage implements IdkMessage {

	private ResourceLocation abilityContainerId;
	private String abilityId;
	int slotIdx;

	public MoveItemFromUtilityBeltMessage(ResourceLocation abilityContainerId, String abilityId, int slotIdx) {
		this.abilityContainerId = abilityContainerId;
		this.abilityId = abilityId;
		this.slotIdx = slotIdx;
	}

	public MoveItemFromUtilityBeltMessage(PacketBuffer packetBuffer) {
		this.slotIdx = packetBuffer.readInt();
		this.abilityContainerId = packetBuffer.readResourceLocation();
		this.abilityId = packetBuffer.readUtf(32767);
	}

	@Override
	public void toBytes(PacketBuffer packetBuffer) {
		packetBuffer.writeInt(slotIdx);
		packetBuffer.writeResourceLocation(abilityContainerId);
		packetBuffer.writeUtf(abilityId);
	}

	@Override
	public void handle(Supplier<NetworkEvent.Context> ctx) {
		ctx.get().enqueueWork(() -> {
			PlayerEntity player = ctx.get().getSender();
			if (player != null) {
				IAbilityContainer abilityContainer = AbilityHelper.getAbilityContainerFromId(player, this.abilityContainerId);
				Ability ab = AbilityHelper.getAbilityById(player, this.abilityId, abilityContainer);
				UtilityBeltInventoryAbility ability = ab instanceof UtilityBeltInventoryAbility ? (UtilityBeltInventoryAbility) ab : null;

				if (ability == null) return;

				ItemStack stack = ability.getItemHandler().getStackInSlot(this.slotIdx);
				if (stack.isEmpty()) return;

				stack = ability.getItemHandler().extractItem(this.slotIdx, stack.getCount(), false); // extract item

				Hand activeHand = player.getUsedItemHand();
				ItemStack heldStack = player.getItemInHand(activeHand);

				player.setItemInHand(activeHand, stack); // set retrieved item from utility to active hand

				if (!heldStack.isEmpty()) { // Set held stack to one of the slots in player inventory
					boolean wasHeldStackPlaced = false;

					// Loop through player hotbar (9) and player inventory (27) = 9 + 27 = 36
					for (int i = 0; i < 36; i++) {
						ItemStack slotStack = player.inventory.getItem(i);

						if (slotStack.isEmpty()) {
							player.inventory.setItem(i, heldStack);
							wasHeldStackPlaced = true;
							break;
						}
					}

					if (!wasHeldStackPlaced) { // if no space in player inventory, we drop the item
						player.drop(heldStack, false);
					}
				}

				/*player.getCapability(CapabilityUtilityBelt.UTILITY_BELT).ifPresent(iUtilityBelt -> {
					ItemStack stack = iUtilityBelt.getInventory().getStackInSlot(this.slotIdx); // get stack so that we can use the stack's count
					stack = iUtilityBelt.getInventory().extractItem(this.slotIdx, stack.getCount(), false); // extract item

					if (stack.isEmpty()) return;

					Hand activeHand = player.getUsedItemHand();
					ItemStack heldStack = player.getItemInHand(activeHand);

					player.setItemInHand(activeHand, stack); // set retrieved item from utility to active hand

					if (!heldStack.isEmpty()) { // Set held stack to one of the slots in player inventory
						boolean wasHeldStackPlaced = false;

						// Loop through player hotbar (9) and player inventory (27) = 9 + 27 = 36
						for (int i = 0; i < 36; i++) {
							ItemStack slotStack = player.inventory.getItem(i);

							if (slotStack.isEmpty()) {
								player.inventory.setItem(i, heldStack);
								wasHeldStackPlaced = true;
								break;
							}
						}

						if (!wasHeldStackPlaced) { // if no space in player inventory, we drop the item
							player.drop(heldStack, false);
						}
					}

					if (iUtilityBelt instanceof CapabilityUtilityBelt) ((CapabilityUtilityBelt) iUtilityBelt).syncToClient(player);
				});*/
			}
		});
		ctx.get().setPacketHandled(true);
	}
}
