package com.spyeedy.idkmod.common.network;

import com.spyeedy.idkmod.common.capability.journal.CapabilityJournal;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.network.PacketDistributor;

import java.util.function.Supplier;

public class DropQuestMessage implements IdkMessage {

	 private String questId;

	 public DropQuestMessage(String questId) {
	 	this.questId = questId;
	 }

	 public DropQuestMessage(PacketBuffer packetBuffer) {
	 	this.questId = packetBuffer.readUtf(32767);
	 }

	@Override
	 public void toBytes(PacketBuffer buffer) {
	 	buffer.writeUtf(questId);
	 }

	@Override
	 public void handle(Supplier<NetworkEvent.Context> ctx) {
	 	ctx.get().enqueueWork(() -> {
		    ServerPlayerEntity player = ctx.get().getSender();
		    if (player != null) {
		    	player.getCapability(CapabilityJournal.JOURNAL).ifPresent(iJournal -> {
		    		iJournal.dropQuest(this.questId);
		    		if (iJournal instanceof CapabilityJournal)
				        PacketHandler.NETWORK.send(PacketDistributor.PLAYER.with(() -> player), new SyncJournalMessage(((CapabilityJournal) iJournal).serializeNBT(), SyncJournalMessage.UpdateClientType.QUEST_JOURNAL));
			    });
		    }
	    });
	 	ctx.get().setPacketHandled(true);
	 }
}