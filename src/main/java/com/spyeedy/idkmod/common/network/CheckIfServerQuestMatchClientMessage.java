package com.spyeedy.idkmod.common.network;

import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.capability.journal.QuestReader;
import net.minecraft.client.Minecraft;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Supplier;

public class CheckIfServerQuestMatchClientMessage implements IdkMessage {

	private Collection<String> questIdList;

	public CheckIfServerQuestMatchClientMessage(Collection<String> questIdList) {
		this.questIdList = questIdList;
	}

	public CheckIfServerQuestMatchClientMessage(PacketBuffer packetBuffer) {
		int size = packetBuffer.readInt();
		Collection<String> questIdList = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			questIdList.add(packetBuffer.readUtf(32767));
		}
		this.questIdList = questIdList;
	}

	@Override
	public void toBytes(PacketBuffer packetBuffer) {
		packetBuffer.writeInt(questIdList.size());
		for (String string : questIdList)
			packetBuffer.writeUtf(string);
	}

	@Override
	public void handle(Supplier<NetworkEvent.Context> ctx) {
		ctx.get().enqueueWork(() -> {
			QuestReader.shouldUseServerQuests = true;
			QuestReader.readServerQuests();

			System.out.println(questIdList);

			int matchedStrings = 0;
			for (String questId : this.questIdList) {
				if (QuestReader.getQuests().containsKey(questId)) matchedStrings++;
			}

			System.out.println(QuestReader.getQuests().keySet());

			if (matchedStrings == questIdList.size() && this.questIdList.size() == QuestReader.getQuests().size()) { // we check if the IDs matches the server's quest list, and compare server quest size to client quest size
				IdkMod.LOGGER.info(QuestReader.getQuests().values().size());
			} else {
				IdkMod.LOGGER.info("Should be kicking player");
				if (Minecraft.getInstance().player != null)
					PacketHandler.NETWORK.sendToServer(new KickPlayerMessage());
			}
		});
		ctx.get().setPacketHandled(true);
	}
}