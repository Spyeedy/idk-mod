package com.spyeedy.idkmod.common.network;

import com.spyeedy.idkmod.common.capability.chakra.CapabilityChakra;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class SyncChakraMessage implements IdkMessage {

	private CompoundNBT dataTag;

	public SyncChakraMessage(CompoundNBT dataTag) {
		this.dataTag = dataTag;
	}

	public SyncChakraMessage(PacketBuffer buf) {
		this.dataTag = buf.readNbt();
	}

	@Override
	public void toBytes(PacketBuffer buf) {
		buf.writeNbt(dataTag);
	}

	@Override
	public void handle(Supplier<NetworkEvent.Context> ctx) {
		ctx.get().enqueueWork(() -> {
			Minecraft minecraft = Minecraft.getInstance();
			ClientPlayerEntity entity = minecraft.player;

			if (entity != null) {
				entity.getCapability(CapabilityChakra.CHAKRA_CAP).ifPresent(iChakra -> iChakra.deserializeNBT(this.dataTag));
			}
		});
		ctx.get().setPacketHandled(true);
	}
}
