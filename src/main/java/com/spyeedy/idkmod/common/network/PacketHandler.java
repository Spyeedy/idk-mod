package com.spyeedy.idkmod.common.network;

import com.spyeedy.idkmod.common.IdkMod;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;

public class PacketHandler {

	public static SimpleChannel NETWORK = NetworkRegistry.newSimpleChannel(new ResourceLocation(IdkMod.MODID, IdkMod.MODID), () -> "1", s -> s.equals("1"), s -> s.equals("1"));
	private static int networkId = -1;

	public static void registerPackets() {
		NETWORK.registerMessage(networkId++, MoveItemFromUtilityBeltMessage.class, MoveItemFromUtilityBeltMessage::toBytes, MoveItemFromUtilityBeltMessage::new, MoveItemFromUtilityBeltMessage::handle);

		NETWORK.registerMessage(networkId++, SyncJournalMessage.class, SyncJournalMessage::toBytes, SyncJournalMessage::new, SyncJournalMessage::handle);
		NETWORK.registerMessage(networkId++, AcceptQuestMessage.class, AcceptQuestMessage::toBytes, AcceptQuestMessage::new, AcceptQuestMessage::handle);
		NETWORK.registerMessage(networkId++, OpenScreenMessage.class, OpenScreenMessage::toBytes, OpenScreenMessage::new, OpenScreenMessage::handle);
		NETWORK.registerMessage(networkId++, CheckIfServerQuestMatchClientMessage.class, CheckIfServerQuestMatchClientMessage::toBytes, CheckIfServerQuestMatchClientMessage::new, CheckIfServerQuestMatchClientMessage::handle);
		NETWORK.registerMessage(networkId++, KickPlayerMessage.class, KickPlayerMessage::toBytes, KickPlayerMessage::new, KickPlayerMessage::handle);
		NETWORK.registerMessage(networkId++, SetTrackedQuestMessage.class, SetTrackedQuestMessage::toBytes, SetTrackedQuestMessage::new, SetTrackedQuestMessage::handle);
		NETWORK.registerMessage(networkId++, DropQuestMessage.class, DropQuestMessage::toBytes, DropQuestMessage::new, DropQuestMessage::handle);

		NETWORK.registerMessage(networkId++, SaveClientDataColorMessage.class, SaveClientDataColorMessage::toBytes, SaveClientDataColorMessage::new, SaveClientDataColorMessage::handle);
		NETWORK.registerMessage(networkId++, SaveClientDataBooleanMessage.class, SaveClientDataBooleanMessage::toBytes, SaveClientDataBooleanMessage::new, SaveClientDataBooleanMessage::handle);

		NETWORK.registerMessage(networkId++, SyncChakraMessage.class, SyncChakraMessage::toBytes, SyncChakraMessage::new, SyncChakraMessage::handle);
	}
}
