package com.spyeedy.idkmod.common.network;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public interface IdkMessage {

	void toBytes(PacketBuffer packetBuffer);

	void handle(Supplier<NetworkEvent.Context> ctx);
}
