package com.spyeedy.idkmod.common.network;

import com.spyeedy.idkmod.common.capability.journal.CapabilityJournal;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class SetTrackedQuestMessage implements IdkMessage {

	private final String questId;
	private final boolean shouldTrack;

	public SetTrackedQuestMessage(boolean shouldTrack) {
		this("", shouldTrack);
	}

	public SetTrackedQuestMessage(String questId, boolean shouldTrack) {
		this.questId = questId;
		this.shouldTrack = shouldTrack;
	}

	public SetTrackedQuestMessage(PacketBuffer packetBuffer) {
		this.questId = packetBuffer.readUtf(32767);
		this.shouldTrack = packetBuffer.readBoolean();
	}

	@Override
	public void toBytes(PacketBuffer packetBuffer) {
		packetBuffer.writeUtf(questId);
		packetBuffer.writeBoolean(shouldTrack);
	}

	@Override
	public void handle(Supplier<NetworkEvent.Context> ctx) {
		ctx.get().enqueueWork(() -> {
			PlayerEntity player = ctx.get().getSender();
			if (player != null) {
				player.getCapability(CapabilityJournal.JOURNAL).ifPresent(iJournal -> {
					if (this.shouldTrack)
						iJournal.trackQuest(questId);
					else
						iJournal.untrackQuest();
				});
			}
		});
		ctx.get().setPacketHandled(true);
	}
}