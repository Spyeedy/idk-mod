package com.spyeedy.idkmod.common.network;

import com.spyeedy.idkmod.common.tileentity.ClientDataTileEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class SaveClientDataColorMessage implements IdkMessage {

	private BlockPos tePos;
	private int red;
	private int green;
	private int blue;
	private int alpha;

	public SaveClientDataColorMessage(BlockPos tePos, int red, int green, int blue, int alpha) {
		this.tePos = tePos;
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.alpha = alpha;
	}

	public SaveClientDataColorMessage(PacketBuffer buffer) {
		this.tePos = buffer.readBlockPos();
		this.red = buffer.readInt();
		this.green = buffer.readInt();
		this.blue = buffer.readInt();
		this.alpha = buffer.readInt();
	}

	@Override
	public void toBytes(PacketBuffer packetBuffer) {
		packetBuffer.writeBlockPos(tePos);
		packetBuffer.writeInt(red);
		packetBuffer.writeInt(green);
		packetBuffer.writeInt(blue);
		packetBuffer.writeInt(alpha);
	}

	@Override
	public void handle(Supplier<NetworkEvent.Context> ctx) {
		ctx.get().enqueueWork(() -> {
			ServerPlayerEntity playerEntity = ctx.get().getSender();
			if (playerEntity != null && playerEntity.level != null) {
				TileEntity te = playerEntity.level.getBlockEntity(tePos);

				if (te instanceof ClientDataTileEntity)
					((ClientDataTileEntity) te).setColor(red, green, blue, alpha);
			}
		});
		ctx.get().setPacketHandled(true);
	}
}
