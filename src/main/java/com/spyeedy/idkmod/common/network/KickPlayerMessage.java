package com.spyeedy.idkmod.common.network;

import com.spyeedy.idkmod.common.IdkMod;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class KickPlayerMessage implements IdkMessage {

	public KickPlayerMessage() {}

	public KickPlayerMessage(PacketBuffer packetBuffer) {}

	@Override
	public void toBytes(PacketBuffer packetBuffer) {}

	@Override
	public void handle(Supplier<NetworkEvent.Context> ctx) {
		ctx.get().enqueueWork(() -> {
			ServerPlayerEntity playerEntity = ctx.get().getSender();
			if (playerEntity != null)
				playerEntity.connection.disconnect(new TranslationTextComponent(IdkMod.MODID + ".server.kick_message.quests_not_same")); // from: https://forums.minecraftforge.net/topic/77370-1144-how-to-kick-player-from-server/
		});
		ctx.get().setPacketHandled(true);
	}
}