package com.spyeedy.idkmod.common.container;

import com.spyeedy.idkmod.common.ability.UtilityBeltInventoryAbility;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.SlotItemHandler;

public class UtilityBeltContainer extends Container {

	public int inRadius;
	public int outRadius;
	public int yPos;
	private int utilityBeltCount;

	public UtilityBeltContainer(int windowId, UtilityBeltInventoryAbility ability, PlayerInventory playerInv) {
		super(ContainerRegistryHandler.UTILITY_BELT_ABILITY_CONTAINER, windowId);

		// values has to be the same as UtilityBeltInventoryScreen, because the "x" and "y" root, is based on width 176 and height 166, these 2 x & y root values cannot be modified.
		inRadius = 38;
		outRadius = inRadius*2;

		yPos = 35;
		int privYPos = yPos;
		final int sectorYPos = privYPos;

		// Ability Inventory
		{
			int sectorXPos = 176/2; // get centre
			int sectorCount = utilityBeltCount = ability.getItemHandler().getSlots();
			float angle = 360f/sectorCount;

			for (int sector = 0; sector < sectorCount; sector++) {
				float startAngle = sector * angle;
				float endAngle = (sector + 1) * angle;

				float radiusHalfLength = inRadius + (outRadius - inRadius)/2f;
				float sectorAngle = (float) Math.toRadians(startAngle + (endAngle - startAngle)/2);
				int drawSize = 16;

				// cos and sin takes in radians
				int x = (int) (radiusHalfLength * Math.sin(sectorAngle)) - drawSize/2; // adjacent line is horizontal, so CAH, therefore cos is used
				int y = (int) -(radiusHalfLength * Math.cos(sectorAngle)) - drawSize/2; // opposite line is vertical, so SOH, therefore sin is used

				this.addSlot(new SlotItemHandler(ability.getItemHandler(), sector, sectorXPos + x, sectorYPos + y));
			}
		}

		privYPos += outRadius + 4;
		// Player Inventory
		for (int j = 0; j < 3; j++)
			for (int i = 0; i < 9; i++)
				this.addSlot(new Slot(playerInv, i + (j + 1) * 9, 8 + i * 18, privYPos + 8 + j * 18));

		// Player Hotbar
		for (int i = 0; i < 9; i++)
			this.addSlot(new Slot(playerInv, i, 8 + i * 18, privYPos + 66));
	}

	@Override
	public boolean stillValid(PlayerEntity pPlayer) {
		return true;
	}

	/*
	 * Slot IDS
	 * Utility Belt Inventory: 0 to 11 (So total 12 slots, but can be varying)
	 * Player Inventory: utilitySlotCount to utilitySlotCount + 26 (27 slots)
	 * Hotbar: utilitySlotCount + 27 to utilitySlotCount + 35 [35 = 27 + 8] (9 slots)
	 * */
	@Override
	public ItemStack quickMoveStack(PlayerEntity playerIn, int index) {
		ItemStack itemStack = ItemStack.EMPTY;
		Slot slot = this.slots.get(index);

		if (utilityBeltCount > 0) { // allow transferring if there are utility belt slots
			if (slot != null && slot.hasItem()) {
				ItemStack stack = slot.getItem();
				itemStack = stack.copy();

				// from utility belt to hotbar first, if not then to player inventory
				if (index < utilityBeltCount && !this.moveItemStackTo(stack, utilityBeltCount + 27, utilityBeltCount + 36, false) && !this.moveItemStackTo(stack, utilityBeltCount, utilityBeltCount + 27, false)) {
					return ItemStack.EMPTY;
				} else if (index < utilityBeltCount + 36 && !this.moveItemStackTo(stack, 0, utilityBeltCount, false)) {
					return ItemStack.EMPTY;
				}

				if (stack.isEmpty())
					slot.set(ItemStack.EMPTY);
				else
					slot.setChanged();
			}
		}

		return itemStack;
	}
}
