package com.spyeedy.idkmod.common.container;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class DNASequencerContainer extends Container {

	public DNASequencerContainer(int id, World world, BlockPos pos, PlayerInventory playerInv) {
		super(ContainerRegistryHandler.DNA_SEQUENCER_CONTAINER, id);

		TileEntity te = world.getBlockEntity(pos);

		if (te != null) {
			te.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(inv -> {
				// Result slot
				this.addSlot(new SlotItemHandler(inv, 0, 125, 21));

				// Special slot
				this.addSlot(new SlotItemHandler(inv, 1, 35, 9));
				this.addSlot(new SlotItemHandler(inv, 2, 35, 33));

				// Block Storage
				for (int i = 0; i < 9; i++)
					this.addSlot(new SlotItemHandler(inv, 3 + i, 8 + i * 18, 56));
			});
		}

		// Player Inventory
		for (int j = 0; j < 3; j++)
			for (int i = 0; i < 9; i++)
				this.addSlot(new Slot(playerInv, i + (j + 1) * 9, 8 + i * 18, 84 + j * 18));

		// Player Hotbar
		for (int i = 0; i < 9; i++)
			this.addSlot(new Slot(playerInv, i, 8 + i * 18, 142));
	}

	@Override
	public boolean stillValid(PlayerEntity playerIn) {
		return true;
	}

	@Override
	public ItemStack quickMoveStack(PlayerEntity playerIn, int index) {
		ItemStack itemStack = ItemStack.EMPTY;
		Slot slot = this.slots.get(index);

		if (slot != null && slot.hasItem()) {
			ItemStack stack = slot.getItem();
			itemStack = stack.copy();

			if (stack.isEmpty()) {
				slot.set(ItemStack.EMPTY);
			} else {
				slot.setChanged();
			}

			if (stack.getCount() == itemStack.getCount()) {
				return ItemStack.EMPTY;
			}

			slot.onTake(playerIn, stack);
		}

		return itemStack;
	}
}
