package com.spyeedy.idkmod.common.container;

import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.ability.UtilityBeltInventoryAbility;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.extensions.IForgeContainerType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.ability.AbilityHelper;
import net.threetag.threecore.ability.container.IAbilityContainer;

@Mod.EventBusSubscriber(modid = IdkMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ContainerRegistryHandler {

	public static final ContainerType<DNASequencerContainer> DNA_SEQUENCER_CONTAINER = IForgeContainerType.create((windowId, inv, data) -> new DNASequencerContainer(windowId, inv.player.level, data.readBlockPos(), inv));
	public static final ContainerType<UtilityBeltContainer> UTILITY_BELT_ABILITY_CONTAINER = IForgeContainerType.create((windowId, inv, data) -> {
		ResourceLocation abilityContainerId = data.readResourceLocation();
		String abilityId = data.readUtf(32767);
		IAbilityContainer abilityContainer = AbilityHelper.getAbilityContainerFromId(inv.player, abilityContainerId);
		Ability ability = abilityContainer != null ? abilityContainer.getAbility(abilityId) : null;
		return ability instanceof UtilityBeltInventoryAbility ? new UtilityBeltContainer(windowId, (UtilityBeltInventoryAbility) ability, inv) : null;
	});

	@SubscribeEvent
	public static void registerContainerTypes(RegistryEvent.Register<ContainerType<?>> e) {
		e.getRegistry().registerAll(DNA_SEQUENCER_CONTAINER.setRegistryName("dna_sequencer"));
		e.getRegistry().registerAll(UTILITY_BELT_ABILITY_CONTAINER.setRegistryName("utility_belt_ability"));
	}
}
