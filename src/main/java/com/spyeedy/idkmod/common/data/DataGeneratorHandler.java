package com.spyeedy.idkmod.common.data;

import net.minecraft.data.DataGenerator;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class DataGeneratorHandler {

	@SubscribeEvent
	public static void gatherData(GatherDataEvent e) {
		DataGenerator dataGenerator = e.getGenerator();
		dataGenerator.addProvider(new BlockDataGenerator(dataGenerator, e.getExistingFileHelper()));
	}
}
