package com.spyeedy.idkmod.common.data;

import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.block.QuestBoardBlock;
import com.spyeedy.idkmod.common.block.BlockRegistryHandler;
import net.minecraft.data.DataGenerator;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.client.model.generators.ConfiguredModel;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.common.data.ExistingFileHelper;

public class BlockDataGenerator extends BlockStateProvider {

	public BlockDataGenerator(DataGenerator gen, ExistingFileHelper exFileHelper) {
		super(gen, IdkMod.MODID, exFileHelper);
	}

	@Override
	protected void registerStatesAndModels() {
		ModelFile dnaSequencerModel = models().getExistingFile(new ResourceLocation(IdkMod.MODID, "block/dna_sequencer"));

		getVariantBuilder(BlockRegistryHandler.DNA_SEQUENCER_BLOCK)
				.forAllStates(blockState -> {
					Direction dir = blockState.getValue(BlockStateProperties.HORIZONTAL_FACING);
					int horiIdx = dir == Direction.NORTH || dir == Direction.SOUTH ? dir.get2DDataValue() : dir == Direction.WEST ? Direction.EAST.get2DDataValue() : Direction.WEST.get2DDataValue();

					return ConfiguredModel.builder()
							.modelFile(dnaSequencerModel)
							.rotationY(dir == Direction.WEST ? 0 : (4 - (horiIdx+1)) * 90)
							.build();
				});
		getVariantBuilder(BlockRegistryHandler.QUEST_BOARD_BLOCK)
				.forAllStates(blockState -> {
					Direction dir = blockState.getValue(BlockStateProperties.HORIZONTAL_FACING);
					QuestBoardBlock.Placement placement = blockState.getValue(QuestBoardBlock.PLACEMENT);

					int horiIdx = dir == Direction.NORTH || dir == Direction.SOUTH ? 2 : dir == Direction.WEST ? 1 : 3;

					return ConfiguredModel.builder()
							.modelFile(models().getExistingFile(new ResourceLocation(IdkMod.MODID, "block/quest_board_" + placement.getModelSuffix())))
							.rotationY(dir == Direction.SOUTH ? 0 : horiIdx * 90)
							.build();
				});
	}
}
