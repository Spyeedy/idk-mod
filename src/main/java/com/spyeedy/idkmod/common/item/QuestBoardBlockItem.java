package com.spyeedy.idkmod.common.item;

import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.block.BlockRegistryHandler;
import net.minecraft.block.AirBlock;
import net.minecraft.item.BlockItem;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.Item;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;

public class QuestBoardBlockItem extends BlockItem {

	public QuestBoardBlockItem() {
		super(BlockRegistryHandler.QUEST_BOARD_BLOCK, new Item.Properties().tab(IdkMod.GROUP_IDKMOD));
		this.setRegistryName(BlockRegistryHandler.QUEST_BOARD_BLOCK.getRegistryName());
	}

	@Override
	public ActionResultType place(BlockItemUseContext context) {
		Direction facing = context.getNearestLookingDirection();
		BlockPos pos = context.getClickedPos();

		if (facing.getAxis() != Direction.Axis.Y && context.getLevel().getBlockState(pos.above()).getBlock() instanceof AirBlock
				&& context.getLevel().getBlockState(pos.relative(facing.getClockWise())).getBlock() instanceof AirBlock
				&& context.getLevel().getBlockState(pos.relative(facing.getClockWise()).above()).getBlock() instanceof AirBlock) {
			return super.place(context);
		} else {
			return ActionResultType.FAIL;
		}
	}
}
