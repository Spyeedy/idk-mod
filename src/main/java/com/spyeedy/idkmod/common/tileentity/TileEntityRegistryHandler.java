package com.spyeedy.idkmod.common.tileentity;

import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.block.BlockRegistryHandler;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = IdkMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class TileEntityRegistryHandler {

	public static final TileEntityType<DNASequencerTileEntity> DNA_SEQUENCER_TILE_ENTITY = TileEntityType.Builder.of(DNASequencerTileEntity::new, BlockRegistryHandler.DNA_SEQUENCER_BLOCK).build(null);
	public static final TileEntityType<ReactorCoreTileEntity> IDK_RENDERER_TILE_ENTITY = TileEntityType.Builder.of(ReactorCoreTileEntity::new, BlockRegistryHandler.REACTOR_CORE_BLOCK).build(null);
	public static final TileEntityType<ClientDataTileEntity> CLIENT_DATA_TILE_ENTITY = TileEntityType.Builder.of(ClientDataTileEntity::new, BlockRegistryHandler.CLIENT_DATA_BLOCK).build(null);

	@SubscribeEvent
	public static void registerTileEntityType(RegistryEvent.Register<TileEntityType<?>> e) {
		e.getRegistry().registerAll(DNA_SEQUENCER_TILE_ENTITY.setRegistryName("dna_sequencer"));
		e.getRegistry().registerAll(IDK_RENDERER_TILE_ENTITY.setRegistryName("idk_renderer"));
		e.getRegistry().registerAll(CLIENT_DATA_TILE_ENTITY.setRegistryName("client_data"));
	}
}
