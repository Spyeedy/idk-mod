package com.spyeedy.idkmod.common.tileentity;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;

import javax.annotation.Nullable;

public class ClientDataTileEntity extends TileEntity implements ITickableTileEntity {

	public int red;
	public int green;
	public int blue;
	public int alpha;
	public boolean shouldHideColorBox;

	// Lightsaber Animation tick
	public int lightsaberAnimTick = 0;
	public int lastLightsaberAnimTick = 0;
	public int maxLightsaberAnimTick = 7 * 20;
	public float lightsaberY = 0;

	// Pendulum
	public int prevAngle = -90;
	public int angle = -90;
	private boolean shouldAngleIncrement = true;

	public ClientDataTileEntity() {
		super(TileEntityRegistryHandler.CLIENT_DATA_TILE_ENTITY);
		this.red = 255;
		this.green = 255;
		this.blue = 255;
		this.alpha = 255;
		this.shouldHideColorBox = false;
	}

	public void setColor(int red, int green, int blue, int alpha) {
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.alpha = alpha;

		makeWorldSyncTEData();
	}

	public void setShouldHideColorBox(boolean shouldHideColorBox) {
		this.shouldHideColorBox = shouldHideColorBox;
		makeWorldSyncTEData();
	}

	public void makeWorldSyncTEData() {
		this.setChanged(); // to sync and save

		// to sync to client
		if (this.getLevel() != null)
			this.getLevel().sendBlockUpdated(getBlockPos(), getBlockState(), getBlockState(), 3);
	}

	@Override
	public void tick() {
		this.lastLightsaberAnimTick = lightsaberAnimTick; // set last tick
		lightsaberAnimTick++; // increment variable
		if (lightsaberAnimTick > maxLightsaberAnimTick) {
			lastLightsaberAnimTick = 0;
			lightsaberAnimTick = 0;
			lightsaberY = 0;
		}

		this.prevAngle = angle;

		if (this.shouldAngleIncrement)
			this.angle++;
		else
			this.angle--;

		if (this.angle > 90) { // angle was going positive, so we reverse the direction to be negative
			this.angle = 90;
			this.shouldAngleIncrement = false;
		}
		else if (this.angle < -90) { // angle was going negative, so we reverse the direction to be positive
			this.angle = -90;
			this.shouldAngleIncrement = true;
		}
	}

	@Override
	public CompoundNBT save(CompoundNBT compound) {
		return super.save(writeTEToNBT(compound));
	}

	@Override
	public void load(BlockState state, CompoundNBT nbt) {
		super.load(state, nbt);

		readTEFromNBT(nbt);
	}

	/* ---- Send Data to Client ---- */
	@Nullable
	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(getBlockPos(), -1, writeTEToNBT());
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		readTEFromNBT(pkt.getTag());
	}

	/* ---- Custom NBT Data Helper */
	public CompoundNBT writeTEToNBT() {
		return writeTEToNBT(new CompoundNBT());
	}

	public CompoundNBT writeTEToNBT(CompoundNBT nbt) {
		nbt.putInt("red", this.red);
		nbt.putInt("green", this.green);
		nbt.putInt("blue", this.blue);
		nbt.putInt("alpha", this.alpha);
		nbt.putBoolean("shouldHideColorBox", this.shouldHideColorBox);

		return nbt;
	}

	public void readTEFromNBT(CompoundNBT nbt) {
		this.red = nbt.getInt("red");
		this.green = nbt.getInt("green");
		this.blue = nbt.getInt("blue");
		this.alpha = nbt.getInt("alpha");
		this.shouldHideColorBox = nbt.getBoolean("shouldHideColorBox");
	}

	public static void onEntityJoinWorld(EntityJoinWorldEvent e) {
		if (!e.getWorld().isClientSide() && e.getEntity() instanceof PlayerEntity) {
			World world = e.getWorld();

			for (TileEntity te : world.blockEntityList) {
				if (te instanceof ClientDataTileEntity) {
					ClientDataTileEntity tileEntity = (ClientDataTileEntity) te;

					tileEntity.makeWorldSyncTEData();
				}
			}
		}
	}
}