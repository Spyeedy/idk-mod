package com.spyeedy.idkmod.common.tileentity;

import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;

public class DNASequencerTileEntity extends TileEntity {

	// 1 for result, 2 for special items, 9 for storage
	private ItemStackHandler inventory = new ItemStackHandler(12) {
		@Override
		public boolean isItemValid(int slot, @Nonnull ItemStack stack) {
			if (slot == 0) {
				return false;
			}
			return super.isItemValid(slot, stack);
		}

		@Override
		protected void onContentsChanged(int slot) {
			setChanged();
		}
	};

	public DNASequencerTileEntity() {
		super(TileEntityRegistryHandler.DNA_SEQUENCER_TILE_ENTITY);
	}

	@Override
	public CompoundNBT save(CompoundNBT compound) {
		compound.put("inv", inventory.serializeNBT());

		return super.save(compound);
	}

	@Override
	public void load(BlockState state, CompoundNBT nbt) {
		super.load(state, nbt);

		inventory.deserializeNBT(nbt.getCompound("inv"));
	}

	@Nonnull
	@Override
	public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap) {
		if (cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
			return LazyOptional.of(() -> inventory).cast();
		return LazyOptional.empty();
	}
}
