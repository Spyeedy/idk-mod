package com.spyeedy.idkmod.client;

import com.mojang.blaze3d.systems.RenderSystem;
import com.spyeedy.idkmod.client.gui.DNASequencerScreen;
import com.spyeedy.idkmod.client.gui.QuestJournalScreen;
import com.spyeedy.idkmod.client.gui.UtilityBeltScreen;
import com.spyeedy.idkmod.client.gui.widget.button.IdkModButton;
import com.spyeedy.idkmod.client.renderer.model.BakedModelClientData;
import com.spyeedy.idkmod.client.renderer.tileentity.ClientDataTileEntityRenderer;
import com.spyeedy.idkmod.client.renderer.tileentity.ReactorCoreTileEntityRenderer;
import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.ability.GlideAbility;
import com.spyeedy.idkmod.common.block.BlockRegistryHandler;
import com.spyeedy.idkmod.common.block.ClientDataBlock;
import com.spyeedy.idkmod.common.capability.chakra.CapabilityChakra;
import com.spyeedy.idkmod.common.capability.journal.CapabilityJournal;
import com.spyeedy.idkmod.common.capability.journal.Quest;
import com.spyeedy.idkmod.common.capability.journal.QuestReader;
import com.spyeedy.idkmod.common.capability.journal.Step;
import com.spyeedy.idkmod.common.capability.journal.objectives.Objective;
import com.spyeedy.idkmod.common.container.ContainerRegistryHandler;
import com.spyeedy.idkmod.common.tileentity.TileEntityRegistryHandler;
import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScreenManager;
import net.minecraft.client.renderer.BlockModelShapes;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.client.util.InputMappings;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.ITextProperties;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.client.event.ModelBakeEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.settings.KeyConflictContext;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.ability.AbilityHelper;
import net.threetag.threecore.event.SetRotationAnglesEvent;
import org.lwjgl.glfw.GLFW;

import java.awt.*;

@OnlyIn(Dist.CLIENT)
public class ClientHandler {

	/* Keys */
	public static final String CATEGORY = "IdkMod";
	public static final KeyBinding OPEN_JOURNAL = new KeyBinding("key." + IdkMod.MODID + ".open_journal", KeyConflictContext.IN_GAME, InputMappings.Type.KEYSYM, GLFW.GLFW_KEY_J, CATEGORY);
	public static final KeyBinding TRACKED_QUEST_SCROLL_UP = new KeyBinding("key." + IdkMod.MODID + ".tracked_quest_scroll_up", KeyConflictContext.IN_GAME, InputMappings.Type.KEYSYM, GLFW.GLFW_KEY_LEFT_BRACKET, CATEGORY);
	public static final KeyBinding TRACKED_QUEST_SCROLL_DOWN = new KeyBinding("key." + IdkMod.MODID + ".tracked_quest_scroll_down", KeyConflictContext.IN_GAME, InputMappings.Type.KEYSYM, GLFW.GLFW_KEY_RIGHT_BRACKET, CATEGORY);

	/* Quest HUD Details */
	private static int hudObjectiveScrollY = 0;
	private static int contentHeight = 0;
	private static int descHeight = 50;

	public ClientHandler() {
		// Screen Container
		ScreenManager.register(ContainerRegistryHandler.DNA_SEQUENCER_CONTAINER, DNASequencerScreen::new);
		ScreenManager.register(ContainerRegistryHandler.UTILITY_BELT_ABILITY_CONTAINER, UtilityBeltScreen::new);

		// TESR
		ClientRegistry.bindTileEntityRenderer(TileEntityRegistryHandler.IDK_RENDERER_TILE_ENTITY, ReactorCoreTileEntityRenderer::new);
		ClientRegistry.bindTileEntityRenderer(TileEntityRegistryHandler.CLIENT_DATA_TILE_ENTITY, ClientDataTileEntityRenderer::new);

		// Keybindings
		ClientRegistry.registerKeyBinding(OPEN_JOURNAL);
		ClientRegistry.registerKeyBinding(TRACKED_QUEST_SCROLL_UP);
		ClientRegistry.registerKeyBinding(TRACKED_QUEST_SCROLL_DOWN);
	}

	@SubscribeEvent
	public void onKeyInput(InputEvent.KeyInputEvent e) {
		if (OPEN_JOURNAL.isDown())
			Minecraft.getInstance().setScreen(new QuestJournalScreen());
		else {
			ClientPlayerEntity playerEntity = Minecraft.getInstance().player;

			if (playerEntity == null) return;

			playerEntity.getCapability(CapabilityJournal.JOURNAL).ifPresent(iJournal -> {
				if (iJournal.getTrackedQuest() == null || contentHeight <= descHeight) return;

				if (TRACKED_QUEST_SCROLL_UP.isDown()) {
					hudObjectiveScrollY -= 12;
					if (hudObjectiveScrollY <= 0)
						hudObjectiveScrollY = 0;
				} else if (TRACKED_QUEST_SCROLL_DOWN.isDown()) {
					hudObjectiveScrollY += 12;
					if (hudObjectiveScrollY > contentHeight - descHeight) {
						hudObjectiveScrollY = contentHeight - descHeight;
					}
				}
			});
		}
	}

	@SubscribeEvent
	public void onGuiInit(RenderGameOverlayEvent.Post e) {
		ClientPlayerEntity playerEntity = Minecraft.getInstance().player;
		FontRenderer fontRenderer = ClientUtils.fontRenderer;

		if (playerEntity == null) return;

		if (!ClientUtils.minecraft.options.renderDebug) {
			playerEntity.getCapability(CapabilityJournal.JOURNAL).ifPresent(iJournal -> {
				Quest trackedQuest = iJournal.getTrackedQuest();
				if (trackedQuest == null) return;
				if (contentHeight <= descHeight) hudObjectiveScrollY = 0;

				int textWidth = 100;
				int currStepIdx = trackedQuest.getCurrentStep();
				Step currStep = trackedQuest.getStep(currStepIdx);

				int x = e.getWindow().getGuiScaledWidth() - 2 - textWidth;
				int y = e.getWindow().getGuiScaledHeight() / 2 - descHeight/2;

				double scale = ClientUtils.minecraft.getWindow().getGuiScale();
				RenderSystem.pushMatrix();
				RenderSystem.enableScissor((int) ((x) * scale), (int)(ClientUtils.minecraft.getWindow().getHeight() - ((y + descHeight) * scale)),
						(int) (textWidth * scale), (int) (descHeight * scale));

				int yOffset = y - hudObjectiveScrollY;
				contentHeight = 0;
				for (int i = 0; i < currStep.getObjectiveCount(); i++) {
					if (i > 0) {
						yOffset += 4; // next objective, we offset by 4 to distinctly separate objective lines from each other
						contentHeight += 4;
					}
					Objective objective = currStep.getObjective(i);
					ITextComponent desc = QuestReader.Utils.formatObjectiveDescription(objective, true, trackedQuest.getId(), currStepIdx, i); // we get the objective description
//					ITextComponent desc = new StringTextComponent("Hello there, my name is Spyeedy, I'm the creator of this mod"); // we get the objective description

					// Draw completed status
					ClientUtils.minecraft.getTextureManager().bind(IdkModButton.TEX);
					AbstractGui.blit(e.getMatrixStack(), x, yOffset + 2, 0, 21, objective.isComplete() ? 4 : 0, 4, 4, 256, 256); // draw up arrow, x: -3 because length of arrow is 6. y: -5 because height is 3 and offset by 2

					// get text wrapped lines from description
					for (ITextProperties line : ClientUtils.makeWrappedText(desc, textWidth - 8)) {
						ClientUtils.renderTextPropertyText(e.getMatrixStack(), x + 8, yOffset, line, 0xFFFFFFFF, false);

						yOffset += 9; // 8 for font height, 1 for line height
						contentHeight += 9;
					}
				}
				RenderSystem.disableScissor();
				RenderSystem.popMatrix();

				if (contentHeight > descHeight) { // means there's a need to scroll
					// draw arrows
					ClientUtils.minecraft.getTextureManager().bind(IdkModButton.TEX);
					AbstractGui.blit(e.getMatrixStack(), x + textWidth / 2 - 5, y - 9, 0, 27, 0, 10, 6, 256, 256); // draw up arrow, x: -3 because length of arrow is 6. y: -5 because height is 3 and offset by 2
					AbstractGui.blit(e.getMatrixStack(), x + textWidth / 2 - 5, y + descHeight + 3, 0, 37, 0, 10, 6, 256, 256); // draw down arrow. y: +2 is offset
				}
			});

			playerEntity.getCapability(CapabilityChakra.CHAKRA_CAP).ifPresent(iChakra -> {
				String str = String.format("Chakra: %d/%d", iChakra.getChakra(), iChakra.getMaxChakra());
				int strWidth = fontRenderer.width(str);

				int x = e.getWindow().getGuiScaledWidth() - 2 - strWidth;
				int y = 2;

				fontRenderer.drawShadow(e.getMatrixStack(), str, x, y, Color.WHITE.getRGB());

				str = String.format("Chakra Control Rate: %.2f/100", 100 - iChakra.getChakraControlRate());
				strWidth = fontRenderer.width(str);

				x = e.getWindow().getGuiScaledWidth() - 2 - strWidth;
				y += 10;
				fontRenderer.drawShadow(e.getMatrixStack(), str, x, y, Color.WHITE.getRGB());
			});
		}
	}

	@SubscribeEvent
	public void setRotationAngles(SetRotationAnglesEvent e) {
		LivingEntity entity = e.getEntityLiving();
		for (Ability ability : AbilityHelper.getAbilitiesFromClass(entity, GlideAbility.class)) {
			if (!ability.getConditionManager().isEnabled() || entity.isOnGround() || !ability.get(GlideAbility.SHOULD_GLIDE_POSE)
					|| (entity instanceof PlayerEntity && ((PlayerEntity) entity).isCreative() && ((PlayerEntity) entity).abilities.flying)) continue;

			if (e.model != null) {
				e.model.leftArm.xRot = e.model.leftArm.yRot = 0.0f;
				e.model.rightArm.xRot = e.model.rightArm.yRot = 0.0f;
				e.model.leftLeg.xRot = e.model.leftLeg.yRot = 0.0f;
				e.model.rightLeg.xRot = e.model.rightLeg.yRot = 0.0f;

				e.model.leftArm.zRot = (float) -(Math.PI/2.0f);
				e.model.rightArm.zRot = (float) (Math.PI/2.0f);
				break;
			}
		}
	}

	public static void onModelBake(ModelBakeEvent event) {
		for (BlockState state : BlockRegistryHandler.CLIENT_DATA_BLOCK.getStateDefinition().getPossibleStates()) {
			Direction facing = state.getValue(ClientDataBlock.FACING);
			if (facing == Direction.WEST) {
				ModelResourceLocation northVariant = BlockModelShapes.stateToModelLocation(state);
				IdkMod.LOGGER.info("what's this? a `MRL? " + northVariant.toString());
				event.getModelRegistry().put(northVariant, new BakedModelClientData());
			}
		}
	}
}
