package com.spyeedy.idkmod.client;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.spyeedy.idkmod.client.gui.*;
import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.ability.GlideAbility;
import com.spyeedy.idkmod.common.network.OpenScreenMessage;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.*;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.IReorderingProcessor;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.math.vector.Quaternion;
import net.minecraft.util.math.vector.Vector2f;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.util.text.*;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.client.gui.GuiUtils;
import net.threetag.threecore.ability.Ability;
import net.threetag.threecore.ability.AbilityHelper;
import org.lwjgl.opengl.GL11;

import javax.annotation.Nullable;
import java.util.*;

@OnlyIn(Dist.CLIENT)
public class ClientUtils {

	public static Minecraft minecraft = Minecraft.getInstance();
	public static FontRenderer fontRenderer = minecraft.font;

	public static void openGui(OpenScreenMessage.GuiIDs id) {
		openGui(id, BlockPos.ZERO, new ResourceLocation(""), "");
	}

	public static void openGui(OpenScreenMessage.GuiIDs id, BlockPos vertex, ResourceLocation abilityContainerId, String abilityId) {
		DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> {
			switch (id) {
				case DRAGGABLE_ITEM:
					Minecraft.getInstance().setScreen(new DraggableItemScreen());
					break;
				case QUEST_MAKER:
					Minecraft.getInstance().setScreen(new QuestMakerScreen());
					break;
				case QUEST_BOARD:
					Minecraft.getInstance().setScreen(new QuestBoardScreen());
					break;
				case CLIENT_DATA:
					Minecraft.getInstance().setScreen(new ClientDataScreen(vertex));
					break;
				case UTILITY_BELT_SELECTOR:
					Minecraft.getInstance().setScreen(new UtilityBeltSelectorScreen(abilityContainerId, abilityId));
			}
		});
	}

	public static String getEllipsisString(String text, int width) {
		int stringWidth = fontRenderer.width(text);
		if (stringWidth > width) {
			int ellipsisWidth = fontRenderer.width(" ...");
			return fontRenderer.plainSubstrByWidth(text, width - ellipsisWidth + 1) + " ...";
		}
		return text;
	}

	public static IReorderingProcessor getEllipsisString(ITextComponent text, int width) {
		int stringWidth = fontRenderer.width(text);
		if (stringWidth > width) {
			int ellipsisWidth = fontRenderer.width(" ...");
			return IReorderingProcessor.fromPair(fontRenderer.split(text, width - ellipsisWidth + 1).get(0), new StringTextComponent(" ...").getVisualOrderText());
		}
		return text.getVisualOrderText();
	}

	public static void renderTooltip(MatrixStack matrixStack, ITextComponent text, int mouseX, int mouseY) {
		renderTooltip(matrixStack, Collections.singletonList(text), mouseX, mouseY);
	}

	public static void renderTooltip(MatrixStack matrixStack, List<ITextComponent> text, int mouseX, int mouseY) {
		GuiUtils.drawHoveringText(matrixStack, text, mouseX, mouseY, minecraft.getWindow().getGuiScaledWidth(), minecraft.getWindow().getGuiScaledHeight(), -1, fontRenderer);
	}

	public static List<ITextProperties> makeWrappedText(ITextComponent line, int width) {
		return makeWrappedText(Collections.singletonList(line), width);
	}

	// Taken from GuiUtils.drawHoveringText
	public static List<ITextProperties> makeWrappedText(List<ITextComponent> lines, int width) {
		List<ITextProperties> wrappedLines = new ArrayList<>();

		for (ITextComponent line : lines) { // loop through each line and break them up
			if (line instanceof StringTextComponent && line.getContents().equals("")) { // we check if the line is an empty one, if it is, we add it
				wrappedLines.add(new StringTextComponent(""));
				continue;
			}

			// func_238362_b_ to break the line up so that the text fits the width, it's text wrapping basically
			List<ITextProperties> wrappedContent = fontRenderer.getSplitter().splitLines(line, width, Style.EMPTY);
			wrappedLines.addAll(wrappedContent); // add ALL of the NEW text lines to the list
		}

		return wrappedLines;
	}

	// Taken from GuiUtils.drawHoveringText
	public static void renderTextPropertyText(MatrixStack matrixStack, int x, int y, ITextProperties text, int color, boolean drawShadow) {
		// getVisualOrder is to get IReorderingProcessor from ITextProperties
		IReorderingProcessor line = LanguageMap.getInstance().getVisualOrder(text);
		if (drawShadow)
			fontRenderer.drawShadow(matrixStack, line, x, y, color);
		else
			fontRenderer.draw(matrixStack, line, x, y, color);
	}

	/**
	 * xz starts from centre, y starts from 0 to height
	 * @param length x-length, draws from -length/2 to length/2
	 * @param breadth z-length, draws from -breadth/2 to breadth/2
	 * @param height y-length, draws from 0 to height
	 * @param sides which sides to draw
	 */
	public static void renderBox(Matrix4f matrix, IVertexBuilder builder, float length, float breadth, float height, int red, int green, int blue, int alpha, int lightMap, Direction... sides) {
		/* on the quad we are looking at, the points has to be connected CCW-direction */
		if (Arrays.stream(sides).anyMatch(direction -> direction == Direction.NORTH)) {
			renderFace(builder, matrix, Direction.Axis.X, -breadth/2, -length/2, length/2, 0, height, red, green, blue, alpha, lightMap);
		}

		if (Arrays.stream(sides).anyMatch(direction -> direction == Direction.SOUTH)) {
			renderFace(builder, matrix, Direction.Axis.X, breadth/2, length / 2, -length/2, 0, height, red, green, blue, alpha, lightMap);
		}

		if (Arrays.stream(sides).anyMatch(direction -> direction == Direction.WEST)) {
			renderFace(builder, matrix, Direction.Axis.Z, -length/2, breadth/2, -breadth/2, 0, height, red, green, blue, alpha, lightMap);
		}

		if (Arrays.stream(sides).anyMatch(direction -> direction == Direction.EAST)) {
			renderFace(builder, matrix, Direction.Axis.Z, length/2, -breadth / 2, breadth / 2, 0, height, red, green, blue, alpha, lightMap);
		}

		if (Arrays.stream(sides).anyMatch(direction -> direction == Direction.UP)) {
			renderFace(builder, matrix, Direction.Axis.Y, height, -length/2, length/2, -breadth/2, breadth/2, red, green, blue, alpha, lightMap);
		}

		if (Arrays.stream(sides).anyMatch(direction -> direction == Direction.DOWN)) {
			renderFace(builder, matrix, Direction.Axis.Y, 0, length/2, -length/2, -breadth/2, breadth/2, red, green, blue, alpha, lightMap);
		}
	}

	/**
	 * Render a face along an axis of your desires
	 * @param axis Choose which axis you want to render the face on
	 * @param constPos The point that never changes
	 * @param startPoint The bottom-right of the face
	 * @param endPoint The bottom-left of the face
	 * @param startLength The top-right of the face
	 * @param endLength The top-left of the face
	 */
	public static void renderFace(IVertexBuilder builder, Matrix4f matrix4f, Direction.Axis axis, float constPos, float startPoint, float endPoint, float startLength, float endLength, int red, int green, int blue, int alpha, int lightMap) {
		switch (axis) {
			case X:
				builder.vertex(matrix4f, startPoint, startLength, constPos).color(red, green, blue, alpha).uv2(lightMap).endVertex();
				builder.vertex(matrix4f, startPoint, endLength, constPos).color(red, green, blue, alpha).uv2(lightMap).endVertex();
				builder.vertex(matrix4f, endPoint, endLength, constPos).color(red, green, blue, alpha).uv2(lightMap).endVertex();
				builder.vertex(matrix4f, endPoint, startLength, constPos).color(red, green, blue, alpha).uv2(lightMap).endVertex();
				break;
			case Y:
				builder.vertex(matrix4f, startPoint, constPos, startLength).color(red, green, blue, alpha).uv2(lightMap).endVertex();
				builder.vertex(matrix4f, startPoint, constPos, endLength).color(red, green, blue, alpha).uv2(lightMap).endVertex();
				builder.vertex(matrix4f, endPoint, constPos, endLength).color(red, green, blue, alpha).uv2(lightMap).endVertex();
				builder.vertex(matrix4f, endPoint, constPos, startLength).color(red, green, blue, alpha).uv2(lightMap).endVertex();
				break;
			case Z:
				builder.vertex(matrix4f, constPos, startLength, startPoint).color(red, green, blue, alpha).uv2(lightMap).endVertex();
				builder.vertex(matrix4f, constPos, endLength, startPoint).color(red, green, blue, alpha).uv2(lightMap).endVertex();
				builder.vertex(matrix4f, constPos, endLength, endPoint).color(red, green, blue, alpha).uv2(lightMap).endVertex();
				builder.vertex(matrix4f, constPos, startLength, endPoint).color(red, green, blue, alpha).uv2(lightMap).endVertex();
				break;
		}
	}

	public static void fill(Matrix4f matrix4f, float minX, float minY, float maxX, float maxY, int red, int green, int blue, int alpha) {
		if (minX < maxX) {
			float i = minX;
			minX = maxX;
			maxX = i;
		}

		if (minY < maxY) {
			float j = minY;
			minY = maxY;
			maxY = j;
		}

		fill(matrix4f, new Vector2f(minX, maxY), new Vector2f(maxX, maxY), new Vector2f(maxX, minY), new Vector2f(minX, minY), red, green, blue, alpha);
	}

	public static void fill(Matrix4f matrix, Vector2f bottomLeft, Vector2f bottomRight, Vector2f topRight, Vector2f topLeft, int red, int green, int blue, int alpha) {
		BufferBuilder bufferbuilder = Tessellator.getInstance().getBuilder();
		RenderSystem.enableBlend();
		RenderSystem.disableTexture();
		RenderSystem.defaultBlendFunc();
		bufferbuilder.begin(7, DefaultVertexFormats.POSITION_COLOR);
		bufferbuilder.vertex(matrix, bottomLeft.x, bottomLeft.y, 0.0F).color(red, green, blue, alpha).endVertex();
		bufferbuilder.vertex(matrix, bottomRight.x, bottomRight.y, 0.0F).color(red, green, blue, alpha).endVertex();
		bufferbuilder.vertex(matrix, topRight.x, topRight.y, 0.0F).color(red, green, blue, alpha).endVertex();
		bufferbuilder.vertex(matrix, topLeft.x, topLeft.y, 0.0F).color(red, green, blue, alpha).endVertex();
		bufferbuilder.end();
		WorldVertexBufferUploader.end(bufferbuilder);
		RenderSystem.enableTexture();
		RenderSystem.disableBlend();
	}

	public interface DrawPolygon {
		void drawSector(MatrixStack matrixStack, float startAngle, float endAngle, float inRadius, float outRadius);
	}

	/*
	 * Beware, the polygon is drawn clockwise from top-right quadrant.
	 * Draws a polygon circle with a sector count to divide the polygon. Only draws the outline and divisor for each sector. The actual sector fill is up to the coder through the DrawPolygon parameter
	 * */
	public static void drawPolygon(MatrixStack matrixStack, float inRadius, float outRadius, float outlineThickness, int sectorCount, float mouseX, float mouseY, @Nullable java.awt.Color outlineColor, DrawPolygon drawExtraInPolygon) {
		int angle = 360/sectorCount;

		// draw actual sector
		for (int sector = 0; sector < sectorCount; sector++) {
			float startAngle = sector * angle;
			float endAngle = (sector + 1) * angle;

			drawExtraInPolygon.drawSector(matrixStack, startAngle, endAngle, inRadius, outRadius);
		}

		if (outlineColor != null) {
			// draw outline
			for (int sector = 0; sector < sectorCount; sector++) {
				float startAngle = sector * angle;
				float endAngle = (sector + 1) * angle;

				// drawing divisor, if you orient yourself to look at each sectors, with their tip (narrowest angle) as the bottom, we are drawing the divisor on the left side
				/* Imagine the sector oriented like that:
				* __
				* \/
				* Left side is \
				* */
				matrixStack.pushPose();
				matrixStack.mulPose(new Quaternion(0, 0, startAngle, true)); // rotate the drawing board according to the current start angle for the sector
				fill(matrixStack.last().pose(), inRadius-0.5f, -0.5f, outRadius, 0.5f, outlineColor.getRed(), outlineColor.getGreen(), outlineColor.getBlue(), outlineColor.getAlpha()); // in the tilted drawing board, draw to the right and to the top
				matrixStack.popPose();

				// Draw outline itself "bottom" edge of the outline (inRadius), and "top" edge of the outline (outRadius)
				drawPolygonSector(matrixStack, inRadius-outlineThickness, inRadius, startAngle, endAngle, outlineColor.getRed(), outlineColor.getGreen(), outlineColor.getBlue(), outlineColor.getAlpha()); // Inner Ring outline
				drawPolygonSector(matrixStack, outRadius, outRadius+outlineThickness, startAngle, endAngle, outlineColor.getRed(), outlineColor.getGreen(), outlineColor.getBlue(), outlineColor.getAlpha()); // Outer Ring outline
			}
		}
	}

	/*
	* Beware, the sector is drawn clockwise from top-right quadrant. Because in Minecraft GUI rendering for y-coordinate, going down is +ve, going up is -ve
	* Additionally, make sure you have set the position of the MatrixStack to be the center of the polygon. The polygon sector is drawn from the center of the polygon
	* */
	public static void drawPolygonSector(MatrixStack matrixStack, float inRadius, float outRadius, float startAngle, float endAngle, int red, int green, int blue, int alpha) {
		// We convert angle to radians because... well math!
		float leftSideAngle = (float) Math.toRadians(startAngle);
		float rightSideAngle = (float) Math.toRadians(endAngle);

		Matrix4f matrix = matrixStack.last().pose();

		/* Calculating x, y coordinates from angle and hypotenuse length https://gamedev.stackexchange.com/a/137292
		 \|/
		--+--
		 /|\
		We start drawing from |/. Top-right quadrant, from the left. BUT BUT, beware that the Y-coordinate will have to be made -ve. See comment above this method for more details.

		|/ : x-coordinate from SOH, y-coordinate from CAH
		*/
		float inLeftX = inRadius * (float) Math.sin(leftSideAngle); // adjacent line is horizontal, so CAH, therefore cos is used
		float inLeftY = -inRadius * (float) Math.cos(leftSideAngle); // opposite line is vertical, so SOH, therefore sin is used
		float outLeftX = outRadius * (float) Math.sin(leftSideAngle); // adjacent line is horizontal, so CAH, therefore cos is used
		float outLeftY = -outRadius * (float) Math.cos(leftSideAngle); // opposite line is vertical, so SOH, therefore sin is used
		float inRightX = inRadius * (float) Math.sin(rightSideAngle); // adjacent line is horizontal, so CAH, therefore cos is used
		float inRightY = -inRadius * (float) Math.cos(rightSideAngle); // opposite line is vertical, so SOH, therefore sin is used
		float outRightX = outRadius * (float) Math.sin(rightSideAngle); // adjacent line is horizontal, so CAH, therefore cos is used
		float outRightY = -outRadius * (float) Math.cos(rightSideAngle); // opposite line is vertical, so SOH, therefore sin is used

		// Imagine the first sector drawn in the bottom-right quadrant, coordinates go: bottom-left corner (inner ring left), bottom-right corner (inner ring right), top-right corner (outer ring right), top-left corner (outer ring left)
		/* Imagine the sector like this
		* __
		* \/
		* Left side is \
		* */
		ClientUtils.fill(matrix, new Vector2f(inLeftX, inLeftY), new Vector2f(inRightX, inRightY), new Vector2f(outRightX, outRightY), new Vector2f(outLeftX, outLeftY), red, green, blue, alpha);
	}

	// for runtime glide pose changes
	public static boolean LivingRendererSetupRotations(LivingEntity livingEntity, MatrixStack matrixStack, float bobbing, float renderYawLerp, float partialRenderTick) {
		for (Ability ability : AbilityHelper.getAbilitiesFromClass(livingEntity, GlideAbility.class)) {
			if (!ability.getConditionManager().isEnabled() || livingEntity.isOnGround() || !ability.get(GlideAbility.SHOULD_GLIDE_POSE)
				/*|| (entity instanceof PlayerEntity && ((PlayerEntity) entity).isCreative() && ((PlayerEntity) entity).abilities.flying)*/) continue;

			matrixStack.popPose();

			matrixStack.pushPose();
			matrixStack.mulPose(Vector3f.YP.rotationDegrees(180.0f-renderYawLerp));
			matrixStack.mulPose(Vector3f.XP.rotationDegrees(-67.5f));

			return true;
		}

		return false;
	}

	public static class RenderTypes extends RenderType {
		public RenderTypes(String nameIn, VertexFormat formatIn, int drawModeIn, int bufferSizeIn, boolean useDelegateIn, boolean needsSortingIn, Runnable setupTaskIn, Runnable clearTaskIn) {
			super(nameIn, formatIn, drawModeIn, bufferSizeIn, useDelegateIn, needsSortingIn, setupTaskIn, clearTaskIn);
		}

		public static RenderType getGlowingLine(double width) {
			return create(IdkMod.MODID + ":glowing_line", DefaultVertexFormats.POSITION_COLOR_LIGHTMAP, GL11.GL_LINE_STRIP, 256, State.builder()
					.setLineState(new LineState(OptionalDouble.of(width)))
					.setTextureState(RenderState.NO_TEXTURE)
					.setCullState(RenderState.NO_CULL)
					.setAlphaState(DEFAULT_ALPHA)
					.setTransparencyState(RenderState.LIGHTNING_TRANSPARENCY)
					.createCompositeState(true));
		}

		public static final RenderType GLOWING_TRANSLUCENT = create(IdkMod.MODID + ":glowing_translucent", DefaultVertexFormats.POSITION_COLOR_LIGHTMAP, GL11.GL_QUADS, 256, State.builder()
				.setTextureState(RenderState.NO_TEXTURE)
				.setCullState(RenderState.CULL)
				.setAlphaState(DEFAULT_ALPHA)
				.setTransparencyState(RenderState.LIGHTNING_TRANSPARENCY)
				.createCompositeState(true));

		public static final RenderType GLOWING_SOLID = create(IdkMod.MODID + ":glowing_solid", DefaultVertexFormats.POSITION_COLOR_LIGHTMAP, GL11.GL_QUADS, 256, State.builder()
				.setTextureState(RenderState.NO_TEXTURE)
				.setCullState(RenderState.NO_CULL)
				.createCompositeState(false));

		public static final RenderType TRANSLUCENT = create(IdkMod.MODID + ":translucent", DefaultVertexFormats.POSITION_COLOR, GL11.GL_QUADS, 256, State.builder()
				.setTextureState(RenderState.NO_TEXTURE)
				.setCullState(RenderState.CULL)
				.setAlphaState(DEFAULT_ALPHA)
				.setTransparencyState(RenderState.TRANSLUCENT_TRANSPARENCY)
				.createCompositeState(true));

		public static final RenderType SOLID = create(IdkMod.MODID + ":solid", DefaultVertexFormats.POSITION_COLOR, GL11.GL_QUADS, 256, State.builder()
				.setTextureState(RenderState.NO_TEXTURE)
				.setCullState(RenderState.NO_CULL)
				.createCompositeState(true));
	}
}
