package com.spyeedy.idkmod.client.renderer.tileentity;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.spyeedy.idkmod.client.ClientUtils;
import com.spyeedy.idkmod.client.model.saber.CrossSaber;
import com.spyeedy.idkmod.client.model.saber.ModelSaber;
import com.spyeedy.idkmod.client.model.saber.SkywalkerSaber;
import com.spyeedy.idkmod.client.model.saber.WinduSaber;
import com.spyeedy.idkmod.common.block.ClientDataBlock;
import com.spyeedy.idkmod.common.tileentity.ClientDataTileEntity;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.Direction;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.math.vector.Vector3f;

import javax.annotation.Nullable;
import java.util.HashMap;

public class ClientDataTileEntityRenderer extends TileEntityRenderer<ClientDataTileEntity> {
	public static final HashMap<ModelSaber.SaberType, ModelSaber> SABER_MODELS = new HashMap<>();

	static {
		SABER_MODELS.put(ModelSaber.SaberType.CROSS, new CrossSaber());
		SABER_MODELS.put(ModelSaber.SaberType.SKYWALKER, new SkywalkerSaber());
		SABER_MODELS.put(ModelSaber.SaberType.WINDU, new WinduSaber());
	}

	public ClientDataTileEntityRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	@Override
	public void render(ClientDataTileEntity tileEntityIn, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
		if (tileEntityIn.getBlockState().hasProperty(ClientDataBlock.FACING)) {
			Direction blockFacing = tileEntityIn.getBlockState().getValue(ClientDataBlock.FACING);

			if (blockFacing != Direction.WEST && blockFacing != Direction.SOUTH) {
				if (!tileEntityIn.shouldHideColorBox) {
					matrixStackIn.pushPose();
					Matrix4f matrix = matrixStackIn.last().pose();
					IVertexBuilder builder = bufferIn.getBuffer(ClientUtils.RenderTypes.TRANSLUCENT);

					matrixStackIn.translate(0.5, 1, 0.5);
					ClientUtils.renderBox(matrix, builder, 0.25f, 0.25f, 1, tileEntityIn.red, tileEntityIn.green, tileEntityIn.blue, tileEntityIn.alpha, combinedLightIn, Direction.NORTH, Direction.EAST, Direction.SOUTH, Direction.WEST, Direction.UP);

					matrixStackIn.popPose();
				}

				IVertexBuilder builder = bufferIn.getBuffer(ClientUtils.RenderTypes.SOLID);

				matrixStackIn.pushPose();
				float boxSize = 0.1f;
				matrixStackIn.translate(0.5, 2.5, 0.5);
				ClientUtils.renderBox(matrixStackIn.last().pose(), builder, boxSize, boxSize, boxSize, 0, 0, 0, 255, combinedLightIn, Direction.values());
				matrixStackIn.popPose();

				// TODO Make an animation like spidey's web swinging... one day.. one day
				matrixStackIn.pushPose();
				Matrix4f matrix = matrixStackIn.last().pose();
				matrixStackIn.translate(0.5, 2.5f, 0.5);
				matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(blockFacing == Direction.EAST ? 270 : blockFacing.getOpposite().get2DDataValue() * 90));
				matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(tileEntityIn.prevAngle + (tileEntityIn.angle - tileEntityIn.prevAngle)*partialTicks));

				int red = 255, colors = 0;
				float thickness = 0.1f;
				float length = 0.7f;
				/*
				* Imagine the triangle like this:
				* |\
				* where | is the adjacent line, and \ is the hypotenuse line.
				* In this triangle, the opposite line determines the X, while the adjacent line determines the Y (beware that the Y value has to be multipled by -1 to become a negative value, since it's going down)
				* */
//				double angle = Math.toRadians(70);
//				float offsetX = (float) (length * Math.sin(angle)); // SOH, hypotenuse is known, so Hypotenuse * Sin(angle) = Opposite
//				float offsetY = (float) (length * Math.cos(angle)); // CAH, Hypotenuse * Cos(angle) = Adjacent

				builder.vertex(matrix,0,0, thickness).color(red, colors, colors, 255).endVertex(); // top-right
				builder.vertex(matrix,0,0, -thickness).color(red, colors, colors, 255).endVertex(); // top-left
				builder.vertex(matrix,0, -length, -thickness).color(red, colors, colors, 255).endVertex(); // bottom-left. for the z, offsetX - thickness to move backwards from the bottom - right coordinate
				builder.vertex(matrix,0, -length, thickness).color(red, colors, colors, 255).endVertex(); // bottom-right

				matrixStackIn.popPose();
			}

			if (blockFacing == Direction.EAST) {
				float scale = -0.0625f * 0.1f;
				matrixStackIn.pushPose();
				matrixStackIn.translate(-0.1 * 0.0625f, 1, 0.01 * 0.0625f);
				matrixStackIn.scale(scale, scale, scale);

				matrixStackIn.pushPose();
				matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(90));
				// Text Color is RGB, Background Color is ARGB
				this.renderer.font.drawInBatch("Test", 0, 0, 0x00FFFF, false, matrixStackIn.last().pose(), bufferIn, false, 0x00FFFFFF, combinedLightIn);
				matrixStackIn.popPose();

				matrixStackIn.popPose();
			}
			else if (blockFacing == Direction.SOUTH) {
				// Do Lightsaber rendering
				float scale = 0.3f;
				float dimensionMultiplier = 0.0625f * scale; // We first multiply by 1/16 (0.0625), then multiply by the scale value, for correct position value.

				ModelSaber emitterModel = null;
				ModelSaber handleModel = null;
				ModelSaber pommelModel = null;

				if (SABER_MODELS.containsKey(ModelSaber.SaberType.SKYWALKER))
					emitterModel = SABER_MODELS.get(ModelSaber.SaberType.CROSS);
				if (SABER_MODELS.containsKey(ModelSaber.SaberType.WINDU))
					handleModel = SABER_MODELS.get(ModelSaber.SaberType.WINDU);
				if (SABER_MODELS.containsKey(ModelSaber.SaberType.CROSS))
					pommelModel = SABER_MODELS.get(ModelSaber.SaberType.CROSS);

				matrixStackIn.pushPose();
				matrixStackIn.translate(0.5, 1 + 0.0625, 0.5); // default translation
				float lerpAnimTick = tileEntityIn.lastLightsaberAnimTick + (tileEntityIn.lightsaberAnimTick - tileEntityIn.lastLightsaberAnimTick) * partialTicks;
				matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(Math.min(360f/(tileEntityIn.maxLightsaberAnimTick-20) * lerpAnimTick, 360)));

				if (emitterModel != null) {
					matrixStackIn.pushPose();
					Vector3f pos = getLightsaberPartPos(tileEntityIn, ModelSaber.SaberPart.EMITTER, emitterModel, handleModel, pommelModel, dimensionMultiplier, partialTicks);
					matrixStackIn.translate(pos.x(), pos.y(), pos.z());
					renderLightsaberPart(matrixStackIn, bufferIn, combinedLightIn, combinedOverlayIn, emitterModel, ModelSaber.SaberPart.EMITTER, scale);
					matrixStackIn.popPose();
				}

				if (handleModel != null) {
					matrixStackIn.pushPose();
					Vector3f pos = getLightsaberPartPos(tileEntityIn, ModelSaber.SaberPart.HANDLE, emitterModel, handleModel, pommelModel, dimensionMultiplier, partialTicks);
					matrixStackIn.translate(pos.x(), pos.y(), pos.z());
					renderLightsaberPart(matrixStackIn, bufferIn, combinedLightIn, combinedOverlayIn, handleModel, ModelSaber.SaberPart.HANDLE, scale);
					matrixStackIn.popPose();
				}

				if (pommelModel != null) {
					matrixStackIn.pushPose();
					Vector3f pos = getLightsaberPartPos(tileEntityIn, ModelSaber.SaberPart.POMMEL, emitterModel, handleModel, pommelModel, dimensionMultiplier, partialTicks);
					matrixStackIn.translate(pos.x(), pos.y(), pos.z());
					renderLightsaberPart(matrixStackIn, bufferIn, combinedLightIn, combinedOverlayIn, pommelModel, ModelSaber.SaberPart.POMMEL, scale);
					matrixStackIn.popPose();
				}

				matrixStackIn.popPose();
			}
		}
	}

	private void renderLightsaberPart(MatrixStack matrixStack, IRenderTypeBuffer buffer, int combinedLight, int combinedOverlay, ModelSaber saberModel, ModelSaber.SaberPart saberPart, float scale) {
		IVertexBuilder builder = buffer.getBuffer(saberModel.renderType(saberModel.TEX));
		matrixStack.scale(scale, scale, scale);
		matrixStack.mulPose(Vector3f.XP.rotationDegrees(90));

		switch (saberPart) {
			case EMITTER:
				saberModel.renderEmitter(matrixStack, builder, combinedLight, combinedOverlay);
				break;
			case HANDLE:
				saberModel.renderHandle(matrixStack, builder, combinedLight, combinedOverlay);
				break;
			case POMMEL:
				saberModel.renderPommel(matrixStack, builder, combinedLight, combinedOverlay);
				break;
		}
//		saberModel.render(matrixStack, builder, combinedLight, combinedOverlay);
	}

	// We do animation POSITION logic here
	private Vector3f getLightsaberPartPos(ClientDataTileEntity tileEntity, ModelSaber.SaberPart saberPart, @Nullable ModelSaber emitterModel, @Nullable ModelSaber handleModel, @Nullable ModelSaber pommelModel, float dimMultiplier, float partialTicks) {
		float x = 0, z = 0;

		float animTick = tileEntity.lastLightsaberAnimTick + (tileEntity.lightsaberAnimTick - tileEntity.lastLightsaberAnimTick) * partialTicks; // Linear interpolation, 20 ticks = 1 second
//		float maxTick = tileEntity.maxLightsaberAnimTick;

		// make the part fly to 0.5 off the block over 3 seconds
		if (animTick >= 15) {
			float multiplier = animTick - 15;
			tileEntity.lightsaberY = 0.75f/tileEntity.maxLightsaberAnimTick * multiplier;
		}

		switch (saberPart) {
			case EMITTER:
				if (emitterModel == null) break;

				float zRecenter = handleModel != null ? emitterModel.getHeight()*dimMultiplier - (handleModel.getHeight()*dimMultiplier/2f) : emitterModel.getHeight() * dimMultiplier; // move towards the center (0.5). Multiply with dimensionMultiplier for accurate recenter value
				float offset = 0.3f;
				z = zRecenter - offset; // position on the table
				float moveBackZ = offset/(tileEntity.maxLightsaberAnimTick - 20) * animTick;
				z += Math.min(moveBackZ, offset); // move the emitter back to "joined" position

				break;
			case HANDLE:
				if (handleModel == null) break;
				zRecenter = (handleModel.getHeight() * dimMultiplier) / 2f; // This positions the handle at the center of the block, meaning that the center of the handle is ALIGNED to the center of the block
				z = zRecenter;
				break;
			case POMMEL:
				if (pommelModel == null) break;
				offset = 0.2f;
				z = (handleModel != null ? handleModel.getHeight() * dimMultiplier / 2f : 0) + offset; // move to the "separated" position
				moveBackZ = offset/(tileEntity.maxLightsaberAnimTick - 20) * animTick;
				z -= Math.min(moveBackZ, offset); // move the pommel back to "joined" position
				break;
		}

		return new Vector3f(x, tileEntity.lightsaberY, z);
	}
}
