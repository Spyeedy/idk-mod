package com.spyeedy.idkmod.client.renderer.model;

import com.google.common.collect.Lists;
import com.spyeedy.idkmod.common.block.ClientDataBlock;
import com.spyeedy.idkmod.common.utils.BakedQuadHelper;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.FaceBakery;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemOverrideList;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.model.pipeline.BakedQuadBuilder;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Random;

public class BakedModelClientData implements IBakedModel {

	private final TextureAtlasSprite mainTexture = ModelLoader.instance().getSpriteMap().getAtlas(AtlasTexture.LOCATION_BLOCKS).getSprite(new ResourceLocation("minecraft", "block/orange_concrete"));
	private FaceBakery faceBakery = new FaceBakery();

	@Override
	public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand) {
		List<BakedQuad> quads = Lists.newArrayList();

		if (side == null) {
			BakedQuadBuilder builder = new BakedQuadBuilder();
			builder.setQuadOrientation(Direction.NORTH);
			builder.setTexture(mainTexture);

			Direction dir = Direction.NORTH;

			if (state != null && state.hasProperty(ClientDataBlock.FACING)) {
				dir = state.getValue(ClientDataBlock.FACING);
				builder.setQuadOrientation(dir);
			}

			// Back
			BakedQuadHelper.addVertex(builder, mainTexture, dir, Direction.NORTH, 1, 1, 0, 0, 0); // top-left
			BakedQuadHelper.addVertex(builder, mainTexture, dir, Direction.NORTH, 1, 0, 0, 0, 16); // bottom-left
			BakedQuadHelper.addVertex(builder, mainTexture, dir, Direction.NORTH, 0, 0, 0, 16, 16); // bottom-right
			BakedQuadHelper.addVertex(builder, mainTexture, dir, Direction.NORTH, 0, 1, 0, 16, 0); // top-right
			quads.add(builder.build());

			// Left
			builder = new BakedQuadBuilder();
			builder.setQuadOrientation(dir);
			builder.setTexture(mainTexture);
			builder.setApplyDiffuseLighting(false);
			BakedQuadHelper.addVertex(builder, mainTexture, dir, dir.getCounterClockWise(), 0, 1, 0, 0, 0); // top-left
			BakedQuadHelper.addVertex(builder, mainTexture, dir, dir.getCounterClockWise(), 0, 0, 0, 0, 16); // bottom-left
			BakedQuadHelper.addVertex(builder, mainTexture, dir, dir.getCounterClockWise(), 0, 0, 1, 16, 16); // bottom-right
			BakedQuadHelper.addVertex(builder, mainTexture, dir, dir.getCounterClockWise(), 0, 0, 1, 16, 0); // top-right
			quads.add(builder.build());

			// Right
			builder = new BakedQuadBuilder();
			builder.setQuadOrientation(dir);
			builder.setTexture(mainTexture);
			builder.setApplyDiffuseLighting(false);
			BakedQuadHelper.addVertex(builder, mainTexture, dir, dir.getClockWise(), 1, 0, 1, 0, 0); // top-left
			BakedQuadHelper.addVertex(builder, mainTexture, dir, dir.getClockWise(), 1, 0, 1, 0, 16); // bottom-left
			BakedQuadHelper.addVertex(builder, mainTexture, dir, dir.getClockWise(), 1, 0, 0, 16, 16); // bottom-right
			BakedQuadHelper.addVertex(builder, mainTexture, dir, dir.getClockWise(), 1, 1, 0, 16, 0); // top-right
			quads.add(builder.build());
			
			// Top
			builder = new BakedQuadBuilder();
			builder.setQuadOrientation(dir);
			builder.setTexture(mainTexture);
			builder.setApplyDiffuseLighting(false);
			BakedQuadHelper.addVertex(builder, mainTexture, dir, Direction.UP, 0, 1, 0, 0, 0); // top-left
			BakedQuadHelper.addVertex(builder, mainTexture, dir, Direction.UP, 0, 0, 1, 0, 16); // bottom-left
			BakedQuadHelper.addVertex(builder, mainTexture, dir, Direction.UP, 1, 0, 1, 16, 16); // bottom-right
			BakedQuadHelper.addVertex(builder, mainTexture, dir, Direction.UP, 1, 1, 0, 16, 0); // top-right
			quads.add(builder.build());
		}

		return quads;
	}

	@Override
	public boolean useAmbientOcclusion() {
		return true;
	}

	@Override
	public boolean isGui3d() {
		return false;
	}

	@Override
	public boolean usesBlockLight() {
		return false;
	}

	@Override
	public boolean isCustomRenderer() {
		return false;
	}

	@Override
	public TextureAtlasSprite getParticleIcon() {
		return mainTexture;
	}

	@Override
	public ItemOverrideList getOverrides() {
		return null;
	}
}
