package com.spyeedy.idkmod.client.renderer.tileentity;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.spyeedy.idkmod.client.ClientUtils;
import com.spyeedy.idkmod.common.tileentity.ReactorCoreTileEntity;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.math.vector.Quaternion;

import java.util.Random;

public class ReactorCoreTileEntityRenderer extends TileEntityRenderer<ReactorCoreTileEntity> {

	private final Random rand = new Random();
	private int rotation = rand.nextInt(360);

	public ReactorCoreTileEntityRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	@Override
	public void render(ReactorCoreTileEntity tileEntityIn, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
		rotation++;

		float rot = rotation;

		matrixStackIn.pushPose();
		Matrix4f matrix = matrixStackIn.last().pose();

		float pixel = 1/16f;
		int red = 255;
		int green = 255;
		int blue = 255;
		int alpha = (int) (0.2 * 255);
		int lightmap = 15728640;

		/* Draw glowing, crazy rotating Cube */
		// We pushPose matrix and pop matrix once the rotated part is done rendering, to end the rotation
		matrixStackIn.pushPose();
		Matrix4f matrix1 = matrixStackIn.last().pose();

		matrixStackIn.translate(0.5, 0.5, 0.5);
		IVertexBuilder vertexBuilder = bufferIn.getBuffer(ClientUtils.RenderTypes.GLOWING_SOLID);
		matrixStackIn.mulPose(new Quaternion(rot, 0, rot * 2, true));

		float length = 1.5f * pixel;
		ClientUtils.renderFace(vertexBuilder, matrix1, Direction.Axis.X, -length, -length, length, -length, length, red, 0, 0, 255, lightmap); // Red North
		ClientUtils.renderFace(vertexBuilder, matrix1, Direction.Axis.Z, length, -length, length, -length, length, 0, green, 0, 255, lightmap); // Green East
		ClientUtils.renderFace(vertexBuilder, matrix1, Direction.Axis.X, length, length, -length, -length, length, 0, 0, blue, 255, lightmap); // Blue South
		ClientUtils.renderFace(vertexBuilder, matrix1, Direction.Axis.Z, -length, length, -length, -length, length, red, green, 0, 255, lightmap); // Yellow West
		ClientUtils.renderFace(vertexBuilder, matrix1, Direction.Axis.Y, length, -length, length, -length, length, red, 0, blue, 255, lightmap); // Purple Up
		ClientUtils.renderFace(vertexBuilder, matrix1, Direction.Axis.Y, -length, -length, length, -length, length, 0, green, blue, 255, lightmap); // Cyan Down
		matrixStackIn.popPose();
		/* End of crazy rotating Cube */

		/* Draw Lightsaber */
		matrixStackIn.pushPose();
		matrix1 = matrixStackIn.last().pose();
		vertexBuilder = bufferIn.getBuffer(ClientUtils.RenderTypes.GLOWING_TRANSLUCENT);

		matrixStackIn.translate(0.5, 1.5, 0.5); // translate for white core
		ClientUtils.renderBox(matrix1, vertexBuilder, 0.2f, 0.2f, 0.9f, red, green, blue, 255, lightmap, Direction.NORTH, Direction.WEST, Direction.EAST, Direction.SOUTH, Direction.UP, Direction.DOWN);

		matrixStackIn.translate(0, -0.05f, 0); // translate for red outline
		ClientUtils.renderBox(matrix1, vertexBuilder, 0.3f, 0.3f, 1f, red, 0, 0, 255, lightmap, Direction.NORTH, Direction.WEST, Direction.EAST, Direction.SOUTH, Direction.UP, Direction.DOWN);
		matrixStackIn.popPose();
		/* End of lightsaber */

		/* Draw line to player eye */
		PlayerEntity closestPlayer = tileEntityIn.getLevel().getNearestPlayer(tileEntityIn.getBlockPos().getX(), tileEntityIn.getBlockPos().getY(), tileEntityIn.getBlockPos().getZ(), 2, false);
		if (tileEntityIn.hasLevel() && closestPlayer != null) {
			matrixStackIn.pushPose();
			matrix1 = matrixStackIn.last().pose();
			vertexBuilder = bufferIn.getBuffer(ClientUtils.RenderTypes.getGlowingLine(5));

			float posX = (float) (closestPlayer.getX() - tileEntityIn.getBlockPos().getX());
			float posY = (float) (closestPlayer.getY() - tileEntityIn.getBlockPos().getY() + closestPlayer.getEyeHeight());
			float posZ = (float) (closestPlayer.getZ() - tileEntityIn.getBlockPos().getZ());

			matrixStackIn.translate(0.5, 0.5, 0.5); // translate for white core

			vertexBuilder.vertex(matrix1, 0, 0, 0).color(1, 0.3f, 0, 1f).uv2(lightmap).endVertex();
			vertexBuilder.vertex(matrix1, posX - 0.5f, posY - 0.5f, posZ - 0.5f).color(1, 0.3f, 0, 1f).uv2(lightmap).endVertex();

			matrixStackIn.popPose();
		}

		matrixStackIn.pushPose();
		matrix1 = matrixStackIn.last().pose();
		vertexBuilder = bufferIn.getBuffer(ClientUtils.RenderTypes.GLOWING_SOLID);
		matrixStackIn.translate(-0.5, 0, 0);

		vertexBuilder.vertex(matrix1, 0, 0, 0).color(0.78f, 0.36f, 0.78f, alpha).uv2(lightmap).endVertex();
		vertexBuilder.vertex(matrix1, 0.5f, 0, 0).color(0.78f, 0.36f, 0.78f, alpha).uv2(lightmap).endVertex();
		vertexBuilder.vertex(matrix1, 0.5f, 0, 0).color(0.78f, 0.36f, 0.78f, alpha).uv2(lightmap).endVertex();
		vertexBuilder.vertex(matrix1, 0.5f, 0.5f, 0).color(0.78f, 0.36f, 0.78f, alpha).uv2(lightmap).endVertex();

		matrixStackIn.popPose();

		/* Draw "glass" on all sides. For Horizontal sides starting from facing side's bot-right -> top-right -> top-left -> bot-left. For Top side, facing south, bottom is northwards, top is southwards. For Bottom side, facing south, top is northwards, bottom is southwards*/
		vertexBuilder = bufferIn.getBuffer(ClientUtils.RenderTypes.TRANSLUCENT);

		ClientUtils.renderFace(vertexBuilder, matrix, Direction.Axis.X, pixel/2, pixel, 15 * pixel, pixel, 15 * pixel, red, green, blue, alpha, 0); // North
		ClientUtils.renderFace(vertexBuilder, matrix, Direction.Axis.Z, 15.5f * pixel, pixel, 15 * pixel, pixel, 15 * pixel, red, green, blue, alpha, 0); // East
		ClientUtils.renderFace(vertexBuilder, matrix, Direction.Axis.X, 15.5f * pixel, 15 * pixel, pixel, pixel, 15 * pixel, red, green, blue, alpha, 0); // South
		ClientUtils.renderFace(vertexBuilder, matrix, Direction.Axis.Z, pixel/2, 15 * pixel, pixel, pixel, 15 * pixel, red, green, blue, alpha, 0); // West
		ClientUtils.renderFace(vertexBuilder, matrix, Direction.Axis.Y, 15.5f * pixel, pixel, 15 * pixel, pixel, 15 * pixel, red, green, blue, alpha, 0); // Top
		ClientUtils.renderFace(vertexBuilder, matrix, Direction.Axis.Y, pixel/2, 15 * pixel, pixel, pixel, 15 * pixel, red, green, blue, alpha, 0); // Down

		matrixStackIn.popPose();
	}
}
