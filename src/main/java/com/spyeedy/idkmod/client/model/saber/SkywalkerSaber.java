package com.spyeedy.idkmod.client.model.saber;//Made with Blockbench by dhi_awesome
//Paste this code into your mod.

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.spyeedy.idkmod.common.IdkMod;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;

public class SkywalkerSaber extends ModelSaber {

	public ModelRenderer lightsaber;
	private ModelRenderer endcap;
	private ModelRenderer blackbars;
	private ModelRenderer handle;
	private ModelRenderer sideswitch;
	private ModelRenderer switchbump;
	private ModelRenderer button;
	private ModelRenderer dots;
	private ModelRenderer releaseknob;
	private ModelRenderer emitter;
	private ModelRenderer clip;

	public SkywalkerSaber() {
		super(RenderType::entityCutoutNoCull, new ResourceLocation(IdkMod.MODID, "textures/item/skywalker_saber.png"));
		texWidth = 64;
		texHeight = 64;

		lightsaber = new ModelRenderer(this);
		lightsaber.setPos(0.0F, 24.0F, 0.0F);

		endcap = new ModelRenderer(this);
		endcap.setPos(0.0F, 0.0F, 0.0F);
		lightsaber.addChild(endcap);
		endcap.texOffs(12, 12).addBox( -2.0F, -0.5F, -2.0F, 4, 9, 4, 0.0F, false);

		blackbars = new ModelRenderer(this);
		blackbars.setPos(0.0F, 5.5F, 0.0F);
		setRotationAngle(blackbars, 0.0F, -0.7854F, 0.0F);
		endcap.addChild(blackbars);
		blackbars.texOffs(16, 0).addBox(-2.5F, -4.5F, -1.0F, 5, 7, 2, 0.0F, false);
		blackbars.texOffs(0, 22).addBox( -1.0F, -4.5F, -2.5F, 2, 7, 5, 0.0F, false);

		handle = new ModelRenderer(this);
		handle.setPos(0.0F, 0.0F, 0.0F);
		lightsaber.addChild(handle);
		handle.texOffs(24, 11).addBox( -2.0F, -1.0F, -2.0F, 4, 1, 4, 0.125F, false);
		handle.texOffs(28, 16).addBox( -2.0F, -4.0F, -2.0F, 4, 1, 4, 0.125F, false);
		handle.texOffs(0, 0).addBox( -2.0F, -12.5F, -2.0F, 4, 12, 4, 0.0F, false);

		sideswitch = new ModelRenderer(this);
		sideswitch.setPos(0.0F, 0.0F, 0.0F);
		handle.addChild(sideswitch);
		sideswitch.texOffs(30, 4).addBox( -3.0F, -4.0F, -1.0F, 1, 4, 2, 0.0F, false);
		sideswitch.texOffs(12, 0).addBox( -3.0625F, -2.8125F, -1.5F, 1, 2, 1, -0.1875F, false);

		switchbump = new ModelRenderer(this);
		switchbump.setPos(0.0F, 0.0F, 0.0F);
		setRotationAngle(switchbump, 0.1745F, 0.0F, 0.0F);
		sideswitch.addChild(switchbump);
		switchbump.texOffs(0, 0).addBox( -3.0625F, -3.8248F, -1.0966F, 1, 2, 1, -0.1875F, false);

		button = new ModelRenderer(this);
		button.setPos(0.0F, 0.0F, 0.125F);
		handle.addChild(button);
		button.texOffs(5, 16).addBox( -1.0F, -11.3125F, -2.4375F, 2, 2, 1, 0.0F, false);
		button.texOffs(28, 21).addBox( -1.0F, -11.3125F, -2.9375F, 2, 2, 1, -0.125F, false);

		dots = new ModelRenderer(this);
		dots.setPos(1.0F, 0.0F, 0.125F);
		setRotationAngle(dots, -0.7854F, 0.0F, 0.0F);
		handle.addChild(dots);
		dots.texOffs(8, 19).addBox( 0.125F, -7.8042F, -10.525F, 1, 1, 1, -0.25F, false);
		dots.texOffs(24, 11).addBox( -3.125F, -7.8042F, -10.525F, 1, 1, 1, -0.25F, false);
		dots.texOffs(0, 21).addBox( 0.125F, -7.4506F, -10.1715F, 1, 1, 1, -0.25F, false);
		dots.texOffs(0, 23).addBox( -3.125F, -7.4506F, -10.1715F, 1, 1, 1, -0.25F, false);

		releaseknob = new ModelRenderer(this);
		releaseknob.setPos(0.0F, 0.0F, 0.0F);
		handle.addChild(releaseknob);
		releaseknob.texOffs(30, 0).addBox( -1.0F, -12.5F, 1.375F, 2, 2, 2, -0.375F, false);
		releaseknob.texOffs(34, 21).addBox( -1.0F, -12.5F, 2.0625F, 2, 2, 1, -0.25F, false);

		emitter = new ModelRenderer(this);
		emitter.setPos(0.0F, 0.0F, 0.0F);
		lightsaber.addChild(emitter);
		emitter.texOffs(24, 24).addBox( -2.0F, -15.5F, -2.0F, 4, 3, 4, 0.0F, false);
		emitter.texOffs(0, 16).addBox( 1.25F, -16.625F, -0.75F, 1, 2, 3, -0.25F, false);
		emitter.texOffs(14, 27).addBox( -2.0F, -17.0F, 1.25F, 4, 2, 1, -0.25F, false);
		emitter.texOffs(14, 30).addBox( -2.25F, -16.625F, -0.75F, 1, 2, 3, -0.25F, false);

		clip = new ModelRenderer(this);
		clip.setPos(0.0F, 0.0F, 0.0F);
		emitter.addChild(clip);
		clip.texOffs(22, 31).addBox( -1.0F, -16.25F, 1.75F, 2, 4, 1, -0.2375F, false);
		clip.texOffs(28, 31).addBox( -1.0F, -16.125F, 2.25F, 2, 3, 1, -0.25F, false);
		clip.texOffs(12, 35).addBox( 0.125F, -16.25F, 2.125F, 1, 2, 2, -0.375F, false);
		clip.texOffs(32, 33).addBox( -1.125F, -16.25F, 2.125F, 1, 2, 2, -0.375F, false);
	}

	@Override
	public void renderHandle(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay) {
		bindTex();
		handle.render(matrixStack, buffer, packedLight, packedOverlay);
	}
	@Override
	public void renderEmitter(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay) {
		bindTex();
		emitter.render(matrixStack, buffer, packedLight, packedOverlay);
	}
	@Override
	public void renderPommel(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay) {
		bindTex();
		endcap.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay){
		lightsaber.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	@Override
	public float getHeight() {
		return 12.5f;
	}

	@Override
	public float getEmitterHeight() {
		return 2.5f;
	}

	@Override
	public void renderToBuffer(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) {

	}
}