package com.spyeedy.idkmod.client.model.saber;//Made with Blockbench
//Paste this code into your mod.

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.spyeedy.idkmod.common.IdkMod;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;

public class WinduSaber extends ModelSaber {
	private final ModelRenderer lightsaber;
	private final ModelRenderer endcap;
	private final ModelRenderer emitter;
	private final ModelRenderer handle;
	private final ModelRenderer bars;

	public WinduSaber() {
		super(RenderType::entityCutoutNoCull, new ResourceLocation(IdkMod.MODID, "textures/item/windu_saber.png"));
		texWidth = 64;
		texHeight = 64;

		lightsaber = new ModelRenderer(this);
		lightsaber.setPos(0.0F, 24.0F, 0.0F);

		endcap = new ModelRenderer(this);
		endcap.setPos(0.0F, 0.0F, 0.0F);
		lightsaber.addChild(endcap);
		endcap.texOffs(17, 0).addBox( -2.0F, 0.6F, -2.0F, 4, 2, 4, 0.0F, false);
		endcap.texOffs(0, 22).addBox( 1.3F, 1.0F, -0.5F, 1, 1, 1, -0.125F, false);
		endcap.texOffs(5, 22).addBox( 2.1F, 1.0F, -0.5F, 1, 1, 1, 0.0F, false);
		endcap.texOffs(17, 7).addBox( -2.0F, 3.85F, -2.0F, 4, 1, 4, 0.0F, false);
		endcap.texOffs(18, 14).addBox( -1.0F, 5.55F, -1.0F, 2, 1, 2, 0.0F, false);
		endcap.texOffs(0, 0).addBox( -2.0F, -0.125F, -2.0F, 4, 1, 4, -0.125F, false);
		endcap.texOffs(0, 7).addBox( -2.0F, 2.075F, -2.0F, 4, 2, 4, -0.125F, false);
		endcap.texOffs(0, 14).addBox( -2.0F, 4.575F, -2.0F, 4, 2, 4, -0.125F, false);

		emitter = new ModelRenderer(this);
		emitter.setPos(0.0F, 0.0F, 0.0F);
		lightsaber.addChild(emitter);
		emitter.texOffs(0, 33).addBox( -2.0F, -12.75F, -2.0F, 4, 1, 4, 0.0F, false);
		emitter.texOffs(0, 40).addBox( -2.0F, -16.625F, -2.0F, 4, 4, 4, -0.125F, false);
		emitter.texOffs(0, 40).addBox( -2.0F, -16.625F, -2.0F, 4, 1, 4, 0.0F, false);
		emitter.texOffs(34, 15).addBox( -2.0F, -16.625F, -1.0F, 4, 4, 3, 0.0F, false);
		emitter.texOffs(0, 49).addBox( -1.5F, -14.925F, -2.1F, 1, 2, 1, -0.125F, false);
		emitter.texOffs(0, 49).addBox( -0.5F, -14.925F, -2.1F, 1, 2, 1, -0.125F, false);
		emitter.texOffs(0, 55).addBox( -1.5F, -15.825F, -2.1F, 3, 1, 1, -0.125F, false);
		emitter.texOffs(0, 49).addBox( 0.5F, -14.925F, -2.1F, 1, 2, 1, -0.125F, false);
		emitter.texOffs(18, 14).addBox( -1.0F, -16.85F, -1.0F, 2, 1, 2, 0.0F, false);

		handle = new ModelRenderer(this);
		handle.setPos(0.0F, 0.0F, 0.0F);
		lightsaber.addChild(handle);
		handle.texOffs(34, 0).addBox( -2.0F, -9.0F, -2.0F, 4, 9, 4, 0.0F, false);
		handle.texOffs(0, 25).addBox( -2.0F, -11.875F, -2.0F, 4, 3, 4, -0.125F, false);
		handle.texOffs(0, 25).addBox( -2.0F, -10.125F, -2.0F, 4, 1, 4, 0.0F, false);
		handle.texOffs(0, 25).addBox( -2.0F, -11.375F, -2.0F, 4, 1, 4, 0.0F, false);

		bars = new ModelRenderer(this);
		bars.setPos(0.0F, 0.0F, 0.0F);
		setRotationAngle(bars, 0.0F, 0.7854F, 0.0F);
		handle.addChild(bars);
		bars.texOffs(52, 0).addBox( -1.0F, -8.5F, -2.5F, 2, 8, 2, 0.0F, false);
		bars.texOffs(52, 0).addBox( -1.0F, -8.5F, 0.5F, 2, 8, 2, 0.0F, false);
		bars.texOffs(52, 0).addBox( -2.5F, -8.5F, -1.0F, 2, 8, 2, 0.0F, false);
		bars.texOffs(52, 0).addBox( 0.5F, -8.5F, -1.0F, 2, 8, 2, 0.0F, false);
	}

	@Override
	public void renderHandle(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay) {
		bindTex();
		handle.render(matrixStack, buffer, packedLight, packedOverlay);
	}
	@Override
	public void renderEmitter(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay) {
		bindTex();
		emitter.render(matrixStack, buffer, packedLight, packedOverlay);
	}
	@Override
	public void renderPommel(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay) {
		bindTex();
		endcap.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay){
		lightsaber.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	@Override
	public float getHeight() {
		return 11.75f;
	}

	@Override
	public float getEmitterHeight() {
		return 5f;
	}

	@Override
	public void renderToBuffer(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) {

	}
}