package com.spyeedy.idkmod.client.model.saber;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;

import java.util.function.Function;

public abstract class ModelSaber extends Model {

    public ModelRenderer lightsaber;
    public ResourceLocation TEX;
    public Minecraft mc = Minecraft.getInstance();

    public ModelSaber(Function<ResourceLocation, RenderType> renderTypeIn, ResourceLocation saberTexLoc) {
        super(renderTypeIn);
        this.TEX = saberTexLoc;
    }

    public void renderHandle(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay) {}
    public void renderEmitter(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay) {}
    public void renderPommel(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay) {}
    public void bindTex() {
        mc.getTextureManager().bind(TEX);
    }
    public float getHeight() { //Handle Height
        return 0;
    }
    public float getEmitterHeight() { //Emitter Height (from base to where the blade starts)
        return 0;
    }

    public float getCrossGuardOffset() { //Height difference between emitter height and where to position the cross blades
        return 0;
    }

    public abstract void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn);

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.xRot = x;
        modelRenderer.yRot = y;
        modelRenderer.zRot = z;
    }

    public enum SaberType {
    	CROSS, SKYWALKER, WINDU
    }

    public enum SaberPart {
    	EMITTER, HANDLE, POMMEL
    }
}
