// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports
package com.spyeedy.idkmod.client.model.saber;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.spyeedy.idkmod.common.IdkMod;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;

public class CrossSaber extends ModelSaber {
	private ModelRenderer lightsaber;
	private ModelRenderer endcap;
	private ModelRenderer handle;
	private ModelRenderer rings;
	private ModelRenderer buttons;
	private ModelRenderer emitter;
	private ModelRenderer vents;
	private ModelRenderer ventw;
	private ModelRenderer vente;
	private ModelRenderer emitterRing;

	public CrossSaber() {
		super(RenderType::entityCutoutNoCull, new ResourceLocation(IdkMod.MODID, "textures/item/cross_saber.png"));
		texWidth = 64;
		texHeight = 64;

		lightsaber = new ModelRenderer(this);
		lightsaber.setPos(0.0F, 24.0F, 0.0F);

		endcap = new ModelRenderer(this);
		endcap.setPos(0.0F, -0.125F, 0.0F);
		lightsaber.addChild(endcap);
		endcap.texOffs(0, 33).addBox(-2.0F, 1.875F, -2.0F, 4.0F, 1.0F, 4.0F, 0.0F, false);
		endcap.texOffs(24, 18).addBox(-2.0F, 3.625F, -2.0F, 4.0F, 1.0F, 4.0F, 0.0F, false);
		endcap.texOffs(12, 13).addBox(-2.0F, 0.0F, -2.0F, 4.0F, 5.0F, 4.0F, -0.125F, false);

		handle = new ModelRenderer(this);
		handle.setPos(0.0F, 0.0F, 0.0F);
		lightsaber.addChild(handle);
		handle.texOffs(0, 17).addBox(-1.5F, -9.0F, -1.5F, 3.0F, 8.0F, 3.0F, 0.0F, false);
		handle.texOffs(16, 6).addBox(-2.0F, -2.875F, -2.0F, 4.0F, 3.0F, 4.0F, -0.125F, false);

		rings = new ModelRenderer(this);
		rings.setPos(2.0F, -6.9F, -2.0F);
		handle.addChild(rings);
		rings.texOffs(32, 32).addBox(-4.0F, -2.0F, 0.0F, 4.0F, 1.0F, 4.0F, -0.125F, false);
		rings.texOffs(20, 29).addBox(-4.0F, -1.0F, 0.0F, 4.0F, 1.0F, 4.0F, -0.125F, false);
		rings.texOffs(28, 10).addBox(-4.0F, 0.0F, 0.0F, 4.0F, 1.0F, 4.0F, -0.125F, false);
		rings.texOffs(28, 5).addBox(-4.0F, 1.0F, 0.0F, 4.0F, 1.0F, 4.0F, -0.125F, false);
		rings.texOffs(8, 28).addBox(-4.0F, 2.0F, 0.0F, 4.0F, 1.0F, 4.0F, -0.125F, false);
		rings.texOffs(26, 0).addBox(-4.0F, 3.0F, 0.0F, 4.0F, 1.0F, 4.0F, -0.125F, false);

		buttons = new ModelRenderer(this);
		buttons.setPos(0.0F, 0.0F, 0.0F);
		handle.addChild(buttons);
		buttons.texOffs(0, 8).addBox(-0.5F, -2.5F, -2.1F, 1.0F, 1.0F, 1.0F, -0.1F, false);
		buttons.texOffs(0, 6).addBox(-0.5F, -1.25F, -2.1F, 1.0F, 1.0F, 1.0F, -0.1F, false);

		emitter = new ModelRenderer(this);
		emitter.setPos(0.0F, 0.0F, 0.0F);
		lightsaber.addChild(emitter);
		emitter.texOffs(0, 6).addBox(-2.0F, -16.0F, -2.0F, 4.0F, 7.0F, 4.0F, 0.0F, false);
		emitter.texOffs(12, 22).addBox(-2.0F, -13.25F, -2.0F, 4.0F, 2.0F, 4.0F, 0.125F, false);
		emitter.texOffs(24, 24).addBox(-2.0F, -10.5F, -2.0F, 4.0F, 1.0F, 4.0F, 0.125F, false);
		emitter.texOffs(16, 34).addBox(-1.5F, -16.35F, -1.5F, 3.0F, 1.0F, 3.0F, 0.0F, false);

		vents = new ModelRenderer(this);
		vents.setPos(3.0F, 0.0F, 0.5F);
		emitter.addChild(vents);
		vents.texOffs(0, 0).addBox(-8.0F, -15.5F, -2.0F, 10.0F, 3.0F, 3.0F, 0.0F, false);

		ventw = new ModelRenderer(this);
		ventw.setPos(0.0F, 0.0F, 0.0F);
		vents.addChild(ventw);
		ventw.texOffs(24, 0).addBox(2.25F, -12.5F, -1.5F, 0.0F, 0.0F, 2.0F, 0.25F, false);
		ventw.texOffs(24, 0).addBox(2.25F, -15.5F, -1.5F, 0.0F, 0.0F, 2.0F, 0.25F, false);
		ventw.texOffs(24, 0).addBox(2.25F, -15.5F, 1.0F, 0.0F, 3.0F, 0.0F, 0.25F, false);
		ventw.texOffs(24, 0).addBox(2.25F, -15.5F, -2.0F, 0.0F, 3.0F, 0.0F, 0.25F, false);

		vente = new ModelRenderer(this);
		vente.setPos(-6.0F, 0.0F, 0.0F);
		vents.addChild(vente);
		vente.texOffs(24, 0).addBox(-2.25F, -12.5F, -1.5F, 0.0F, 0.0F, 2.0F, 0.25F, false);
		vente.texOffs(24, 0).addBox(-2.25F, -15.5F, -1.5F, 0.0F, 0.0F, 2.0F, 0.25F, false);
		vente.texOffs(24, 0).addBox(-2.25F, -15.5F, -2.0F, 0.0F, 3.0F, 0.0F, 0.25F, false);
		vente.texOffs(24, 0).addBox(-2.25F, -15.5F, 1.0F, 0.0F, 3.0F, 0.0F, 0.25F, false);

		emitterRing = new ModelRenderer(this);
		emitterRing.setPos(0.0F, 0.0F, 0.0F);
		emitter.addChild(emitterRing);
		emitterRing.texOffs(36, 15).addBox(1.0F, -17.0F, -2.0F, 1.0F, 1.0F, 4.0F, -0.25F, false);
		emitterRing.texOffs(24, 34).addBox(-2.0F, -17.0F, -2.0F, 1.0F, 1.0F, 4.0F, -0.25F, false);
		emitterRing.texOffs(12, 8).addBox(-1.5F, -17.0F, -2.0F, 3.0F, 1.0F, 1.0F, -0.25F, false);
		emitterRing.texOffs(12, 6).addBox(-1.5F, -17.0F, 1.0F, 3.0F, 1.0F, 1.0F, -0.25F, false);
	}

	@Override
	public void renderHandle(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay) {
		bindTex();
		handle.render(matrixStack, buffer, packedLight, packedOverlay);
	}
	@Override
	public void renderEmitter(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay) {
		bindTex();
		emitter.render(matrixStack, buffer, packedLight, packedOverlay);
	}
	@Override
	public void renderPommel(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay) {
		bindTex();
		endcap.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay){
		lightsaber.render(matrixStack, buffer, packedLight, packedOverlay);
	}
	@Override
	public void bindTex() {
		mc.getTextureManager().bind(TEX);
	}

	@Override
	public float getHeight() {
		return 9f;
	}

	@Override
	public float getEmitterHeight() {
		return 7.75f;
	}

	@Override
	public float getCrossGuardOffset() {
		return 2.75f;
	}

	@Override
	public void renderToBuffer(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) {

	}
}