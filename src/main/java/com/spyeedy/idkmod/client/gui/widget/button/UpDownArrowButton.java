package com.spyeedy.idkmod.client.gui.widget.button;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.chat.NarratorChatListener;

public class UpDownArrowButton extends IdkModButton {

	private boolean isExpanded;

	public UpDownArrowButton(IPressable pressedAction) {
		this(0, 0, false, pressedAction);
	}

	public UpDownArrowButton(int x, int y, boolean isUpArrow, IPressable pressedAction) {
		super(x, y, 8, 6, NarratorChatListener.NO_TITLE, pressedAction);
		this.isExpanded = isUpArrow;
	}

	@Override
	public void renderBg(MatrixStack matrixStack, Minecraft mc, int mouseX, int mouseY) {
		int magicX = this.isExpanded ? 1 : 0;

		mc.getTextureManager().bind(TEX);
		this.blit(matrixStack, this.x, this.y, magicX * this.width, getYImage(isHovered) * this.height, this.width, this.height);
	}

	public boolean isExpanded() {
		return isExpanded;
	}

	public void setExpanded() {
		this.isExpanded = !isExpanded;
	}
}
