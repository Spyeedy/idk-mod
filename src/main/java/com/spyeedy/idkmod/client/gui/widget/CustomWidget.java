package com.spyeedy.idkmod.client.gui.widget;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FocusableGui;
import net.minecraft.client.gui.IGuiEventListener;
import net.minecraft.client.gui.IRenderable;
import net.minecraft.client.gui.screen.IScreen;

import java.util.List;

public abstract class CustomWidget extends FocusableGui implements IRenderable, IScreen {

	public boolean visible = true;
	protected static final Minecraft minecraft = Minecraft.getInstance();

	private final List<IGuiEventListener> children = Lists.newArrayList();

	@Override
	public List<? extends IGuiEventListener> children() {
		return children;
	}

	public IGuiEventListener addListener(IGuiEventListener listener) {
		this.children.add(listener);

		return listener;
	}

	@Override
	public void tick() {
		if (!visible) return;
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		for (IGuiEventListener listener : children) {
			if (listener instanceof IRenderable) ((IRenderable) listener).render(matrixStack, mouseX, mouseY, partialTicks);
		}
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) {
		if (!visible) return false;
		return super.mouseClicked(mouseX, mouseY, button);
	}
}
