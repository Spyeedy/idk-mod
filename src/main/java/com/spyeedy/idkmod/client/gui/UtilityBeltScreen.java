package com.spyeedy.idkmod.client.gui;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.spyeedy.idkmod.client.ClientUtils;
import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.container.UtilityBeltContainer;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

import java.awt.*;

public class UtilityBeltScreen extends ContainerScreen<UtilityBeltContainer> {

	private static final ResourceLocation TEX_GUI = new ResourceLocation(IdkMod.MODID, "textures/gui/utility_belt_inventory.png");

	public UtilityBeltScreen(UtilityBeltContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
		super(screenContainer, inv, titleIn);
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		super.render(matrixStack, mouseX, mouseY, partialTicks);
		this.renderTooltip(matrixStack, mouseX, mouseY);
	}

	@Override
	protected void renderLabels(MatrixStack matrixStack, int x, int y) {}

	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY) {
		renderBackground(matrixStack);

		int i = (this.width - imageWidth)/2;
		int j = (this.height - imageHeight)/2;

		// because the "x" and "y" root, is based on width 176 and height 166, these 2 x & y root values cannot be modified.
		// so care must be taken with the x and y values here
		int yPos = j + menu.yPos;

		int inRadius = menu.inRadius;
		int outRadius = menu.outRadius;

		matrixStack.pushPose();
		matrixStack.translate(this.width/2f, yPos, 0);
		ClientUtils.drawPolygon(matrixStack, inRadius, outRadius, 1, 12, mouseX, mouseY, Color.BLACK,
				(mStack, startAngle, endAngle, inRadius1, outRadius1) -> ClientUtils.drawPolygonSector(mStack, inRadius1, outRadius1, startAngle, endAngle, 90, 90, 90, 200));
		matrixStack.popPose();

		minecraft.getTextureManager().bind(TEX_GUI);
		// draw item slot
		int drawSize = 18;
		for (int slotIdx = 0; slotIdx < 12; slotIdx++) {
			Slot slot = this.menu.getSlot(slotIdx);
			this.blit(matrixStack, i + slot.x - 1, j + slot.y - 1, 7, 7, drawSize, drawSize); // offset by 1, because we are drawing the slot (18x), not the item (16x)
		}

		yPos += outRadius + 4; // move down yPos from center of polygon, for the top of inventory interface

		this.blit(matrixStack, i, yPos, 0, 0, imageWidth, 90);
	}
}
