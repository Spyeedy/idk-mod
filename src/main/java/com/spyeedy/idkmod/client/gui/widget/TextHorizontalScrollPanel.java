package com.spyeedy.idkmod.client.gui.widget;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import com.spyeedy.idkmod.client.ClientUtils;
import com.spyeedy.idkmod.client.gui.widget.button.TinyButton;
import net.minecraft.client.gui.widget.button.Button;

import java.awt.*;

public class TextHorizontalScrollPanel extends CustomScrollPanel {

	private String textContent;
	private final Color textColor;

	private Button leftArrowBtn;
	private Button rightArrowBtn;

	private int scrollX = 0;

	public TextHorizontalScrollPanel(int x, int y, int width, int height, String textContent) {
		this(x, y, width, height, textContent, Color.WHITE);
	}

	public TextHorizontalScrollPanel(int x, int y, int width, int height, String textContent, Color textColor) {
		super(x, y, width, height);
		this.textContent = textContent;
		this.textColor = textColor;

		this.addListener(this.leftArrowBtn = new TinyButton(x + width - 18, y + height + 3, TinyButton.Icon.LEFT_ARROW, (button) -> {
			this.scrollX -= getScrollAmount();
			this.applyScrollLimits();
		}));

		this.addListener(this.rightArrowBtn = new TinyButton(x + width - 7, y + height + 3, TinyButton.Icon.RIGHT_ARROW, (button) -> {
			this.scrollX += getScrollAmount();
			this.applyScrollLimits();
		}));
	}

	@Override
	public void tick() {
		super.tick();

		if (!this.visible || this.textContent.equals("")) return;

		this.leftArrowBtn.active = canScroll() && scrollX > 0;
		this.rightArrowBtn.active = canScroll() && scrollX + width < getContentLength();
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		if (!this.visible || this.textContent.equals("")) return;

		double scale = ClientUtils.minecraft.getWindow().getGuiScale();
		RenderSystem.pushMatrix();
		RenderSystem.enableScissor((int) ((x + 1) * scale), (int)(ClientUtils.minecraft.getWindow().getHeight() - ((y + 1 + height) * scale)),
				(int) (width * scale), (int) ((height) * scale));

		minecraft.font.draw(matrixStack, textContent, x + 1 - scrollX, y + 1, this.textColor.getRGB());

		RenderSystem.disableScissor();
		RenderSystem.popMatrix();

		super.render(matrixStack, mouseX, mouseY, partialTicks);
	}

	@Override
	public boolean mouseScrolled(double mouseX, double mouseY, double delta) {
		if (!this.visible || this.textContent.equals("")) return false;

		if (canScroll() && mouseX >= this.x && mouseX <= this.x + width && mouseY >= this.y && mouseY <= this.y + height) {
			scrollX += -delta * getScrollAmount();
			applyScrollLimits();
		}
		return false;
	}

	@Override
	protected boolean canScroll() {
		return getContentLength() > width;
	}

	public void setTextContent(String textContent) {
		this.textContent = textContent;
		this.scrollX = 0;
	}

	@Override
	protected void applyScrollLimits() {
		if (scrollX <= 0) scrollX = 0;
		else if (scrollX + width >= getContentLength()) scrollX = getContentLength() - width;
	}

	@Override
	protected int getScrollAmount() {
		return 20;
	}

	@Override
	protected int getContentLength() {
		return ClientUtils.fontRenderer.width(textContent);
	}
}
