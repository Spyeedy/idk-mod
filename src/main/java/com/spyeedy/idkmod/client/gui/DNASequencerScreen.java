package com.spyeedy.idkmod.client.gui;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.container.DNASequencerContainer;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

public class DNASequencerScreen extends ContainerScreen<DNASequencerContainer> {

	public static final ResourceLocation TEX_GUI = new ResourceLocation(IdkMod.MODID, "textures/gui/dna_sequencer.png");
	private static final int DNA_WIDTH = 40;
	private static final int DNA_HEIGHT = 7;
	public int dnaAnimationProgress = 0;

	public DNASequencerScreen(DNASequencerContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
		super(screenContainer, inv, titleIn);
	}

	@Override
	protected void renderLabels(MatrixStack matrixStack, int x, int y) {}

	@Override
	public void tick() {
		super.tick();

		if (dnaAnimationProgress < DNA_WIDTH/2) {
			dnaAnimationProgress++;
		} else {
			dnaAnimationProgress = 0;
		}
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		super.render(matrixStack, mouseX, mouseY, partialTicks);
		this.renderTooltip(matrixStack, mouseX, mouseY);
	}

	@Override
	protected void renderBg(MatrixStack matrixStack, float partialTicks, int x, int y) {
		renderBackground(matrixStack);
		this.minecraft.getTextureManager().bind(TEX_GUI);
		RenderSystem.color3f(1, 1, 1);

		int i = (this.width - this.imageWidth)/2;
		int j = (this.height - this.imageHeight)/2;

		this.blit(matrixStack, i, j, 0, 0, imageWidth, imageHeight);

		int actualDNALength = DNA_WIDTH/2;
		int dnaPosX = i + this.imageWidth/2 - actualDNALength;
		int dnaPosY = j + 26;

//		this.blit(matrixStack, dnaPosX, dnaPosY, 0, imageHeight, DNA_WIDTH, DNA_HEIGHT);

		RenderSystem.color4f(1, 0, 0, 1);
		this.blit(matrixStack, dnaPosX, dnaPosY, actualDNALength - dnaAnimationProgress, imageHeight, actualDNALength, DNA_HEIGHT);
		this.blit(matrixStack, dnaPosX + actualDNALength, dnaPosY, DNA_WIDTH/2 - dnaAnimationProgress, imageHeight, actualDNALength, DNA_HEIGHT);

		RenderSystem.color4f(1, 1, 1, 1);
	}
}
