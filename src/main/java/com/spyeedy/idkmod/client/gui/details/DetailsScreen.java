package com.spyeedy.idkmod.client.gui.details;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.spyeedy.idkmod.client.gui.QuestMakerScreen;
import com.spyeedy.idkmod.client.gui.widget.button.IdkModButton;
import com.spyeedy.idkmod.common.IdkMod;
import net.minecraft.client.gui.chat.NarratorChatListener;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.*;

import java.awt.Color;

public abstract class DetailsScreen extends Screen {

	public static final ResourceLocation TEX = new ResourceLocation(IdkMod.MODID, "textures/gui/step_details.png");

	protected int xSize = 132;
	protected int ySize = 102;

	protected final QuestMakerScreen parent;

	private final boolean shouldAddNameTextField;
	private final boolean shouldAddDescTextField;

	public TextFieldWidget nameTextField = null;
	public TextFieldWidget descTextField = null;
	public Button btnSave;
	public Button btnCancel;

	public DetailsScreen(QuestMakerScreen parent, ITextComponent titleIn, boolean shouldAddNameTextField, boolean shouldAddDescTextField) {
		super(titleIn);
		this.parent = parent;
		this.shouldAddNameTextField = shouldAddNameTextField;
		this.shouldAddDescTextField = shouldAddDescTextField;
	}

	@Override
	protected void init() {
		super.init();

		int x = (this.width - xSize) / 2;
		int y = (this.height - ySize) / 2;

		if (this.shouldAddNameTextField) {
			this.nameTextField = new TextFieldWidget(minecraft.font, x + 8, y + 32, 118, 12, NarratorChatListener.NO_TITLE);
			this.addWidget(nameTextField);
		}

		if (this.shouldAddDescTextField) {
			this.descTextField = new TextFieldWidget(minecraft.font, x + 8, y + (shouldAddNameTextField ? 62 : 32), 118, 12, NarratorChatListener.NO_TITLE);
			this.addWidget(descTextField);
		}

		String text = I18n.get(IdkMod.MODID + ".string.save");
		this.btnSave = new IdkModButton(x + 7, y + ySize - 16 - 7, font.width(text) + 8, 16, new StringTextComponent(text), this::onBtnSave);

		text = I18n.get(IdkMod.MODID + ".string.cancel");
		this.btnCancel = new IdkModButton(x + xSize - 7 - font.width(text) - 8, btnSave.y, font.width(text) + 8, 16, new StringTextComponent(text), this::onBtnCancel);
		this.addButton(btnSave);
		this.addButton(btnCancel);
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		renderBackground(matrixStack);

		int x = (this.width - xSize) / 2;
		int y = (this.height - ySize) / 2;
		
		int halfWidth = xSize/2;
		int halfHeight = ySize/2;
		int texYSize = 102;
		int extraHeight = ySize % 2 == 0 ? 0 : 1;

		minecraft.getTextureManager().bind(TEX);
		
		this.blit(matrixStack, x,                   y              , 0                , 0                 , halfWidth, halfHeight); // top left
		this.blit(matrixStack, x + halfWidth   , y              , xSize - halfWidth, 0                 , halfWidth, halfHeight); // top right
		this.blit(matrixStack, x                ,y + halfHeight , 0                , texYSize - halfHeight - extraHeight, halfWidth, halfHeight); // bottom left
		this.blit(matrixStack, x + halfWidth, y + halfHeight , xSize - halfWidth, texYSize - halfHeight - extraHeight, halfWidth, halfHeight); // bottom right

		ITextComponent titleComponent = (getTitle() instanceof IFormattableTextComponent) ? ((IFormattableTextComponent) getTitle()).withStyle(TextFormatting.BOLD) : getTitle();
		drawCenteredString(matrixStack, minecraft.font, titleComponent,x + xSize/2, y + 7, Color.WHITE.getRGB());

		if (this.shouldAddNameTextField && this.nameTextField != null) {
			drawString(matrixStack, minecraft.font, new TranslationTextComponent("idkmod.string.name"), x + 7, y + 19, Color.WHITE.getRGB());
			this.nameTextField.render(matrixStack, mouseX, mouseY, partialTicks);
		}

		if (this.shouldAddDescTextField && this.descTextField != null) {
			drawString(matrixStack, minecraft.font, new TranslationTextComponent(IdkMod.MODID + ".string.desc"), x + 7, y + (shouldAddNameTextField ? 49 : 19), Color.WHITE.getRGB());
			this.descTextField.render(matrixStack, mouseX, mouseY, partialTicks);
		}

		renderExtra(matrixStack, mouseX, mouseY, x, y, partialTicks);

		super.render(matrixStack, mouseX, mouseY, partialTicks);
	}

	protected void renderExtra(MatrixStack matrixStack, int mouseX, int mouseY, int x, int y, float partialTicks) {}

	@Override
	public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
		return this.getFocused() != null && this.getFocused().keyPressed(keyCode, scanCode, modifiers);
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) {
		this.children().forEach(iGuiEventListener -> {
			if (iGuiEventListener instanceof TextFieldWidget)
				((TextFieldWidget) iGuiEventListener).setFocus(false);
		});
		return super.mouseClicked(mouseX, mouseY, button);
	}

	@Override
	public boolean mouseScrolled(double mouseX, double mouseY, double delta) {
		return this.getFocused() != null && this.getFocused().mouseScrolled(mouseX, mouseY, delta);
	}

	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int button) {
		return this.getFocused() != null && this.getFocused().mouseReleased(mouseX, mouseY, button);
	}

	@Override
	public boolean mouseDragged(double mouseX, double mouseY, int button, double dragX, double dragY) {
		return this.getFocused() != null && this.getFocused().mouseDragged(mouseX, mouseY, button, dragX, dragY);
	}

	public void onBtnSave(Button button) {
		onBtnCancel(button);
	}

	public void onBtnCancel(Button button) {
		if (shouldAddNameTextField) this.parent.children().remove(this.nameTextField);
		if (shouldAddDescTextField) this.parent.children().remove(this.descTextField);
		this.parent.children().remove(this.btnSave);
		this.parent.children().remove(this.btnCancel);

		this.parent.childScreen = null;
	}
}
