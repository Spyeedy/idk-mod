package com.spyeedy.idkmod.client.gui.details;

import com.spyeedy.idkmod.client.gui.QuestMakerScreen;
import com.spyeedy.idkmod.client.gui.widget.button.UpDownArrowButton;
import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.capability.journal.Step;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.text.TranslationTextComponent;

import javax.annotation.Nullable;

public class StepDetailsScreen extends DetailsScreen {

	@Nullable
	private final Step step; // To pass on step information to textfield
	private final int idx;

	public StepDetailsScreen(QuestMakerScreen parent) {
		this(parent, null, -1);
	}

	public StepDetailsScreen(QuestMakerScreen parent, @Nullable Step step, int idx) {
		super(parent, new TranslationTextComponent(IdkMod.MODID + (step != null ? ".string.edit_step" : ".string.add_step")), false, true);

		this.step = step;
		this.idx = idx;
	}

	@Override
	protected void init() {
		this.ySize = 72;
		super.init();

		if (step != null) {
			this.descTextField.setValue(this.step.getDescription().getContents());
		}
	}

	@Override
	public void onBtnSave(Button button) {
		if (this.step == null) {
			this.parent.quest.addStep(new Step(descTextField.getValue()));
			this.parent.questArrowButtonList.add(new UpDownArrowButton((btn2) -> ((UpDownArrowButton)btn2).setExpanded()));
		} else
			this.parent.quest.getStep(idx).setDescription(descTextField.getValue());

		super.onBtnSave(button);
	}
}
