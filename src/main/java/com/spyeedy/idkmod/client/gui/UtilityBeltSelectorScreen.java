package com.spyeedy.idkmod.client.gui;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.spyeedy.idkmod.client.ClientUtils;
import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.ability.UtilityBeltInventoryAbility;
import com.spyeedy.idkmod.common.network.MoveItemFromUtilityBeltMessage;
import com.spyeedy.idkmod.common.network.PacketHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.threetag.threecore.ability.AbilityHelper;
import net.threetag.threecore.ability.container.IAbilityContainer;

import java.awt.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class UtilityBeltSelectorScreen extends Screen {

	private int hoveredSector = -1;
	private boolean isMouseHolding = false; // we want to prevent highlights of other sectors if mouse is held down

	private UtilityBeltInventoryAbility ability;

	public UtilityBeltSelectorScreen(ResourceLocation abilityContainerId, String abilityId) {
		super(new TranslationTextComponent("screen." + IdkMod.MODID + ".radial_menu"));

		IAbilityContainer abilityContainer = AbilityHelper.getAbilityContainerFromId(Minecraft.getInstance().player, abilityContainerId);
		this.ability = abilityContainer != null ? (UtilityBeltInventoryAbility) abilityContainer.getAbility(abilityId) : null;
	}

	@Override
	protected void init() {
		super.init();
	}

	@Override
	public void tick() {
		super.tick();

		if (ability == null) this.onClose();
	}

	// Credits for the sector calculation, specifically, using radians for sin cos: https://github.com/FrederikRasmussen/ImmersiveRadialMenu/blob/master/src/main/java/immersiveradialmenu/client/GuiRadialMenu.java#L365
	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		float x = this.width/2f;
		float y = this.height/2f;

		super.render(matrixStack, mouseX, mouseY, partialTicks);

		if (minecraft == null || ability == null) return;

		int inRadius = 40;
		int outRadius = inRadius*2;
		int sectorCount = ability.getItemHandler().getSlots();
		final float sectorAngle = 360f/sectorCount;

		if (sectorCount <= 0) return;

		// Calculate variables for Mouse if-checks
		float relativeMouseY = mouseY - y; // subtract true mouse coordinate to center of polygon
		float relativeMouseX = mouseX - x;

		/* Draw Utility Belt Menu */
		matrixStack.pushPose();
		matrixStack.translate(x, y, 0);

		AtomicBoolean hasHoverSector = new AtomicBoolean(false);
		ClientUtils.drawPolygon(matrixStack, inRadius, outRadius, 1, sectorCount, mouseX, mouseY, Color.BLACK,
				(matrixStack1, startAngle, endAngle, inRadius1, outRadius1) -> {
					int sector = (int) (startAngle/sectorAngle);

					float mouseAngle = (float) Math.toDegrees(Math.atan2(relativeMouseX, -relativeMouseY)); // See https://en.wikipedia.org/wiki/Atan2. Atan2 is in radians, so convert to degrees
					if (mouseAngle < 0)
						mouseAngle += 360; // We want to make the mouse angle positive if it's below 0, meaning range -0 to -180, make that positive, into range 180 to 360

					boolean isMouseOverSector = !isMouseHolding && mouseAngle >= startAngle && mouseAngle < endAngle; // mouse is hovering over sector, or not, using mouse angles from the polygon center
					boolean isSelected = this.isMouseHolding && sector == this.hoveredSector;

					if (isMouseOverSector) {
						this.hoveredSector = sector;
						hasHoverSector.set(true);
					}

					int color = isMouseOverSector ? 150 : isSelected ? 30 : 90;
					ClientUtils.drawPolygonSector(matrixStack, inRadius, outRadius, startAngle, endAngle, color, color, color, 200);

					// Draw ItemStack
					ItemStack stack = ability.getItemHandler().getStackInSlot(sector);

					if (stack.isEmpty()) return;

					float radiusHalfLength = inRadius1 + (outRadius1 - inRadius1)/2;
					float angle = (float) Math.toRadians(startAngle + (endAngle - startAngle)/2);
					int drawSize = 16;

					// cos and sin takes in radians. We got the center from hypotenuse * Math.angle, now we want to make it center, so we get the top-left coordinate.
					// we make the y for hypotenuse * Math.angle negative to move upwards. Because in Minecraft, down is +ve, up is -ve.
					int itemX = (int) (radiusHalfLength * Math.sin(angle)) - drawSize/2; // opposite line is horizontal, so SOH, therefore cos is used
					int itemY = (int) -(radiusHalfLength * Math.cos(angle)) - drawSize/2; // adjacent line is vertical, so CAH, therefore sin is used

					this.itemRenderer.renderAndDecorateItem(stack, (int)x + itemX, (int)y + itemY); // item and any effects like enchantments
					this.itemRenderer.renderGuiItemDecorations(font, stack, (int)x + itemX, (int)y + itemY); // item count text
				});

		if (!hasHoverSector.get() && !isMouseHolding)
			this.hoveredSector = -1;

		matrixStack.popPose();

		/* Draw Tooltip */ // draw above the menu, so it's after the selector menu rendering
		float mouseAngle = (float) Math.toDegrees(Math.atan2(relativeMouseX, -relativeMouseY)); // See https://en.wikipedia.org/wiki/Atan2. Atan2 is in radians, so convert to degrees
		if (mouseAngle < 0)
			mouseAngle += 360; // We want to make the mouse angle positive if it's below 0, meaning range -0 to -180, make that positive, into range 180 to 360

		for (int sector = 0; sector < sectorCount; sector++) {
			float startAngle = sector * sectorAngle;
			float endAngle = (sector + 1) * sectorAngle;

			if (this.isMouseHolding) { // render tooltip for selected sector when mouse is held down
				if (sector == this.hoveredSector) {
					ItemStack stack = ability.getItemHandler().getStackInSlot(sector);
					if (!stack.isEmpty()) drawSectorItemStackTooltip(matrixStack, stack, (int) x, (int) y, inRadius, outRadius, startAngle, endAngle);
					break;
				}
			} else if (mouseAngle >= startAngle && mouseAngle <= endAngle) { // if mouse is not held, then the item in the sector where mouse is hovering
				ItemStack stack = ability.getItemHandler().getStackInSlot(sector);
				if (!stack.isEmpty()) drawSectorItemStackTooltip(matrixStack, stack, (int) x, (int) y, inRadius, outRadius, startAngle, endAngle);
				break;
			}
		}
	}

	private void drawSectorItemStackTooltip(MatrixStack matrixStack, ItemStack stack, int posX, int posY, float inRadius, float outRadius, float startAngle, float endAngle) {
		float radiusHalfLength = inRadius + (outRadius - inRadius)/2f;
		float angle = (float) Math.toRadians(startAngle + (endAngle - startAngle)/2);

		// cos and sin takes in radians
		int itemX = (int) (radiusHalfLength * Math.sin(angle)); // adjacent line is horizontal, so CAH, therefore cos is used
		int itemY = (int) -(radiusHalfLength * Math.cos(angle)); // opposite line is vertical, so SOH, therefore sin is used

		this.renderTooltip(matrixStack, stack, posX + itemX, posY + itemY);
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) {
		if (this.hoveredSector > -1 && ability != null) {
			this.isMouseHolding = true;

			if (minecraft != null && minecraft.player != null) {
				if (this.hoveredSector < ability.getItemHandler().getSlots()) {
					if (!ability.getItemHandler().getStackInSlot(this.hoveredSector).isEmpty()) {
						PacketHandler.NETWORK.sendToServer(new MoveItemFromUtilityBeltMessage(this.ability.container.getId(), this.ability.getId(), this.hoveredSector));
						this.onClose();
					}
				}
			}

			return true;
		}
		return false;
	}

	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int button) {
		if (this.isMouseHolding) {
			this.isMouseHolding = false;
			return true;
		}
		return false;
	}

	@Override
	public boolean mouseScrolled(double mouseX, double mouseY, double delta) {
		return false;
	}
}
