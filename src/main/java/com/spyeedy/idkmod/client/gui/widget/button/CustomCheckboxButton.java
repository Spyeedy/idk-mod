package com.spyeedy.idkmod.client.gui.widget.button;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.util.text.ITextComponent;

import javax.annotation.Nonnull;
import java.awt.*;

public class CustomCheckboxButton extends IdkModButton {

	private boolean checked;

	public CustomCheckboxButton(int x, int y, @Nonnull ITextComponent title, boolean checked, IPressable iPressable) {
		super(x, y, 16, 16, title, iPressable);
		this.checked = checked;
	}

	@Override
	public void onPress() {
		this.checked = !checked;
		super.onPress();
	}

	@Override
	public void renderButton(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		renderBg(matrixStack, minecraft, mouseX, mouseY);

		drawString(matrixStack, Minecraft.getInstance().font, this.getMessage(), this.x + this.width + 6, this.y + (this.height - 8) / 2, Color.WHITE.getRGB());
	}

	@Override
	protected void renderBg(MatrixStack matrixStack, Minecraft minecraft, int mouseX, int mouseY) {
		super.renderBg(matrixStack, minecraft, mouseX, mouseY);

		if (!this.checked) return;

		minecraft.getTextureManager().bind(TEX);
		int checkboxSize = 12;
		int x = this.x + (this.width - checkboxSize)/2;
		int y = this.y + (this.height - checkboxSize)/2;

		this.blit(matrixStack, x, y, 49,0, checkboxSize, checkboxSize);
	}

	@Override
	public boolean isMouseOver(double mouseX, double mouseY) {
		return this.active && this.visible && mouseX >= (double)this.x && mouseY >= (double)this.y && mouseX < (double)(this.x + this.width + 6 + minecraft.font.width(this.getMessage())) && mouseY < (double)(this.y + this.height);
	}

	@Override
	protected boolean clicked(double mouseX, double mouseY) {
		return isMouseOver(mouseX, mouseY);
	}

	@Override
	protected int getYImage(boolean isHovered) {
		return 0;
	}

	public boolean isChecked() {
		return checked;
	}
}
