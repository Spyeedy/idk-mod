package com.spyeedy.idkmod.client.gui.details;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.spyeedy.idkmod.client.gui.QuestMakerScreen;
import com.spyeedy.idkmod.client.gui.widget.DropdownList;
import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.capability.journal.objectives.CollectCountObjective;
import com.spyeedy.idkmod.common.capability.journal.objectives.KillCountObjective;
import com.spyeedy.idkmod.common.capability.journal.objectives.Objective;
import net.minecraft.client.gui.chat.NarratorChatListener;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.text.TranslationTextComponent;

import javax.annotation.Nullable;
import java.awt.*;
import java.util.Arrays;

public class ObjectiveDetailsScreen extends DetailsScreen {

	@Nullable
	private final Objective objective;
	private DropdownList typesList;
	private final int parentStepIdx;
	private final int idx;

	private TextFieldWidget countTextField;

	public ObjectiveDetailsScreen(QuestMakerScreen parent, int parentStepIdx) {
		this(parent, null, parentStepIdx, -1);
	}

	public ObjectiveDetailsScreen(QuestMakerScreen parent, @Nullable Objective objective, int parentStepIdx, int idx) {
		super(parent, new TranslationTextComponent(IdkMod.MODID + (objective != null ? ".string.edit_objective" : ".string.add_objective")), false, false);
		this.objective = objective;
		this.parentStepIdx = parentStepIdx;
		this.idx = idx;
	}

	@Override
	protected void init() {
		this.ySize = 103;
		super.init();

		int x = (this.width - xSize) / 2;
		int y = (this.height - ySize) / 2;

		this.countTextField = new TextFieldWidget(font, x + 8, y + 63, 30, 12, NarratorChatListener.NO_TITLE);
		this.countTextField.setMaxLength(3);
		if (this.objective == null)
			this.countTextField.setValue("10");
		else if (this.objective instanceof KillCountObjective) {
			int count = ((KillCountObjective) this.objective).getMaxCount();
			this.countTextField.setValue(Integer.toString(count));
		} else if (this.objective instanceof CollectCountObjective) {
			int count = ((CollectCountObjective) this.objective).getMaxCount();
			this.countTextField.setValue(Integer.toString(count));
		}
		this.addWidget(countTextField);

		if (objective == null) {
			this.typesList = new DropdownList(x + 7, y + 32, Arrays.asList(Objective.Type.getNames()), (button) -> {});
			this.addButton(typesList);
		}
	}

	@Override
	protected void renderExtra(MatrixStack matrixStack, int mouseX, int mouseY, int x, int y, float partialTicks) {
		drawString(matrixStack, font, new TranslationTextComponent("idkmod.string.type"), x + 7, y + 19, Color.WHITE.getRGB());

		if (this.objective != null) font.draw(matrixStack, objective.getType().getLocalizedName(), x + 9, y + 34, 0xFF);

		if (this.countTextField != null && this.countTextField.active) {
			this.countTextField.render(matrixStack, mouseX, mouseY, partialTicks);
			drawString(matrixStack, font, new TranslationTextComponent(IdkMod.MODID + ".string.max_count"), x + 7, y + 50, Color.WHITE.getRGB());
		}
	}

	@Override
	public void onBtnSave(Button button) {
		if (this.countTextField.getValue().isEmpty() || this.countTextField.getValue().equals("0")) {
			this.countTextField.setFocus(true);
			return;
		}

		int maxCount = Integer.parseInt(this.countTextField.getValue());

		if (this.objective == null) {
			Objective obj = null;
			switch (Objective.Type.values()[this.typesList.selected]) {
				case KILL_COUNT:
					obj = new KillCountObjective(maxCount);
					break;
				case COLLECT_COUNT:
					obj = new CollectCountObjective(maxCount, new ItemStack(Items.DIAMOND_SWORD));
					break;
			}

			if (obj != null)
				this.parent.quest.getStep(parentStepIdx).addObjective(obj);
		} else {
			Objective obj = this.parent.quest.getStep(parentStepIdx).getObjective(idx);

			switch (objective.getType()) {
				case KILL_COUNT:
					((KillCountObjective) obj).setMaxCount(maxCount);
					break;
				case COLLECT_COUNT:
					((CollectCountObjective) obj).setMaxCount(maxCount);
					break;
			}
		}

		super.onBtnSave(button);
	}

	@Override
	public boolean charTyped(char codePoint, int modifiers) {
		if (this.countTextField.isFocused() && !Character.isDigit(codePoint))
			return false;
		return super.charTyped(codePoint, modifiers);
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) {
		if (getFocused() instanceof DropdownList && ((DropdownList) getFocused()).expanded) {
			return getFocused().mouseClicked(mouseX, mouseY, button);
		}
		return super.mouseClicked(mouseX, mouseY, button);
	}
}
