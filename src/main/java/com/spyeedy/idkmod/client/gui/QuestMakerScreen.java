package com.spyeedy.idkmod.client.gui;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.spyeedy.idkmod.client.gui.details.ObjectiveDetailsScreen;
import com.spyeedy.idkmod.client.gui.details.SaveQuestScreen;
import com.spyeedy.idkmod.client.gui.details.StepDetailsScreen;
import com.spyeedy.idkmod.client.gui.widget.StepsScrollPanel;
import com.spyeedy.idkmod.client.gui.widget.button.TinyButton;
import com.spyeedy.idkmod.client.gui.widget.button.UpDownArrowButton;
import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.capability.journal.Quest;
import com.spyeedy.idkmod.common.capability.journal.QuestReader;
import com.spyeedy.idkmod.common.capability.journal.Step;
import com.spyeedy.idkmod.common.capability.journal.objectives.Objective;
import net.minecraft.client.gui.chat.NarratorChatListener;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class QuestMakerScreen extends Screen {

	public static final ResourceLocation TEX = new ResourceLocation(IdkMod.MODID, "textures/gui/quest_maker.png");

	public static int xSize = 177; // public for TinyButton to access
	public static int ySize = 166; // public for UpDownArrowButton to access

	@Nullable
	public Screen childScreen; // to allow a smaller gui to be displayed, like when adding a new step
	private StepsScrollPanel stepsPanel;

	public List<UpDownArrowButton> questArrowButtonList;
	private Button btnAddStep;
	private Button btnAddObjective;
	private Button btnEdit;
	private Button btnDelete;
	private Button btnSave;
	private Button btnShiftUp; // moves an entry upwards
	private Button btnShiftDown; // moves an entry downwards

	public Quest quest;

	public QuestMakerScreen() {
		super(NarratorChatListener.NO_TITLE);
		this.quest = new Quest();

		this.questArrowButtonList = new ArrayList<>();
	}

	@Override
	protected void init() {
		super.init();

		if (this.childScreen != null) {
			this.childScreen.init(minecraft, width, height);
		}

		int i = (this.width - xSize)/2;
		int j = (this.height - ySize)/2;

		this.stepsPanel = new StepsScrollPanel(this, j + 8, i + 8);

		this.btnAddStep = new TinyButton(i + 162, j + 10, TinyButton.Icon.ADD_STEP, new TranslationTextComponent(IdkMod.MODID + ".quest_maker.button.add_step.tooltip"), (button) -> this.setChildScreen(new StepDetailsScreen(this)));

		this.btnAddObjective = new TinyButton(btnAddStep.x, btnAddStep.y + 11, TinyButton.Icon.ADD_OBJECTIVE, new TranslationTextComponent(IdkMod.MODID + ".quest_maker.button.add_objective.tooltip"), (button) -> {
			if (this.stepsPanel.selectedEntryType == null
					|| (this.stepsPanel.selected <= -1 && this.stepsPanel.selectedEntryType == StepsScrollPanel.SelectedEntryType.STEP)
					|| (this.stepsPanel.selectedObjParent <= -1 && this.stepsPanel.selectedEntryType == StepsScrollPanel.SelectedEntryType.OBJECTIVE)) return;

			int stepNumber = this.stepsPanel.selectedEntryType == StepsScrollPanel.SelectedEntryType.STEP ? this.stepsPanel.selected : this.stepsPanel.selectedObjParent;

			setChildScreen(new ObjectiveDetailsScreen(this, stepNumber));
		});

		this.btnEdit = new TinyButton(btnAddStep.x, btnAddObjective.y + 11, TinyButton.Icon.PENCIL, new TranslationTextComponent(IdkMod.MODID + ".quest_maker.button.edit.tooltip"), (button) -> {
			int selectedEntry = this.stepsPanel.selected;
			int parentIdx = this.stepsPanel.selectedObjParent;

			Screen screen = null;

			if (selectedEntry > -1 && this.stepsPanel.selectedEntryType != null) {
				switch (this.stepsPanel.selectedEntryType) {
					case STEP:
						screen = new StepDetailsScreen(this, this.quest.getStep(selectedEntry), selectedEntry);
						break;
					case OBJECTIVE:
						if (parentIdx < -1) return;
						screen = new ObjectiveDetailsScreen(this, this.quest.getStep(parentIdx).getObjective(selectedEntry), parentIdx, selectedEntry);
						break;
				}
			}

			if (screen != null) setChildScreen(screen);

		});

		this.btnDelete = new TinyButton(btnAddStep.x, btnEdit.y + 11, TinyButton.Icon.CROSS, new TranslationTextComponent(IdkMod.MODID + ".quest_maker.button.delete.tooltip"), (button) -> {
			int selectedEntry = this.stepsPanel.selected;

			if (selectedEntry > -1 && this.stepsPanel.selectedEntryType != null) {
				switch (this.stepsPanel.selectedEntryType) {
					case STEP:
						this.quest.getSteps().remove(selectedEntry);
						this.questArrowButtonList.remove(selectedEntry);

						break;
					case OBJECTIVE:
						int parentIdx = this.stepsPanel.selectedObjParent;
						if (parentIdx < 0 || parentIdx >= this.quest.getStepSize()) break;

						this.quest.getStep(parentIdx).getObjectives().remove(selectedEntry);
						break;
				}
			}

			this.stepsPanel.selectedEntryType = null;
			this.stepsPanel.selectedObjParent = -1;
			this.stepsPanel.selected = -1;

			this.stepsPanel.readjustScrollBar();
		});

		this.btnSave = new TinyButton(btnAddStep.x, btnDelete.y + 11, TinyButton.Icon.SAVE, new TranslationTextComponent(IdkMod.MODID + ".string.save"), (button) -> setChildScreen(new SaveQuestScreen(this, this.quest)));

		this.btnShiftUp = new TinyButton(btnAddStep.x, btnSave.y + 20, TinyButton.Icon.UP_ARROW, new TranslationTextComponent(IdkMod.MODID + ".quest_maker.button.shift_up.tooltip"), (button) -> {
			int selectedEntry = this.stepsPanel.selected;
			int newIndex = selectedEntry - 1;

			if (selectedEntry > 0 && this.stepsPanel.selectedEntryType != null) {
				switch (this.stepsPanel.selectedEntryType) {
					case STEP:
						Step movingStep = this.quest.getStep(selectedEntry);
						UpDownArrowButton movingBtn = this.questArrowButtonList.get(selectedEntry);

						this.quest.getSteps().remove(selectedEntry); // Remove step
						this.quest.getSteps().add(newIndex, movingStep); // Add step at the desired index
						this.questArrowButtonList.remove(selectedEntry); // Remove button
						this.questArrowButtonList.add(newIndex, movingBtn); // Add button at the desired index

						this.stepsPanel.selected = newIndex; // change selected index to copy the movedStep's new index
						break;
					case OBJECTIVE:
						int parentIdx = this.stepsPanel.selectedObjParent;
						Step parentStep = this.quest.getStep(parentIdx);
						Objective movingObj = parentStep.getObjective(selectedEntry);

						parentStep.getObjectives().remove(selectedEntry);
						parentStep.getObjectives().add(newIndex, movingObj);

						this.stepsPanel.selected = newIndex; // change selected index to copy the movedStep's new index
						break;
				}
			}
		});

		this.btnShiftDown = new TinyButton(btnAddStep.x, btnShiftUp.y + 11, TinyButton.Icon.DOWN_ARROW, new TranslationTextComponent(IdkMod.MODID + ".quest_maker.button.shift_down.tooltip"), (button) -> {
			int selectedEntry = this.stepsPanel.selected;
			int newIndex = selectedEntry + 1;

			if (selectedEntry > -1 && this.stepsPanel.selectedEntryType != null) {
				switch (this.stepsPanel.selectedEntryType) {
					case STEP:
						if (selectedEntry >= (quest.getStepSize() - 1)) break;

						Step movingStep = this.quest.getStep(selectedEntry);
						UpDownArrowButton movingBtn = this.questArrowButtonList.get(selectedEntry);

						this.quest.getSteps().remove(selectedEntry); // Remove step
						this.quest.getSteps().add(newIndex, movingStep); // Add step at the desired index
						this.questArrowButtonList.remove(selectedEntry); // Remove button
						this.questArrowButtonList.add(newIndex, movingBtn); // Add button at the desired index

						this.stepsPanel.selected = newIndex; // change selected index to copy the movedStep's new index
						break;
					case OBJECTIVE:
						int parentIdx = this.stepsPanel.selectedObjParent;
						Step parentStep = this.quest.getStep(parentIdx);

						if (selectedEntry >= (parentStep.getObjectiveCount() - 1)) break;

						Objective movingObj = this.quest.getStep(parentIdx).getObjective(selectedEntry);

						parentStep.getObjectives().remove(selectedEntry);
						parentStep.getObjectives().add(newIndex, movingObj);

						this.stepsPanel.selected = newIndex; // change selected index to copy the movedStep's new index
						break;
				}
			}
		});

		this.addButton(btnAddStep);
		this.addButton(btnAddObjective);
		this.addButton(btnEdit);
		this.addButton(btnDelete);
		this.addButton(btnSave);
		this.addButton(btnShiftUp);
		this.addButton(btnShiftDown);

		this.addButton(new TinyButton(btnAddStep.x, btnShiftDown.y + 40, TinyButton.Icon.PENCIL, new StringTextComponent("Test rendering itemstack in client gui"), (button) -> setChildScreen(new SearchAllItemsScreen(this))));
	}

	@Override
	public void tick() {
		if (childScreen != null) {
			childScreen.tick();
			this.buttons.forEach(widget -> {
				if (widget.active) widget.active = false;
			});
			return;
		}

		int selectedEntry = this.stepsPanel.selected;
		boolean isEntrySelected = selectedEntry > -1 && this.stepsPanel.selectedEntryType != null;

		if (!this.btnAddStep.active) this.btnAddStep.active = true;

		this.btnAddObjective.active = (this.stepsPanel.selectedEntryType == StepsScrollPanel.SelectedEntryType.STEP && selectedEntry > -1)
				|| (this.stepsPanel.selectedEntryType == StepsScrollPanel.SelectedEntryType.OBJECTIVE && this.stepsPanel.selectedObjParent > -1);

		this.btnEdit.active = this.btnDelete.active = isEntrySelected;

		Step parentStep = this.stepsPanel.selectedObjParent > -1 ? this.quest.getStep(this.stepsPanel.selectedObjParent) : null;

		this.btnShiftUp.active = isEntrySelected && selectedEntry > 0;
		this.btnShiftDown.active = isEntrySelected
				&& (
						(this.stepsPanel.selectedEntryType == StepsScrollPanel.SelectedEntryType.OBJECTIVE && parentStep != null && selectedEntry < (parentStep.getObjectiveCount() - 1)) // check for objective
								|| (this.stepsPanel.selectedEntryType == StepsScrollPanel.SelectedEntryType.STEP && selectedEntry < (quest.getStepSize() - 1)) // check for step
				);

		this.btnSave.active = this.quest.getStepSize() > 0;
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		int x = (this.width - xSize)/2;
		int y = (this.height - ySize)/2;

		this.minecraft.getTextureManager().bind(TEX);
		this.blit(matrixStack, x, y, 0, 0, xSize, ySize);

		if (this.stepsPanel != null) {
			this.stepsPanel.render(matrixStack, mouseX, mouseY, partialTicks);

			int rootX = x + 10;
			int rootY = y + 10;
			for (Step step : quest.getSteps()) {
				if (mouseX >= rootX && mouseX < rootX + 141 && mouseY >= rootY && mouseY < rootY + 14 && step.getDescription() != null) {
					this.renderTooltip(matrixStack, step.getDescription(), mouseX, mouseY);
				}

				rootY += 14;

				for (Objective objective : step.getObjectives()) {
					// render objective tooltip
					if (mouseX >= rootX + 10 && mouseX <= rootX + 141 && mouseY >= rootY && mouseY <= rootY + 10) {
						this.renderTooltip(matrixStack, QuestReader.Utils.formatObjectiveDescription(objective), mouseX, mouseY);
					}

					rootY += 10;
				}
			}
		}

		super.render(matrixStack, mouseX, mouseY, partialTicks);

		// Let childScreen render over the rest
		if (this.childScreen != null) {
			this.childScreen.render(matrixStack, mouseX, mouseY, partialTicks);
		}
	}

	/** IMPORTANT: We let childScreen take precedence over other components' mouse actions */

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) {
		if (this.childScreen != null)
			return this.childScreen.mouseClicked(mouseX, mouseY, button);
		return super.mouseClicked(mouseX, mouseY, button) || (this.stepsPanel != null && this.stepsPanel.mouseClicked(mouseX, mouseY, button));
	}

	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int button) {
		if (this.childScreen != null)
			return this.childScreen.mouseReleased(mouseX, mouseY, button);
		return super.mouseReleased(mouseX, mouseY, button) || this.stepsPanel != null && this.stepsPanel.mouseReleased(mouseX, mouseY, button);
	}

	@Override
	public boolean mouseDragged(double mouseX, double mouseY, int button, double dragX, double dragY) {
		if (this.childScreen != null)
			return this.childScreen.mouseDragged(mouseX, mouseY, button, dragX, dragY);
		return super.mouseDragged(mouseX, mouseY, button, dragX, dragY) || this.stepsPanel != null && this.stepsPanel.mouseDragged(mouseX, mouseY, button, dragX, dragY);
	}

	@Override
	public boolean mouseScrolled(double mouseX, double mouseY, double delta) {
		if (this.childScreen != null)
			return this.childScreen.mouseScrolled(mouseX, mouseY, delta);
		return super.mouseScrolled(mouseX, mouseY, delta) || this.stepsPanel != null && this.stepsPanel.mouseScrolled(mouseX, mouseY, delta);
	}

	@Override
	public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
		if (this.childScreen != null) {
			// We check if Esc key was pressed, if it was pressed, we 'close' the child screen by setting it to null, otherwise, proceed to childScreen#keyPressed
			if (keyCode == 256 && this.shouldCloseOnEsc()) {
				this.children().removeAll(this.childScreen.children());
				this.childScreen = null;
				return true;
			}
			return this.childScreen.keyPressed(keyCode, scanCode, modifiers);
		}
		return super.keyPressed(keyCode, scanCode, modifiers);
	}

	@Override
	public boolean charTyped(char codePoint, int modifiers) {
		if (this.childScreen != null)
			return this.childScreen.charTyped(codePoint, modifiers);
		return false;
	}

	public void setChildScreen(Screen screen) {
		screen.init(minecraft, width, height);

		this.childScreen = screen;
	}
}
