package com.spyeedy.idkmod.client.gui.widget;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.spyeedy.idkmod.client.gui.DNASequencerScreen;
import com.spyeedy.idkmod.client.gui.widget.button.TinyButton;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.registry.Registry;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import java.awt.*;

public class ItemStackScrollPanel extends CustomScrollPanel {
	private static final StringBuilder stringBuilder = new StringBuilder();

	private static final int slotHeight = 18;
	private final Screen parent;
	private final int cols;
	private final int visibleRows;
	private final IPressSlot iPressSlot;

	private int scrollY = 0; // for scrollbar
	private int scrollDistance = 0; // for scrolling content

	private ItemStack hoveringStack = ItemStack.EMPTY;

	private Button upArrowBtn;
	private Button downArrowBtn;
	//private TextFieldWidget searchField;

	/**
	 * Override mouseClicked and mouseScrolled, return the methods from your ItemStackScrollPanel variable
	 */
	public ItemStackScrollPanel(Screen parent, int x, int y, int cols, int visibleRows, IPressSlot iPressSlot) {
		super(x, y, cols * slotHeight, visibleRows * slotHeight);
		this.parent = parent;
		this.cols = cols;
		this.visibleRows = visibleRows;

		//this.searchField = new TextFieldWidget(parent.getMinecraft().fontRenderer, )
		//parent.
		this.iPressSlot = iPressSlot;

		this.addListener(this.upArrowBtn = new TinyButton(this.x + this.cols * slotHeight, this.y, TinyButton.Icon.UP_ARROW, p_onPress_1_ -> {
			this.scrollDistance -= slotHeight;
			this.scrollY -= getScrollAmount();
			this.applyScrollLimits();
		}));

		this.addListener(this.downArrowBtn = new TinyButton(this.x + this.cols * slotHeight, this.y + getVisibleHeight() - 10, TinyButton.Icon.DOWN_ARROW, p_onPress_1_ -> {
			this.scrollDistance += slotHeight;
			this.scrollY += getScrollAmount();
			this.applyScrollLimits();
		}));
	}

	@Override
	public void tick() {
		super.tick();

		this.downArrowBtn.active = this.upArrowBtn.active = canScroll();
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		if (!this.visible) return;

		int count = 0; // maintain count for the row
		int col = 0; // column affects x offset
		int row = 0; // row affects y offset

		for (Item item : Registry.ITEM) {
			int i = x + col * slotHeight;
			int j = y + row * slotHeight - scrollDistance;

			if (item == Items.AIR) continue; // we skip display air item

			if (j >= this.y && j < this.y + getVisibleHeight()) { // ensures the rows are in in visible area
				ItemStack stack = new ItemStack(item);

				this.parent.getMinecraft().getTextureManager().bind(DNASequencerScreen.TEX_GUI);
				this.parent.blit(matrixStack, i, j, 34, 8, slotHeight, slotHeight);
				this.parent.getMinecraft().getItemRenderer().renderAndDecorateItem(stack, i + 1, j + 1);

				if (mouseX >= i && mouseX <= i + 18 && mouseY >= j && mouseY <= j + 18) {
					this.hoveringStack = stack;
					parent.renderWrappedToolTip(matrixStack, parent.getTooltipFromItem(stack), mouseX, mouseY, parent.getMinecraft().font);
					Screen.fill(matrixStack, i + 1, j + 1, i + 17, j + 17, 0x80FFFFFF); // renders translucent white square to show is hovering
				}
			}

			count++;
			col++;

			if (count >= this.cols) { // if count reaches max column for the row, we reset count and col, and move to next row
				count = col = 0;
				row++;
			}
			if (y + row * slotHeight - scrollDistance >= this.y + getVisibleHeight()) break; // we stop looping when we reach the next row after the LAST VISIBLE row, to prevent further unnecessary loops
		}

		super.render(matrixStack, mouseX, mouseY, partialTicks);

		int btnX = this.x + this.cols * slotHeight;
		int bottom = this.y + getVisibleHeight() - 10;

		/* Draw Scrollbar */
		int scrollTopPos = this.y + 10;
		Screen.fill(matrixStack, btnX, scrollTopPos, btnX + 10, bottom, Color.BLACK.getRGB()); // Border
		Screen.fill(matrixStack, btnX + 1, scrollTopPos, btnX + 9, bottom, Color.DARK_GRAY.getRGB()); // draw dark gray background for scrollbar to move about.
		if (canScroll())
			Screen.fill(matrixStack, btnX + 1, scrollTopPos + scrollY, btnX + 9, scrollTopPos + scrollY + getScrollBarHeight(), Color.LIGHT_GRAY.getRGB()); // draw scrollbar
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) {
		if (visible) {
			int btnX = this.x + this.cols * slotHeight;
			if (mouseX >= btnX && mouseX <= btnX + 10) { // if over button width-area
				int scrollDelta = Math.round((float) slotHeight / getContentLength() * getScrollBGHeight());
			} else if (mouseX >= this.x && mouseX <= this.x + this.cols * slotHeight && mouseY >= this.y && mouseY <= this.y + getVisibleHeight()) { // if within slot area
				if (!hoveringStack.isEmpty()) {
					this.iPressSlot.onPress(this.hoveringStack);

					return true;
				}
			}
		}
		return super.mouseClicked(mouseX, mouseY, button);
	}

	@Override
	public boolean mouseScrolled(double mouseX, double mouseY, double delta) {
		boolean isMouseInSlotArea = mouseX >= this.x && mouseX <= this.x + (this.cols * slotHeight) && mouseY >= this.y && mouseY <= this.y + getVisibleHeight();
		boolean isMouseInScrollArea = mouseX >= this.x + (this.cols * slotHeight) + 1 && mouseX <= this.x + (this.cols * slotHeight) + 9 && mouseY >= this.y + 10 && mouseY <= this.y + getVisibleHeight() - 10;

		if (visible && canScroll() && (isMouseInSlotArea || isMouseInScrollArea)) {
			scrollDistance += -delta * slotHeight;
			scrollY += -delta * getScrollAmount(); // We move by a multiplier of 1/itemSize x scrollbar background length

			/*int i = (Registry.ITEM.getEntries().size()) / cols - visibleRows;
			this.scrollY = (int) Math.ceil((this.scrollY - delta / i) * 1000);
			System.out.println(MathHelper.clamp(this.scrollY - delta / i, 0.0f, 1.0f));*/

			applyScrollLimits();
			return true;
		}
		return super.mouseScrolled(mouseX, mouseY, delta);
	}

	@Override
	protected boolean canScroll() {
		return getContentLength() > getVisibleHeight();
	}

	//TODO Fix scroll limits
	@Override
	protected void applyScrollLimits() {
		if (scrollY + getScrollBarHeight() > getScrollBGHeight()) { // if scrollbar is out of the scroll area, we move it back to proper place
			scrollY = getScrollBGHeight() - getScrollBarHeight();
		} else if (scrollY < 0) { // if scrollbar is out of the scroll area, we move it forward to top of the scroll area
			scrollY = 0;
		}

		if (scrollDistance <= 0) {
			scrollDistance = 0;
			scrollY = 0; // we set scrollY because scrollY might still be pixels away from reaching the bottom.
		}
		else if (scrollDistance >= getContentLength() - getVisibleHeight()) { // if scrollDistance reached the max
			scrollDistance = getContentLength() - getVisibleHeight();
			scrollY = getScrollBGHeight() - getScrollBarHeight(); // we set scrollY because scrollY might still be pixels away from reaching the bottom.
		}
	}

	private int getScrollBGHeight() {
		return (this.y + getVisibleHeight() - 10) - (this.y + 10);
	}

	private int getScrollBarHeight() {
		float division = (float) getVisibleHeight()/ getContentLength();
		int scrollBarHeight = Math.round(division * getScrollBGHeight());

		if ((division * getScrollBGHeight()) < 1) { // if division is less than 0, the scrollbar must move for every X rows
			division = (float) (getScrollBGHeight() - scrollBarHeight) / (getContentLength() - getVisibleHeight());
			stringBuilder.setLength(0);

			float decimal = division % 1;
			while (decimal < 1) {
				decimal *= 10; // multiply by 10 to remove a zero
				stringBuilder.append("0");
			}
			stringBuilder.append("0"); // we increment by one to get 2 numbers in whole number

			int multiplier = Integer.parseInt("1" + stringBuilder);

			return 1;
		} else {
			return scrollBarHeight;
		}
	}

	@Override
	protected int getScrollAmount() {
		float division = (float) slotHeight / getContentLength() * getScrollBGHeight();
		int delta = 0;

		if (division < 1) { // if division is less than 0, the scrollbar must move for every X rows
//			division = (float) (getScrollBGHeight() - getScrollBarHeight()) / (getContentHeight() - getVisibleHeight());
			stringBuilder.setLength(0);
			float decimal = division % 1;
			while (decimal < 1) {
				decimal *= 10; // multiply by 10 to remove a zero
				stringBuilder.append("0");
			}
//			stringBuilder.append("0"); // we increment by one to get 2 numbers in whole number

			int multiplier = Integer.parseInt("1" + stringBuilder);
			int numOfRows = (int) Math.ceil(division * multiplier); // we multiply by 10 to get the x rows, and round up to get a whole number

			int rowsScrolled = scrollDistance/slotHeight; // we want to compare rows crolled to number of rows, so we divide by slot height to determine how many rows we have scrolled

			if (rowsScrolled % numOfRows == 0) // we check if rowsScrolled is a multiple of numOfRows or not. If it is a multiple, we can move scrollbar for next set of X rows, otherwise, we cannot move if it's within current set of X row
				delta = 1;
		} else
			delta = Math.round(division); // we round up the division, since it's more or equals to 1, meaning 1 pixel or more for every row.

		return delta;
	}

	private int getVisibleHeight() {
		return this.visibleRows * slotHeight;
	}

	@Override
	protected int getContentLength() {
		float division = (Registry.ITEM.entrySet().size()) / (float) cols;
		int rows = Math.round(division); // check if the division does not have a remainder or not. If it does not, we add a row for those "remainder" items

		if (division > rows && division - rows < 0.5f) // we check if division is more than rows, because we want to be able to do subtraction and produce a positive result. If rows is larger than division, the result would be negative
			rows++;

		return rows * slotHeight;
	}

	private boolean isBtnHover(boolean isUp, int mouseX, int mouseY) {
		boolean mouseYBool = (isUp) ? mouseY >= this.y && mouseY <= this.y + 10 : mouseY >= this.y + getVisibleHeight() - 10 && mouseY <= this.y + getVisibleHeight();
		return mouseX >= this.x + this.cols * slotHeight && mouseX <= this.x + this.cols * slotHeight + 10 && mouseYBool;
	}

	@OnlyIn(Dist.CLIENT)
	public interface IPressSlot {
		void onPress(ItemStack stack);
	}
}
