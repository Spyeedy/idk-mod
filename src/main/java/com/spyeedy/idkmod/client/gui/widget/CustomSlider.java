package com.spyeedy.idkmod.client.gui.widget;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.fml.client.gui.GuiUtils;
import net.minecraftforge.fml.client.gui.widget.Slider;

import javax.annotation.Nullable;

public class CustomSlider extends Slider {

	public CustomSlider(int xPos, int yPos, int width, int height, ITextComponent prefix, ITextComponent suf, double minVal, double maxVal, double currentVal, boolean showDec, boolean drawStr, IPressable handler) {
		super(xPos, yPos, width, height, prefix, suf, minVal, maxVal, currentVal, showDec, drawStr, handler);
	}

	public CustomSlider(int xPos, int yPos, int width, int height, ITextComponent prefix, ITextComponent suf, double minVal, double maxVal, double currentVal, boolean showDec, boolean drawStr, IPressable handler, @Nullable ISlider par) {
		super(xPos, yPos, width, height, prefix, suf, minVal, maxVal, currentVal, showDec, drawStr, handler, par);
	}

	public CustomSlider(int xPos, int yPos, ITextComponent displayStr, double minVal, double maxVal, double currentVal, IPressable handler, ISlider par) {
		super(xPos, yPos, displayStr, minVal, maxVal, currentVal, handler, par);
	}

	@Override
	protected void renderBg(MatrixStack mStack, Minecraft par1Minecraft, int par2, int par3)
	{
		if (this.visible)
		{
			if (this.dragging)
			{
				this.sliderValue = (par2 - (this.x + 4)) / (float)(this.width - 8);
				updateSlider();
			}

			int uvY = isHovered ? 86 : 66;

			GuiUtils.drawContinuousTexturedBox(mStack, WIDGETS_LOCATION, this.x + (int)(this.sliderValue * (float)(this.width - 8)), this.y, 0, uvY, 8, this.height, 200, 20, 2, 3, 2, 2, this.getBlitOffset());
		}
	}
}
