package com.spyeedy.idkmod.client.gui.widget;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import com.spyeedy.idkmod.client.ClientUtils;
import com.spyeedy.idkmod.client.gui.widget.button.TinyButton;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.ITextProperties;

import java.awt.*;
import java.util.Collections;
import java.util.List;

public class TextVerticalScrollPanel extends CustomScrollPanel {

	private final Color textColor;
	private final boolean textShadow;
	private List<ITextProperties> textList;

	private int scrollY = 0;

	private Button upArrowBtn;
	private Button downArrowBtn;

	public TextVerticalScrollPanel(int x, int y, int width, int height, List<ITextComponent> textList) {
		this(x, y, width, height, textList, Color.WHITE, true);
	}

	public TextVerticalScrollPanel(int x, int y, int width, int height, List<ITextComponent> textList, Color textColor, boolean textShadow) {
		super(x, y, width, height);
		this.textColor = textColor;
		this.textShadow = textShadow;
		this.textList = ClientUtils.makeWrappedText(textList, width);

		this.addListener(this.upArrowBtn = new TinyButton(x + width + 3, y + 1 + height/2 - 11, TinyButton.Icon.UP_ARROW, (button) -> {
			this.scrollY -= getScrollAmount();
			applyScrollLimits();
		}));

		this.addListener(this.downArrowBtn = new TinyButton(x + width + 3, y + 1 + height/2, TinyButton.Icon.DOWN_ARROW, (button) -> {
			this.scrollY += getScrollAmount();
			applyScrollLimits();
		}));
	}

	@Override
	public void tick() {
		super.tick();

		if (!this.visible || this.textList.size() == 0) return;

		this.upArrowBtn.active = canScroll() && scrollY > 0;
		this.downArrowBtn.active = canScroll() && scrollY < getContentLength() - height;
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		if (!this.visible || this.textList.size() == 0) return;

		double scale = ClientUtils.minecraft.getWindow().getGuiScale();
		RenderSystem.pushMatrix();
		RenderSystem.enableScissor((int) ((x + 1) * scale), (int)(ClientUtils.minecraft.getWindow().getHeight() - ((y + 1 + height) * scale)),
				(int) (width * scale), (int) (height * scale));

		int y = this.y + 1 - this.scrollY;
		for (ITextProperties line : this.textList) {
			ClientUtils.renderTextPropertyText(matrixStack, x + 1, y, line, textColor.getRGB(), textShadow);
			y += textShadow ? 10 : 9;
		}

		RenderSystem.disableScissor();
		RenderSystem.popMatrix();

		super.render(matrixStack, mouseX, mouseY, partialTicks);
	}

	@Override
	public boolean mouseScrolled(double mouseX, double mouseY, double delta) {
		if (!this.visible || this.textList.size() == 0) return false;

		if (canScroll() && mouseX >= x && mouseX < x + width && mouseY >= y && mouseY < y + height) {
			this.scrollY += -delta * getScrollAmount();
			applyScrollLimits();
		}
		return super.mouseScrolled(mouseX, mouseY, delta);
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) {
		if (!this.visible || this.textList.size() == 0) return false;
		return super.mouseClicked(mouseX, mouseY, button);
	}

	@Override
	protected boolean canScroll() {
		return getContentLength() > height;
	}


	@Override
	protected int getContentLength() {
		return textList.size() * 8 + (textList.size() - 1) * (textShadow ? 2 : 1); // the max height of the text is 8 in minecraft, then we add text padding to separate the lines
	}

	@Override
	protected int getScrollAmount() {
		return 20;
	}

	@Override
	protected void applyScrollLimits() {
		if (scrollY <= 0) scrollY = 0;
		else if (scrollY >= getContentLength() - height) scrollY = getContentLength() - height;
	}

	public void setTextList(ITextComponent textList) {
		setTextList(Collections.singletonList(textList));
	}

	public void setTextList(List<ITextComponent> textList) {
		this.scrollY = 0;
		this.textList = ClientUtils.makeWrappedText(textList, width);

		this.upArrowBtn.y = y + 1 + height/2 - 11;
		this.downArrowBtn.y = y + 1 + height/2;
	}
}
