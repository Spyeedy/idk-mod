package com.spyeedy.idkmod.client.gui.widget;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import com.spyeedy.idkmod.client.ClientUtils;
import com.spyeedy.idkmod.client.gui.widget.button.IdkModButton;
import com.spyeedy.idkmod.client.gui.widget.button.TinyButton;
import com.spyeedy.idkmod.common.capability.journal.QuestReader;
import com.spyeedy.idkmod.common.capability.journal.objectives.Objective;
import org.lwjgl.glfw.GLFW;

import javax.annotation.Nullable;
import java.awt.*;
import java.util.List;

public class ObjectiveListScrollPanel extends CustomScrollPanel {

	private String questId;
	private int stepIdx;
	private List<Objective> objectives;

	private boolean isScrollingX = false;
	private int scrollX = 0;
	private int scrollY = 0;
	private int contentWidth;

	private TinyButton upArrowBtn;
	private TinyButton downArrowBtn;
	private TinyButton leftArrowBtn;
	private TinyButton rightArrowBtn;

	public ObjectiveListScrollPanel(int x, int y, int width, int height, @Nullable String questId, int stepIdx, List<Objective> objectives) {
		super(x, y, width, height);
		this.questId = questId;
		this.stepIdx = stepIdx;
		this.objectives = objectives;

		this.addListener(this.upArrowBtn = new TinyButton(x + width + 1, y + height/2 - 11, TinyButton.Icon.UP_ARROW, (button) -> {
			this.scrollY -= getScrollAmount();
			this.applyScrollLimits();
		}));

		this.addListener(this.downArrowBtn = new TinyButton(x + width + 1, y + height/2, TinyButton.Icon.DOWN_ARROW, (button) -> {
			this.scrollY += getScrollAmount();
			this.applyScrollLimits();
		}));

		this.addListener(this.leftArrowBtn = new TinyButton(x + width/2 - 11, y + height + 1, TinyButton.Icon.LEFT_ARROW, (button) -> {
			this.scrollX -= getScrollAmount();
			this.applyScrollLimits();
		}));

		this.addListener(this.rightArrowBtn = new TinyButton(x + width/2, y + height + 1, TinyButton.Icon.RIGHT_ARROW, (button) -> {
			this.scrollX += getScrollAmount();
			this.applyScrollLimits();
		}));

		int longestWidth = 0;
		if (questId != null) {
			for (int i = 0; i < objectives.size(); i++) {
				Objective objective = objectives.get(i);
				int descWidth = ClientUtils.fontRenderer.width(QuestReader.Utils.formatObjectiveDescription(objective, true, questId, stepIdx, i));

				if (descWidth > longestWidth)
					longestWidth = descWidth;
			}
		}
		this.contentWidth = longestWidth + 6; // + 6 because of our status icon
	}

	@Override
	public void tick() {
		super.tick();

		this.upArrowBtn.active = canScroll() && this.scrollY > 0;
		this.downArrowBtn.active = canScroll() && this.scrollY < getContentLength() - height;

		this.leftArrowBtn.active = canScrollX() && this.scrollX > 0;
		this.rightArrowBtn.active = canScrollX() && this.scrollX < contentWidth - width;
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		if (!visible || this.objectives.size() == 0) return;

		int xOffset = this.x - scrollX;
		int yOffset = this.y - scrollY;

		double scale = ClientUtils.minecraft.getWindow().getGuiScale();
		RenderSystem.pushMatrix();
		RenderSystem.enableScissor((int) (x * scale), (int) (ClientUtils.minecraft.getWindow().getHeight() - ((y + height) * scale)),
				(int) (width * scale), (int) (height * scale));

		for (int i = 0; i < objectives.size(); i++) {
			Objective objective = objectives.get(i);
			minecraft.getTextureManager().bind(IdkModButton.TEX);
			this.blit(matrixStack, xOffset, yOffset + 2, 21, objective.isComplete() ? 4 : 0, 4, 4); // text height 8, 4 for status height, 2 for offset

			minecraft.font.draw(matrixStack, QuestReader.Utils.formatObjectiveDescription(objective, true, questId, stepIdx, i), xOffset + 6, yOffset, Color.GRAY.getRGB());

			yOffset += 12; // 8 for text height, 4 for line height
		}
		RenderSystem.disableScissor();
		RenderSystem.popMatrix();

		super.render(matrixStack, mouseX, mouseY, partialTicks);
	}

	@Override
	public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
		if (visible && (keyCode == GLFW.GLFW_KEY_LEFT_SHIFT || keyCode == GLFW.GLFW_KEY_RIGHT_SHIFT)) {
			this.isScrollingX = true;
			return true;
		}
		return super.keyPressed(keyCode, scanCode, modifiers);
	}

	@Override
	public boolean keyReleased(int keyCode, int scanCode, int modifiers) {
		if (this.isScrollingX && visible && (keyCode == GLFW.GLFW_KEY_LEFT_SHIFT || keyCode == GLFW.GLFW_KEY_RIGHT_SHIFT)) {
			this.isScrollingX = false;
			return true;
		}
		return super.keyReleased(keyCode, scanCode, modifiers);
	}

	@Override
	public boolean mouseScrolled(double mouseX, double mouseY, double delta) {
		if (visible && mouseX >= this.x && mouseX < this.x + width && mouseY >= this.y && mouseY < this.y + height) {
			if ((isScrollingX || !canScroll()) && canScrollX())
				this.scrollX += -delta * getScrollAmount();
			else if (!isScrollingX && canScroll())
				this.scrollY += -delta * getScrollAmount();
			applyScrollLimits();

			return true;
		}
		return super.mouseScrolled(mouseX, mouseY, delta);
	}

	// for scrollY
	@Override
	protected boolean canScroll() {
		return getContentLength() > height;
	}

	protected boolean canScrollX() {
		return contentWidth > width;
	}

	@Override
	protected int getScrollAmount() {
		return 20;
	}

	@Override
	protected int getContentLength() {
		return this.objectives.size() * 8 + (this.objectives.size() - 1) * 4; // 8 for text height, 4 for line height
	}

	@Override
	protected void applyScrollLimits() {
		if (scrollY <= 0) scrollY = 0;
		else if (scrollY + height > getContentLength()) scrollY = getContentLength() - height;

		if (scrollX <= 0) scrollX = 0;
		else if (scrollX + width > contentWidth) scrollX = contentWidth - width;
	}

	public void setObjectives(String questId, int stepIdx, List<Objective> objectives) {
		this.scrollY = this.scrollX = 0;

		this.questId = questId;
		this.stepIdx = stepIdx;
		this.objectives = objectives;

		int longestWidth = 0;
		for (int i = 0; i < objectives.size(); i++) {
			Objective objective = objectives.get(i);
			int descWidth = ClientUtils.fontRenderer.width(QuestReader.Utils.formatObjectiveDescription(objective, true, questId, stepIdx, i));

			if (descWidth > longestWidth)
				longestWidth = descWidth;
		}
		this.contentWidth = longestWidth + 6; // + 6 because of our status icon
	}
}
