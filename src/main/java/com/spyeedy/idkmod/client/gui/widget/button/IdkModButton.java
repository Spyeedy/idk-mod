package com.spyeedy.idkmod.client.gui.widget.button;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.spyeedy.idkmod.client.ClientUtils;
import com.spyeedy.idkmod.common.IdkMod;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;

public class IdkModButton extends Button {

	public ITextComponent toolTipMsg;
	private boolean ifInactiveShouldTooltip = false;
	protected static final Minecraft minecraft = Minecraft.getInstance();
	public static final ResourceLocation TEX = new ResourceLocation(IdkMod.MODID, "textures/gui/widgets.png");

	public IdkModButton(int x, int y, int width, int height, ITextComponent title, IPressable pressedAction) {
		super(x, y, width, height, title, pressedAction);
	}

	public IdkModButton(int x, int y, int width, int height, ITextComponent title, ITextComponent toolTipMsg, IPressable pressedAction) {
		super(x, y, width, height, title, pressedAction, (button, mStack, mouseX, mouseY) -> {
			if (button instanceof IdkModButton) ClientUtils.renderTooltip(mStack, ((IdkModButton) button).toolTipMsg, mouseX, mouseY);
		});
		this.toolTipMsg = toolTipMsg;
	}

	@Override
	public void renderButton(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		renderBg(matrixStack, minecraft, mouseX, mouseY);

		drawCenteredString(matrixStack, Minecraft.getInstance().font, this.getMessage(), this.x + this.width / 2, this.y + (this.height - 8) / 2, getFGColor() | MathHelper.ceil(this.alpha * 255.0F) << 24);

		if (this.isHovered() && (this.active || ifInactiveShouldTooltip)) {
			this.renderToolTip(matrixStack, mouseX, mouseY);
		}
	}

	@Override
	protected void renderBg(MatrixStack matrixStack, Minecraft minecraft, int mouseX, int mouseY) {
		minecraft.getTextureManager().bind(WIDGETS_LOCATION);
		int i = this.getYImage(this.isHovered());
		
		int uvY = 46 + i * 20;

		int halfWidth = this.width/2;
		int halfHeight = this.height/2;

		this.blit(matrixStack, this.x, this.y, 0, uvY, halfWidth, halfHeight); // top left
		this.blit(matrixStack, this.x + halfWidth, this.y, 200 - halfWidth, uvY, halfWidth, halfHeight); // top right
		this.blit(matrixStack, this.x, this.y + halfHeight, 0, uvY + 20 - halfHeight, halfWidth, halfHeight); // bottom left
		this.blit(matrixStack, this.x + halfWidth, this.y + halfHeight, 200 - halfWidth, uvY + 20 - halfHeight, halfWidth, halfHeight); // bottom right
	}

	public IdkModButton setIfInactiveShouldTooltip(boolean ifInactiveShouldTooltip) {
		this.ifInactiveShouldTooltip = ifInactiveShouldTooltip;

		return this;
	}
}
