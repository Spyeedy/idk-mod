package com.spyeedy.idkmod.client.gui.widget;

public abstract class CustomScrollPanel extends CustomWidget {

	public int x;
	public int y;
	public int width;
	public int height;

	public CustomScrollPanel(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	@Override
	public boolean mouseScrolled(double mouseX, double mouseY, double delta) {
		if (!visible) return false;
		return super.mouseScrolled(mouseX, mouseY, delta);
	}

	protected abstract boolean canScroll();
	protected abstract int getScrollAmount();
	protected abstract int getContentLength();
	protected abstract void applyScrollLimits();
}
