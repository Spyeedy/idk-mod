package com.spyeedy.idkmod.client.gui.details;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.spyeedy.idkmod.client.gui.QuestMakerScreen;
import com.spyeedy.idkmod.common.capability.journal.QuestReader;
import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.capability.journal.Quest;
import net.minecraft.client.gui.chat.NarratorChatListener;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;

import java.awt.*;
import java.util.Arrays;

public class SaveQuestScreen extends DetailsScreen {

	private final Quest quest;
	private TextFieldWidget fileNameTextField;

	public SaveQuestScreen(QuestMakerScreen parent, Quest quest) {
		super(parent, new TranslationTextComponent(IdkMod.MODID + ".quest_save"), true, true);
		this.quest = quest;
	}

	@Override
	protected void init() {
		this.ySize = 132;

		super.init();

		int x = (this.width - xSize) / 2;
		int y = (this.height - ySize) / 2;

		this.fileNameTextField = new TextFieldWidget(minecraft.font, x + 8, y + 92, 118, 12, NarratorChatListener.NO_TITLE);
		this.addWidget(fileNameTextField);

		if (quest.getTitle() != null)
			this.nameTextField.setValue(quest.getTitle());

		if (quest.getDescription() != null && quest.getDescription().size() > 0)
			this.descTextField.setValue(quest.getDescription().get(0).getContents());

	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		super.render(matrixStack, mouseX, mouseY, partialTicks);

		int x = (this.width - xSize) / 2;
		int y = (this.height - ySize) / 2;

		if (this.fileNameTextField != null) {
			drawString(matrixStack, minecraft.font, new TranslationTextComponent(IdkMod.MODID + ".string.file_name"), x + 7, y + 79, Color.WHITE.getRGB());
			this.fileNameTextField.render(matrixStack, mouseX, mouseY, partialTicks);
		}
	}

	@Override
	public void onBtnSave(Button button) {
		if (this.fileNameTextField.getValue().isEmpty()) {
			this.fileNameTextField.setFocus(true);
			return;
		}
		QuestReader.writeQuest(quest.setTitle(this.nameTextField.getValue()).setDescription(Arrays.asList(new StringTextComponent(this.descTextField.getValue()))), this.fileNameTextField.getValue());
		this.parent.quest = quest;

		super.onBtnSave(button);
	}
}
