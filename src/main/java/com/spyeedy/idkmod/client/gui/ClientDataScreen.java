package com.spyeedy.idkmod.client.gui;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.spyeedy.idkmod.client.gui.widget.CustomSlider;
import com.spyeedy.idkmod.client.gui.widget.button.CustomCheckboxButton;
import com.spyeedy.idkmod.client.gui.widget.button.IdkModButton;
import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.network.PacketHandler;
import com.spyeedy.idkmod.common.network.SaveClientDataBooleanMessage;
import com.spyeedy.idkmod.common.network.SaveClientDataColorMessage;
import com.spyeedy.idkmod.common.tileentity.ClientDataTileEntity;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.client.gui.widget.Slider;

import java.awt.*;

public class ClientDataScreen extends Screen {

	private static final ResourceLocation TEX = new ResourceLocation(IdkMod.MODID, "textures/gui/client_data.png");
	private static final int XSIZE = 176;
	private static final int YSIZE = 166;

	private ClientDataTileEntity tileEntity = null;
	private final int[] oldValues = new int[4];
	private int red;
	private int green;
	private int blue;
	private int alpha;
	private boolean shouldHideColorBox;

	private IdkModButton btnSave;
	public ClientDataScreen(BlockPos pos) {
		super(new TranslationTextComponent("screen." + IdkMod.MODID + ".client_data"));

		if (Minecraft.getInstance().level.getBlockEntity(pos) instanceof ClientDataTileEntity) {
			this.tileEntity = (ClientDataTileEntity) Minecraft.getInstance().level.getBlockEntity(pos);

			this.red = tileEntity.red;
			this.green = tileEntity.green;
			this.blue = tileEntity.blue;
			this.alpha = tileEntity.alpha;
			this.shouldHideColorBox = tileEntity.shouldHideColorBox;

			this.oldValues[0] = red;
			this.oldValues[1] = green;
			this.oldValues[2] = blue;
			this.oldValues[3] = alpha;
		}
	}

	@Override
	protected void init() {
		super.init();

		int x = (this.width - XSIZE)/2;
		int y = (this.height - YSIZE)/2;

		this.addButton(new CustomSlider(x + 7, y + 7, 120, 16, new TranslationTextComponent(IdkMod.MODID + ".string.red").append(": "), new StringTextComponent(""), 0, 255, this.red, false, true, (button) -> {}, (slider) -> this.red = slider.getValueInt()));
		this.addButton(new CustomSlider(x + 7, y + 27, 120, 16, new TranslationTextComponent(IdkMod.MODID + ".string.green").append(": "), new StringTextComponent(""), 0, 255, this.green, false, true, (button) -> {}, (slider) -> this.green = slider.getValueInt()));
		this.addButton(new CustomSlider(x + 7, y + 47, 120, 16, new TranslationTextComponent(IdkMod.MODID + ".string.blue").append(": "), new StringTextComponent(""), 0, 255, this.blue, false, true, (button) -> {}, (slider) -> this.blue = slider.getValueInt()));
		this.addButton(new CustomSlider(x + 7, y + 67, 120, 16, new TranslationTextComponent(IdkMod.MODID + ".string.alpha").append(": "), new StringTextComponent(""), 0, 255, this.alpha, false, true, (button) -> {}, (slider) -> this.alpha = slider.getValueInt()));

		ITextComponent saveText = new TranslationTextComponent(IdkMod.MODID + ".string.save");
		int width = font.width(saveText);
		this.addButton(this.btnSave = new IdkModButton(x + 7, y + 87, width + 8, 16, saveText, (button) -> {
			if (tileEntity != null) {
				PacketHandler.NETWORK.sendToServer(new SaveClientDataColorMessage(tileEntity.getBlockPos(), red, green, blue, alpha));

				this.oldValues[0] = red;
				this.oldValues[1] = green;
				this.oldValues[2] = blue;
				this.oldValues[3] = alpha;
			}
		}));

		this.addButton(new CustomCheckboxButton(x + 7, y + YSIZE - 24, new TranslationTextComponent(IdkMod.MODID + ".string.hide_light_box"), this.shouldHideColorBox, button -> {
			if (tileEntity != null)
				PacketHandler.NETWORK.sendToServer(new SaveClientDataBooleanMessage(tileEntity.getBlockPos(), shouldHideColorBox = ((CustomCheckboxButton)button).isChecked()));
		}));
	}

	@Override
	public void tick() {
		super.tick();

		if (tileEntity == null || this.btnSave == null) return;

		boolean isRedSame = this.red == this.oldValues[0];
		boolean isGreenSame = this.green == this.oldValues[1];
		boolean isBlueSame = this.blue == this.oldValues[2];
		boolean isAlphaSame = this.alpha == this.oldValues[3];

		this.btnSave.active = !(isRedSame && isGreenSame && isBlueSame && isAlphaSame);
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		this.renderBackground(matrixStack);

		int x = (this.width - XSIZE)/2;
		int y = (this.height - YSIZE)/2;

		minecraft.getTextureManager().bind(TEX);
		this.blit(matrixStack, x, y, 0, 0, XSIZE, YSIZE);

		super.render(matrixStack, mouseX, mouseY, partialTicks);

		/* Draw color preview */
		AbstractGui.fill(matrixStack, x + 131, y + 7, x + XSIZE - 7, y + 63, Color.BLACK.getRGB()); // BORDER. minX: 7 + 120 + 4 = 131, maxY: 47 + 16
		AbstractGui.fill(matrixStack, x + 132, y + 8, x + XSIZE - 8, y + 62, new Color(red, green, blue).getRGB()); // minX: 7 + 120 + 5 = 132, maxY: 47 + 16
	}

	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int button) {
		if (this.getFocused() instanceof Slider) return getFocused().mouseReleased(mouseX, mouseY, button);

		return super.mouseReleased(mouseX, mouseY, button);
	}

	@Override
	public boolean isPauseScreen() {
		return false;
	}
}
