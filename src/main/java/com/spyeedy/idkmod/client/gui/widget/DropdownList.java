package com.spyeedy.idkmod.client.gui.widget;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.spyeedy.idkmod.client.gui.widget.button.IdkModButton;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;

import java.awt.*;
import java.util.List;

public class DropdownList extends IdkModButton {

	private List<ITextComponent> items;
	public boolean expanded;
	public int selected = 0;

	private IPressable entryClickAction;

	private int scrollY = 0; // position of scrollbar
	private int scrollDistance = 0; // position of scrolled contents

	// Width is dependent on the string width. Width: 19 ( includes the left-right border and the button) + 5 (space for scrollbar)
	public DropdownList(int x, int y, List<ITextComponent> items, IPressable entryClickAction) {
		super(x, y, 24, 14, StringTextComponent.EMPTY, (button) -> {
			if (button instanceof DropdownList)
				((DropdownList) button).expanded = !((DropdownList) button).expanded;
		});
		this.items = items;
		this.entryClickAction = entryClickAction;

		int longestText = 0;
		for (ITextComponent text : items) {
			if (minecraft.font.width(text) > longestText) longestText = minecraft.font.width(text);
		}
		this.width += longestText;
	}

	@Override
	public void renderButton(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		int buttonX = this.x + getTextbox();

		// Drawing the first box, where selected/first item text is displayed
		Screen.fill(matrixStack, this.x, this.y, this.x + getTextbox(), this.y + height, (isHovered() || expanded) ? Color.WHITE.getRGB() : Color.BLACK.getRGB()); // draw black border
		Screen.fill(matrixStack, this.x + 1, this.y + 1, this.x + getTextbox() - 1, this.y + height - 1, Color.GRAY.getRGB()); // draw gray background

		int i = (this.isHovered() || expanded) ? 1 : 0;
		minecraft.getTextureManager().bind(TEX);
		this.blit(matrixStack, buttonX, this.y, 70, 18 + i * this.height, 13, 14); // draw button

		minecraft.font.draw(matrixStack, items.get(selected), this.x + 3, this.y + 3, Color.WHITE.getRGB()); // selected text

		if (!expanded) return;

		renderScroll(matrixStack, mouseX, mouseY, this.y + height - scrollDistance);
	}

	private void renderScroll(MatrixStack matrixStack, int mouseX, int mouseY, int baseY) {
		for (int idx = 0; idx < items.size(); idx++) {
			int color = Color.BLACK.getRGB();
			int yOffset = baseY + idx * 13;

			// We don't want to render items out of the scroll area
			if (yOffset < this.y + height || yOffset >= this.y + height + getVisibleHeight()) continue;

			Screen.fill(matrixStack, this.x, yOffset, this.x + getTextbox(), yOffset + 13, color); // draw black border
			Screen.fill(matrixStack, this.x + 1, yOffset, this.x + getTextbox() - 1, yOffset + 12, this.selected == idx ? 0xFF8892c9 : Color.GRAY.getRGB()); // draw gray background

			minecraft.font.draw(matrixStack, items.get(idx), this.x + 3, yOffset + 2, Color.WHITE.getRGB());
		}

		// ---- SCROLL BAR ----
		if (this.items.size() <= 3) return;

		int backgroundHeight = getVisibleHeight() - 1;
		int scrollbarX = this.x + getTextbox() - 6; // 6: 1 (right border) + 5 (scrollbar width)

		// Draw Scrollbar
		Screen.fill(matrixStack, scrollbarX, y + height, scrollbarX + 5, y + height + backgroundHeight, Color.DARK_GRAY.getRGB()); // draw dark gray background for scrollbar to move about. maxY: 3 * 13, 3 is only allow 3 entries at any time, and 13 is entry height, we subtract 1 because of the bottom border
		Screen.fill(matrixStack, scrollbarX, y + height + scrollY, scrollbarX + 5, y + height + scrollY + getScrollBarHeight(), Color.LIGHT_GRAY.getRGB());
	}

	@Override
	public boolean mouseScrolled(double mouseX, double mouseY, double delta) {
		if (this.expanded && this.items.size() > 3 && mouseX >= this.x && mouseX <= this.x + getTextbox() && mouseY >= this.y + this.height && mouseY <= this.y + this.height + getContentHeight()) {
			// delta is negative if scrolling down, and is positive if scrolling up

			scrollDistance += -delta * 13;
			scrollY += -delta * Math.round(1f/items.size() * (getVisibleHeight() - 1)); // We move by a multiplier of 1/itemSize x scrollbar background length
			applyScrollLimits();

			return true;
		}
		return false;
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) {
		if (this.active && this.visible) {
			if (this.isValidClickButton(button)) { // ensure it's left button that is clicking
				boolean isMouseInBtnArea = this.clicked(mouseX, mouseY);

				if (this.expanded) {
					if (mouseX >= x && mouseX <= x + getTextbox() && mouseY >= y + height && mouseY <= y + height + getVisibleHeight()) { // check is mouse is within boundary area
						for (int i = 0; i < 3; i++) { // we check if the mouse clicked on a visible item
							int yOffset = this.y + this.height + 13 * i; // item has 13 height

							if (mouseX >= this.x + 1 && mouseX < this.x + getTextbox() - 1 - 5 && mouseY >= yOffset && mouseY <= yOffset + 12) { // for mouseY <= yOffset + 12, 12 is the height of the text background, we want to make sure mouse is clicking ONLY on text background
								int itemY = yOffset - (this.y + this.height - scrollDistance); // relative from 0 to i * 13
								int itemIdx = itemY / 13; // we divide by 13 (item height), to get the index
								this.selected = itemIdx;
								this.expanded = false;

								this.entryClickAction.onPress(this);

								return true;
							}
						}
					} else if (!isMouseInBtnArea) { // if out of button's click area, we close expanded, to "lose focus"
						 this.expanded = false;
						 return true;
					 }
				}
			}
		}
		return super.mouseClicked(mouseX, mouseY, button);
	}

	private void applyScrollLimits() {
		int max = getVisibleHeight() - 1; // since our scrollbar ends 1 pixel before bottom border, we have to minus 1
		if (scrollY + getScrollBarHeight() > max) { // if scrollbar is out of the scroll area, we move it back to proper place
			scrollY = max - getScrollBarHeight();
			scrollDistance = getContentHeight() - getVisibleHeight();
		} else if (scrollY < 0) { // if scrollbar is out of the scroll area, we move it forward to top of the scroll area
			scrollY = 0;
			scrollDistance = 0;
		}

		if (scrollDistance == 0) scrollY = 0;
		else if (scrollDistance == getContentHeight() - getVisibleHeight()) scrollY = max - getScrollBarHeight();
	}

	private int getVisibleHeight() {
		return 3 * 13;
	}

	private int getScrollBarHeight() {
		return Math.round((3f/items.size() * (getVisibleHeight() - 1))); // 3 * 13 to get the visible height, and -1 to remove a pixel for the border
	}

	private int getContentHeight() {
		return items.size() * 13;
	}

	private int getTextbox() {
		return this.width - 13; // 13 is the width of the button
	}
}
