package com.spyeedy.idkmod.client.gui.widget;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import com.spyeedy.idkmod.client.ClientUtils;
import com.spyeedy.idkmod.client.gui.QuestBoardScreen;
import com.spyeedy.idkmod.client.gui.QuestJournalScreen;
import com.spyeedy.idkmod.client.gui.widget.button.IdkModButton;
import com.spyeedy.idkmod.client.gui.widget.button.UpDownArrowButton;
import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.capability.journal.CapabilityJournal;
import com.spyeedy.idkmod.common.capability.journal.Quest;
import com.spyeedy.idkmod.common.capability.journal.QuestReader;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.ITextProperties;
import net.minecraft.util.text.TranslationTextComponent;

import java.awt.*;
import java.util.List;

public class QuestListScrollPanel extends CustomScrollPanel {

	private int scrollingDistance; // scrolling distance represents invisible entries above the x variable.
	private Button upArrowBtn;
	private Button downArrowBtn;

	private int tickCounter = -1;

	private int selectedIdx = -1;
	private List<Quest> questList;
	private final IQuestListEntrySelect questListEntrySelect;

	public QuestListScrollPanel(int width, int height, int x, int y, List<Quest> questList, IQuestListEntrySelect questListEntrySelect) {
		super(x, y, width, height);
		this.questList = questList;
		this.questListEntrySelect = questListEntrySelect;

		this.addListener(this.upArrowBtn = new UpDownArrowButton(x + width/2 - 4, y, true, p_onPress_1_ -> {
			scrollingDistance -= getScrollAmount();
			applyScrollLimits();
		}));
		this.addListener(this.downArrowBtn = new UpDownArrowButton(x + width/2 - 4, y + height + 8, false, p_onPress_1_ -> {
			scrollingDistance += getScrollAmount();
			applyScrollLimits();
		}));
	}

	@Override
	public void tick() {
		super.tick();
		this.upArrowBtn.active = canScroll() && this.scrollingDistance > 0;
		this.downArrowBtn.active = canScroll() && scrollingDistance + height < getContentLength();

		if (questList.size() > 0 && (this.upArrowBtn.active || this.downArrowBtn.active)) {
			tickCounter++;
			if (tickCounter > 40) { // 20 ticks per second, so 2s * 20
				tickCounter = 0; // reset to 0
			}
		}

		int delta = 0;
		if (tickCounter == 40 || tickCounter == 20) delta = -1;
		else if (tickCounter == 10 || tickCounter == 30) delta = 1;

		if (this.upArrowBtn.active) this.upArrowBtn.y += delta;
		if (this.downArrowBtn.active) this.downArrowBtn.y += -delta;
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		if (!visible) return;

		if (this.questList.size() > 0) {
			double scale = ClientUtils.minecraft.getWindow().getGuiScale();
			RenderSystem.pushMatrix();
			RenderSystem.enableScissor((int) (x * scale), (int) (ClientUtils.minecraft.getWindow().getHeight() - ((y + 7 + height) * scale)),
					(int) (width * scale), (int) (height * scale));

			int questIdx = 0;
			for (Quest quest : questList) {
				int entryY = this.y + 7 - this.scrollingDistance + questIdx * getEntryHeight();
				boolean isHovering = (mouseY >= this.y + 7 && mouseY < this.y + 7 + height // we check if mouseY is in visible area
						&& mouseX >= x && mouseX <= x + width && mouseY >= entryY && mouseY < entryY + getEntryHeight()) || selectedIdx == questIdx;

				// entry background
				boolean isEven = width % 2 == 0;
				int halfWidth = (int) Math.floor(width / 2);
				minecraft.getTextureManager().bind(QuestBoardScreen.TEX);
				this.blit(matrixStack, this.x, entryY, 0, QuestBoardScreen.ySize + (isHovering ? getEntryHeight() : 0), halfWidth, getEntryHeight()); // render left side, length will always be even
				this.blit(matrixStack, this.x + halfWidth, entryY, 118 - width / 2 - (isEven ? 0 : 1), QuestBoardScreen.ySize + (isHovering ? getEntryHeight() : 0), width / 2 + (isEven ? 0 : 1), getEntryHeight()); // render right side, if there's odd, length will always be odd, otherwise even

				boolean isCurrScreenJournal = minecraft.screen instanceof QuestJournalScreen;

				// quest title
				minecraft.font.draw(matrixStack, ClientUtils.getEllipsisString(QuestReader.Utils.getQuestTitle(quest.getId()), width - 6 - (isCurrScreenJournal ? 6 : 0)), this.x + 3, entryY + 3, isHovering ? Color.BLACK.getRGB() : Color.WHITE.getRGB());

				// tracked status
				if (isCurrScreenJournal) { // draw tracked status only for quest journal
					ClientPlayerEntity playerEntity = minecraft.player;
					if (playerEntity != null) {
						playerEntity.getCapability(CapabilityJournal.JOURNAL).ifPresent(iJournal -> {
							boolean isTracked = iJournal.getTrackedQuest() != null && iJournal.getTrackedQuest().getId().equals(quest.getId());

							minecraft.getTextureManager().bind(IdkModButton.TEX);
							this.blit(matrixStack, this.x + width - 7, entryY + 5, 21, isTracked ? 4 : 0, 4, 4); // text height 8, 4 for status height, 2 for offset
						});
					}
				}

				questIdx++;
			}

			RenderSystem.disableScissor();
			RenderSystem.popMatrix();
		} else {
			ITextComponent text = new TranslationTextComponent(IdkMod.MODID + ".quest_list.no_quests");
			List<ITextProperties> lines = ClientUtils.makeWrappedText(text, width);
			int linesHeight = lines.size() * 8 + (lines.size() - 1);

			int lineOffset = y + 7 + height/2 - linesHeight/2;
			for (int i = 0; i < lines.size(); i++) {
				if (i > 0) i += 1;

				ITextProperties line = lines.get(i);
				int textWidth = ClientUtils.fontRenderer.width(line);
				ClientUtils.renderTextPropertyText(matrixStack, x + width/2 - textWidth/2, lineOffset, line, Color.DARK_GRAY.getRGB(), false); // text height is 8, so divide by 2 is 4

				lineOffset += 8;
			}
		}

		super.render(matrixStack, mouseX, mouseY, partialTicks);
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) {
		if (questList.size() == 0 || !visible) return super.mouseClicked(mouseX, mouseY, button);

		int baseY = this.y + 7;

		if (mouseY >= baseY && mouseY <= baseY + height && mouseX >= x && mouseX < x + width) { // we check if in visible area
			int maxRows = 6; // max number of rows visible
			float division = (float) scrollingDistance/getEntryHeight();
			int scrolledRows = (int) Math.floor(division); // get the number of rows scrolled before this
			if (division % 1 == 0) { // if there's no remainder, means scrollingDistance is a multiple of entryHeight
				for (int i = 0; i < maxRows; i++) {
					int y = baseY + getEntryHeight() * i;
					if (mouseY >= y && mouseY < y + getEntryHeight()) {
						int resultIdx = scrolledRows + i;

						if (resultIdx < questList.size()) {
							this.selectedIdx = resultIdx;
							this.questListEntrySelect.onEntrySelect(selectedIdx);

							return true;
						}
					}
				}
			} else { // since there's a remainder, the first visible entry is partially visible.
				int y = baseY;
				int firstRowHiddenHeight = scrollingDistance - (scrolledRows*getEntryHeight());
				int firstRowVisibleHeight = getEntryHeight() - firstRowHiddenHeight;

				if (mouseY >= y && mouseY < y + firstRowVisibleHeight) { // check for first row
					if (scrolledRows < questList.size()) {
						this.selectedIdx = scrolledRows;
						this.questListEntrySelect.onEntrySelect(selectedIdx);

						return true;
					}
				} else {
					y += firstRowVisibleHeight;
					maxRows -= 1; // we minus 1, since the first row was checked for

					for (int i = 0; i < maxRows; i++) {
						if (mouseY >= y && mouseY < y + getEntryHeight()) {
							int resultIdx = scrolledRows + i + 1; // we add 1, since first row was checked

							if (resultIdx < questList.size()) {
								this.selectedIdx = resultIdx;
								this.questListEntrySelect.onEntrySelect(selectedIdx);

								return true;
							}
						}
						y += getEntryHeight();
					}
				}
			}
		}
		return super.mouseClicked(mouseX, mouseY, button);
	}

	@Override
	public boolean mouseScrolled(double mouseX, double mouseY, double delta) {
		if (visible && canScroll() && this.questList.size() > 0) {
			if (mouseX >= this.x && mouseX <= this.x + width && mouseY >= this.y && mouseY <= this.y + height + 12) { // 12 because of the buttons
				scrollingDistance += -delta * getScrollAmount();
				applyScrollLimits();

				return true;
			}
		}
		return super.mouseScrolled(mouseX, mouseY, delta);
	}

	@Override
	protected int getScrollAmount() {
		return getEntryHeight();
	}

	@Override
	protected boolean canScroll() {
		return getContentLength() > height;
	}

	public int getEntryHeight() {
		return 14;
	}

	@Override
	protected int getContentLength() {
		return questList.size() * getEntryHeight();
	}

	@Override
	protected void applyScrollLimits() {
		if (scrollingDistance <= 0) scrollingDistance = 0;
		else if (scrollingDistance + height >= getContentLength()) scrollingDistance = getContentLength() - height; // we add up to get the bottom of the last entry, if it's more or equals to the content height, we know we have to set the scrollingDistance to the proper bottom position
	}

	public void setQuestList(List<Quest> questList) {
		this.selectedIdx = -1;
		this.questList = questList;
	}

	public interface IQuestListEntrySelect {
		void onEntrySelect(int selectedIdx);
	}
}
