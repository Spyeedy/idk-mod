package com.spyeedy.idkmod.client.gui.widget;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.spyeedy.idkmod.client.ClientUtils;
import com.spyeedy.idkmod.client.gui.QuestMakerScreen;
import com.spyeedy.idkmod.client.gui.widget.button.UpDownArrowButton;
import com.spyeedy.idkmod.common.capability.journal.QuestReader;
import com.spyeedy.idkmod.common.capability.journal.Step;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.renderer.Tessellator;
import net.minecraftforge.client.gui.ScrollPanel;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

import javax.annotation.Nullable;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class StepsScrollPanel extends ScrollPanel {

	@Nullable
	public SelectedEntryType selectedEntryType = null;
	public int selected = -1;
	public int selectedObjParent = -1;

	private static final int stepHeight = 14;
	private static final int objectiveHeight = 10;

	private final QuestMakerScreen parent;

	public StepsScrollPanel(QuestMakerScreen parent, int top, int left) {
		super(parent.getMinecraft(), 151, 150, top, left);
		this.parent = parent;
	}

	@Override
	protected int getContentHeight() {
		int total = parent.quest.getStepSize() * stepHeight; // We sum up for the steps' height

		for (int i = 0; i < this.parent.questArrowButtonList.size(); i++) {
			UpDownArrowButton btn = this.parent.questArrowButtonList.get(i);
			Step step = this.parent.quest.getStep(i);

			if (btn.isExpanded() && step.getObjectiveCount() > 0) {
				total += step.getObjectiveCount() * objectiveHeight;
			}
		}

		return total;
	}

	@Override
	protected void drawPanel(MatrixStack mStack, int entryRight, int relativeY, Tessellator tess, int mouseX, int mouseY) {
		FontRenderer fontRenderer = ClientUtils.fontRenderer;
		String test = "Test 123 abc 456 def6789 ghi 012 jkl";

		int rootY = relativeY - 2; // to move the y upwards, so that it's 2 pixels below the border
		int rootX = left + 2; // minus width is to set the rootX at the left, then we add 2 for the margin space

		int stepY = rootY;
		for (int i = 0; i < this.parent.quest.getStepSize(); i++) {
			Step step = this.parent.quest.getStep(i);
			boolean isStepSelected = i == selected && selectedEntryType == SelectedEntryType.STEP;

			ClientUtils.minecraft.getTextureManager().bind(QuestMakerScreen.TEX);
			blit(mStack, rootX, stepY, 0, QuestMakerScreen.ySize + (isStepSelected ? stepHeight : 0), 141, stepHeight);

			ClientUtils.fontRenderer.drawShadow(mStack, ClientUtils.getEllipsisString(step.getDescription(), 114), rootX + 3, stepY + 3, Color.WHITE.getRGB());

			if (this.parent.questArrowButtonList.size() > i) {
				Button arrowBtn = this.parent.questArrowButtonList.get(i);
				arrowBtn.x = rootX + 130;
				arrowBtn.y = stepY + 4;

				arrowBtn.render(mStack, mouseX, mouseY, 0);
			}

			stepY += stepHeight; // to set y for objective, or if no objectives, for the next step

			if (this.parent.questArrowButtonList.get(i).isExpanded() && step.getObjectiveCount() > 0) {
				for (int j = 0; j < step.getObjectiveCount(); j++) {
					boolean isObjectiveSelected = j == selected && selectedEntryType == SelectedEntryType.OBJECTIVE && selectedObjParent == i;
					ClientUtils.minecraft.getTextureManager().bind(QuestMakerScreen.TEX);

					// We add 10 to rootX to move the objective texture to the right. We add 40 to ySize to go to objective texture area
					blit(mStack, rootX + 10, stepY, 0, QuestMakerScreen.ySize + 28 + (isObjectiveSelected ? objectiveHeight : 0), 131, objectiveHeight);

					boolean unicodeFlag = ClientUtils.minecraft.isEnforceUnicode();
					Method setUnicodeFlag = ObfuscationReflectionHelper.findMethod(Minecraft.class, "func_238209_b_", boolean.class);

					try {
						setUnicodeFlag.invoke(ClientUtils.minecraft, true);
					} catch (IllegalAccessException | InvocationTargetException e) {
						e.printStackTrace();
					}

					ClientUtils.fontRenderer.drawShadow(mStack, ClientUtils.getEllipsisString(QuestReader.Utils.formatObjectiveDescription(step.getObjective(j)), 125), rootX + 12, stepY, Color.WHITE.getRGB());

					try {
						setUnicodeFlag.invoke(ClientUtils.minecraft, unicodeFlag);
					} catch (IllegalAccessException | InvocationTargetException e) {
						e.printStackTrace();
					}

					// After drawing, we add to stepY, objectiveHeight, to move the integer below the drawn objective pixel
					stepY += objectiveHeight;
				}
			}
		}
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) {
		if (mouseX >= this.left && mouseX <= this.right && mouseY >= this.top && mouseY <= this.bottom) { // check if is within boundaries of the scroll panel
			int baseY = this.top + 2 - (int)this.scrollDistance; // baseY taken from ScrollPanel#render. 2 instead of "border", since "border" is final and set to 4, but in our scroll panel, our border is 2
			int baseX = this.left + 2;

			int stepY = baseY;

			for (int stepIdx = 0; stepIdx < this.parent.quest.getStepSize(); stepIdx++) {
				UpDownArrowButton btn = this.parent.questArrowButtonList.get(stepIdx);

				if (mouseX >= btn.x && mouseX <= btn.x + btn.getWidth() && mouseY >= btn.y && mouseY <= btn.y + btn.getHeight()) { // Check if the mouse is hovering over the arrow button
					btn.onPress();
					return true;
				} else if (mouseX >= baseX && mouseX <= baseX + 141 // 141 is the width of the step background texture
						&& mouseY >= stepY && mouseY <= stepY + stepHeight) { // Check if the mouse is hovering over the step entry
					this.selected = stepIdx;
					this.selectedEntryType = SelectedEntryType.STEP;

					return true;
				}

				// To offset stepY for the next step
				stepY += stepHeight;
				Step step = this.parent.quest.getStep(stepIdx);

				if (btn.isExpanded() && step.getObjectiveCount() > 0) { // To add-on objectives height if the arrow button is showing the objectives
					for (int objIdx = 0; objIdx < step.getObjectiveCount(); objIdx++) {
						if (mouseX >= baseX + 10 && mouseX <= baseX + 141 && mouseY >= stepY && mouseY <= stepY + objectiveHeight) { // Check if mouse is hovering over objective entry
							this.selectedEntryType = SelectedEntryType.OBJECTIVE;
							this.selected = objIdx;
							this.selectedObjParent = stepIdx;
							return true;
						}

						stepY += objectiveHeight;
					}
				}
			}
		}
		return super.mouseClicked(mouseX, mouseY, button);
	}

	@Override
	public boolean mouseScrolled(double mouseX, double mouseY, double scroll) {
		if (getContentHeight() < height) return false; // we prevent any scrolling if the content height is lesser than the panel height
		return super.mouseScrolled(mouseX, mouseY, scroll);
	}

	@Override
	public boolean mouseDragged(double mouseX, double mouseY, int button, double deltaX, double deltaY) {
		if (getContentHeight() < height) return false; // we prevent any scrolling if the content height is lesser than the panel height
		return super.mouseDragged(mouseX, mouseY, button, deltaX, deltaY);
	}

	// Copied from ScrollPanel#applyScrollLimits
	public void readjustScrollBar() {
		this.scrollDistance = 0.0F;
	}

	public enum SelectedEntryType {
		STEP, OBJECTIVE;
	}
}