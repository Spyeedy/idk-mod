package com.spyeedy.idkmod.client.gui;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.spyeedy.idkmod.client.gui.details.DetailsScreen;
import com.spyeedy.idkmod.client.gui.widget.ItemStackScrollPanel;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.StringTextComponent;

public class SearchAllItemsScreen extends DetailsScreen {

	private ItemStackScrollPanel itemStackScroll;
	private ItemStack selectedItemStack = ItemStack.EMPTY;

	public SearchAllItemsScreen(QuestMakerScreen parent) {
		super(parent, new StringTextComponent("Search All Items"), false, false);
	}

	@Override
	protected void init() {
		this.ySize = 150;
		super.init();

		int x = (this.width - xSize)/2;
		int y = (this.height - ySize)/2;

		this.itemStackScroll = new ItemStackScrollPanel(this, x + 7, y + 19, 6, 3, stack -> this.selectedItemStack = stack);
	}

	@Override
	public void tick() {
		super.tick();

		this.itemStackScroll.tick();
	}

	@Override
	protected void renderExtra(MatrixStack matrixStack, int mouseX, int mouseY, int x, int y, float partialTicks) {
		int j = y + 90;

		this.minecraft.getTextureManager().bind(DNASequencerScreen.TEX_GUI);
		this.blit(matrixStack, x + 7, j, 34, 8, 18, 18);

		if (!this.selectedItemStack.isEmpty()) {
			this.itemRenderer.renderAndDecorateItem(selectedItemStack, x + 8, j + 1);

			if (mouseX >= x + 8 && mouseX <= x + 24 && mouseY >= j && mouseY <= j + 18) {
				renderTooltip(matrixStack, selectedItemStack, mouseX, mouseY);
				fill(matrixStack, x + 8, j + 1, x + 24, j + 17, 0x80FFFFFF); // renders translucent white square to show is hovering
			}
		}

		if (this.itemStackScroll != null)
			this.itemStackScroll.render(matrixStack, mouseX, mouseY, partialTicks);
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) {
		if (this.itemStackScroll != null)
			return this.itemStackScroll.mouseClicked(mouseX, mouseY, button);
		return super.mouseClicked(mouseX, mouseY, button);
	}

	@Override
	public boolean mouseScrolled(double mouseX, double mouseY, double delta) {
		if (this.itemStackScroll != null)
			return this.itemStackScroll.mouseScrolled(mouseX, mouseY, delta);
		return super.mouseScrolled(mouseX, mouseY, delta);
	}
}
