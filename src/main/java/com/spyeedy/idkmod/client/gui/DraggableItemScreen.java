package com.spyeedy.idkmod.client.gui;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.spyeedy.idkmod.common.IdkMod;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.chat.NarratorChatListener;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.potion.Effect;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.client.gui.ScrollPanel;

import java.util.ArrayList;

public class DraggableItemScreen extends Screen {

	public static final ResourceLocation TEX = new ResourceLocation(IdkMod.MODID, "textures/gui/draggable_item.png");

	private final int xSize = 183;
	private final int ySize = 74;
	private ItemsScrollPanel itemsPanel;

	private ArrayList<ActiveItem> activeItems = new ArrayList<>();
	private ArrayList<DraggableItem> draggableItems = new ArrayList<>();
	private DraggableItem heldDraggableItem = null;

	public DraggableItemScreen() {
		super(NarratorChatListener.NO_TITLE);

		int idx = 0;
		for (Effect effect : Registry.MOB_EFFECT) {
			this.draggableItems.add(new DraggableItem(effect.getRegistryName().getPath(), idx));
			idx += 1;
		}

		this.activeItems.add(new ActiveItem(0, 0));
		this.activeItems.add(new ActiveItem(1, 0));
		this.activeItems.add(new ActiveItem(0, 1));
		this.activeItems.add(new ActiveItem(1, 1));
	}

	@Override
	public void tick() {
		super.tick();

		this.itemsPanel.tick();
	}

	@Override
	protected void init() {
		super.init();

		int i = (this.width - this.xSize)/2;
		int j = (this.height - this.ySize)/2;

		this.itemsPanel = new ItemsScrollPanel(this.getMinecraft(),j + 9, i + 62, this);
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		super.render(matrixStack, mouseX, mouseY, partialTicks);

		int i = (this.width - this.xSize)/2;
		int j = (this.height - this.ySize)/2;

		this.minecraft.getTextureManager().bind(TEX);
		this.blit(matrixStack, i, j, 0, 0, xSize, ySize);

		for (ActiveItem activeItem : activeItems)
			activeItem.draw(this, matrixStack, mouseX, mouseY);

		if (this.itemsPanel != null)
			this.itemsPanel.render(matrixStack, mouseX, mouseY, partialTicks);

		if (this.heldDraggableItem != null)
			this.heldDraggableItem.drawDraggableItem(this, matrixStack, mouseX, mouseY);

		for (DraggableItem draggableItem : draggableItems) {
			// guiPanelY check, is to make sure the hover is within the panel's boundaries. Then the next check with i and j, is for the slots
			if (mouseY >= j + 9 && mouseY <= j + 9 + 62 && mouseX >= draggableItem.x && mouseX <= draggableItem.x + 24 && mouseY >= draggableItem.y && mouseY <= draggableItem.y + 24)
				draggableItem.drawHoverText(this, matrixStack, mouseX, mouseY);
		}
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) {
		if (this.itemsPanel != null && this.itemsPanel.mouseClicked(mouseX, mouseY, button)) return true;

		for (ActiveItem activeItem : activeItems)
			if (activeItem.mouseClicked(this, mouseX, mouseY, button)) return true;

		return false;
	}

	@Override
	public boolean mouseDragged(double mouseX, double mouseY, int button, double dragX, double dragY) {
		return this.itemsPanel != null && this.itemsPanel.mouseDragged(mouseX, mouseY, button, dragX, dragY);
	}

	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int button) {
		if (this.itemsPanel != null && this.itemsPanel.mouseReleased(mouseX, mouseY, button)) return true;

		if (this.heldDraggableItem != null)
			for (ActiveItem activeItem : activeItems)
				if (activeItem.mouseReleased(this, mouseX, mouseY, button)) return true;

		this.heldDraggableItem = null;

		return false;
	}

	@Override
	public boolean mouseScrolled(double mouseX, double mouseY, double delta) {
		return this.itemsPanel != null && this.itemsPanel.mouseScrolled(mouseX, mouseY, delta);
	}

	private class ItemsScrollPanel extends ScrollPanel {

		private DraggableItemScreen screen;
		public float scrollDist;
		public int top;
		public int left;

		public ItemsScrollPanel(Minecraft client, int top, int left, DraggableItemScreen screen) {
			super(client, 112, 56, top, left);
			this.screen = screen;

			this.scrollDist = this.scrollDistance;
			this.top = top;
			this.left = left;
		}

		public void tick() {
			this.scrollDist = this.scrollDistance;
		}

		@Override
		protected int getContentHeight() {
			int listSize = screen.draggableItems.size();
			int extra = ((listSize % 4) > 0) ? 1 : 0; // We add 1 if there is a remainder, because when we cast int, it rounds down
			int multipler = Math.round(listSize / 4) + extra;

			return (multipler * 24) + (multipler > 1 ? (multipler - 1) * 2 : 0); // first multiplier is for the height of the icons, second multiplier is for the gaps
		}

		@Override
		protected void drawPanel(MatrixStack mStack, int entryRight, int relativeY, Tessellator tess, int mouseX, int mouseY) {
			for (DraggableItem draggableItem : draggableItems) {
				draggableItem.draw(screen, mStack, mouseX, mouseY, relativeY);
			}
		}

		@Override
		public boolean mouseClicked(double mouseX, double mouseY, int button) {
			if (super.mouseClicked(mouseX, mouseY, button))
				return true;

			for (DraggableItem draggableItem : draggableItems) {
				if (draggableItem.mouseClicked(screen, this, mouseX, mouseY, button)) return true;
			}

			return false;
		}
	}

	private class DraggableItem {
		private static final String TEX_PATH = "textures/mob_effect/";
		private final ResourceLocation TEX;

		private String name;
		private int index;

		public int x;
		public int y;

		public DraggableItem(String name, int idx) {
			this.name = name;
			this.TEX = new ResourceLocation(TEX_PATH + name + ".png");
			this.index = idx;
		}

		public void draw(DraggableItemScreen screen, MatrixStack matrixStack, int mouseX, int mouseY, int relativeY) {
			this.x = (screen.width - screen.xSize)/2 + 62 + 2 + (index % 4) * 26;
			this.y = relativeY - 2 + (index/4) * 26;

			drawDraggableItem(screen, matrixStack, x, y);
		}

		public void drawDraggableItem(DraggableItemScreen screen, MatrixStack matrixStack, int x, int y) {
			screen.minecraft.getTextureManager().bind(DraggableItemScreen.TEX);
			screen.blit(matrixStack, x, y, xSize + 23, 0, 24, 24);

			drawDraggableIcon(screen, matrixStack, x + 3, y + 3);
		}

		public void drawDraggableIcon(DraggableItemScreen screen, MatrixStack matrixStack, int x, int y) {
			screen.minecraft.getTextureManager().bind(TEX);
			Screen.blit(matrixStack, x, y, 0, 0, 18, 18, 18, 18);
		}

		public boolean mouseClicked(DraggableItemScreen screen, ItemsScrollPanel panel, double mouseX, double mouseY, int button) {
			if (button == 0 && mouseY >= panel.top && mouseY <= panel.top + 62 && mouseX >= x && mouseX <= x + 24 && mouseY >= y && mouseY <= y + 24) {
				screen.heldDraggableItem = this;

				return true;
			}

			return false;
		}

		public void drawHoverText(DraggableItemScreen screen, MatrixStack matrixStack, int mouseX, int mouseY) {
			screen.renderTooltip(matrixStack, new TranslationTextComponent("effect.minecraft." + name), mouseX - 4, mouseY);
		}
	}

	private class ActiveItem {
		public DraggableItem draggableItem = null;
		private final int x;
		private final int y;
		private static final int slotLength = 22;

		public ActiveItem(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public void draw(DraggableItemScreen screen, MatrixStack matrixStack, int mouseX, int mouseY) {
			int i = (screen.width - screen.xSize)/2 + 7 + (this.x * 28); // 1st magic number is the gap from left to the first button pos, 2nd magic number is the offset to other buttons
			int j = (screen.height - screen.ySize)/2 + 12 + (this.y * 28);

			screen.minecraft.getTextureManager().bind(DraggableItemScreen.TEX);
			screen.blit(matrixStack, i, j, xSize, draggableItem != null ? slotLength : 0, slotLength, slotLength);

			if (draggableItem != null) {
				draggableItem.drawDraggableIcon(screen, matrixStack, i + 2, j + 2); // offset from button to place the icon in the center of the button
				if (mouseX >= i && mouseX <= i + slotLength && mouseY >= j && mouseY <= j + slotLength) {
					draggableItem.drawHoverText(screen, matrixStack, mouseX, mouseY);
				}
			}
		}

		public boolean mouseReleased(DraggableItemScreen screen, double mouseX, double mouseY, int button) {
			int i = (screen.width - screen.xSize)/2 + 3 + (this.x * 30) + 4;
			int j = (screen.height - screen.ySize)/2 + 8 + (this.y * 30) + 4;

			if (button == 0 && mouseX >= i && mouseX <= i + slotLength && mouseY >= j && mouseY <= j + slotLength) {
				this.draggableItem = screen.heldDraggableItem;
				screen.heldDraggableItem = null;
				return true;
			}

			return false;
		}

		public boolean mouseClicked(DraggableItemScreen screen, double mouseX, double mouseY, int button) {
			int i = (screen.width - screen.xSize)/2 + 3 + (this.x * 30) + 6;
			int j = (screen.height - screen.ySize)/2 + 8 + (this.y * 30) + 6;

			// button: 0 is left click, 1 is right click, 2 is middle click
			if (button == 1 && mouseX >= i && mouseX <= i + slotLength && mouseY >= j && mouseY <= j + slotLength) {
				this.draggableItem = null;
				return true;
			}

			return false;
		}
	}
}
