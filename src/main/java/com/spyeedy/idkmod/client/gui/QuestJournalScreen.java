package com.spyeedy.idkmod.client.gui;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.spyeedy.idkmod.client.ClientUtils;
import com.spyeedy.idkmod.client.gui.widget.ObjectiveListScrollPanel;
import com.spyeedy.idkmod.client.gui.widget.QuestListScrollPanel;
import com.spyeedy.idkmod.client.gui.widget.TextVerticalScrollPanel;
import com.spyeedy.idkmod.client.gui.widget.button.IdkModButton;
import com.spyeedy.idkmod.client.gui.widget.button.TinyButton;
import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.capability.journal.CapabilityJournal;
import com.spyeedy.idkmod.common.capability.journal.Quest;
import com.spyeedy.idkmod.common.capability.journal.QuestReader;
import com.spyeedy.idkmod.common.network.DropQuestMessage;
import com.spyeedy.idkmod.common.network.PacketHandler;
import com.spyeedy.idkmod.common.network.SetTrackedQuestMessage;
import net.minecraft.client.gui.IGuiEventListener;
import net.minecraft.client.gui.IRenderable;
import net.minecraft.client.gui.chat.NarratorChatListener;
import net.minecraft.client.gui.screen.IScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.*;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class QuestJournalScreen extends Screen {

	private int xSize = 291;
	private int ySize = 180;
	private boolean showCompleted = false; // if true, show completed, else show ongoing
	private int rightPageContent = 0; // 0 = quest, 1 = objectives
	private int currentStep = 0;

	private Quest quest = null;
	private List<ITextProperties> titleLines = null;

	// Components
	private QuestListScrollPanel questListScrollPanel;
	private TextVerticalScrollPanel descScrollPanel;
	private TinyButton stepsLeftArrowBtn;
	private TinyButton stepsRightArrowBtn;
	private TextVerticalScrollPanel stepDescScrollPanel;
	private ObjectiveListScrollPanel objectiveListScrollPanel;
	private Button trackButton;
	private Button dropButton;

	// Text
	private static final ITextComponent completedText = new TranslationTextComponent(IdkMod.MODID + ".string.completed");
	private static final ITextComponent ongoingText = new TranslationTextComponent(IdkMod.MODID + ".string.ongoing");
	private static final ITextComponent questText = new TranslationTextComponent(IdkMod.MODID + ".string.quest");
	private static final ITextComponent objectivesText = new TranslationTextComponent(IdkMod.MODID + ".string.objectives");
	private static final ITextComponent trackText = new TranslationTextComponent(IdkMod.MODID + ".string.track");
	private static final ITextComponent untrackText = new TranslationTextComponent(IdkMod.MODID + ".string.untrack");
	private static final int completedWidth = ClientUtils.fontRenderer.width(completedText);
	private final int ongoingWidth = ClientUtils.fontRenderer.width(ongoingText);
	private final int objectivesWidth = ClientUtils.fontRenderer.width(objectivesText);

	public QuestJournalScreen() {
		super(NarratorChatListener.NO_TITLE);
	}

	@Override
	protected void init() {
		super.init();

		int x = (this.width - xSize)/2;
		int y = (this.height - ySize)/2;

		this.quest = null;
		this.titleLines = null;
		this.currentStep = 0;
		this.rightPageContent = 0;

		// 146 - 7*2 = 132. 7 is the btn height in scroll panel
		this.minecraft.player.getCapability(CapabilityJournal.JOURNAL).ifPresent(iJournal -> {
			this.addWidget(this.questListScrollPanel = new QuestListScrollPanel(113, 132, x + 16, y + 12, this.showCompleted ? iJournal.getCompletedQuests() : iJournal.getOngoingQuests(), (quest1) -> {
				// Assign selected quest data
				if (this.showCompleted) {
					this.quest = iJournal.getCompletedQuests().get(quest1);
				} else {
					this.quest = iJournal.getOngoingQuests().get(quest1);
				}

				// Set Quest Title
				this.titleLines = ClientUtils.makeWrappedText(new StringTextComponent(QuestReader.Utils.getQuestTitle(this.quest.getId())).withStyle(TextFormatting.BOLD), 113);

				// Set Quest Description Scroll Panel
				int titleHeight = getTitleHeight(titleLines);
				this.descScrollPanel.y = y + 12 + titleHeight + 3;
				this.descScrollPanel.height = getDescVisibleHeight(titleHeight);
				this.descScrollPanel.setTextList(QuestReader.Utils.getQuestDesc(this.quest.getId()));

				// Set data for "Objectives" page
				if (this.quest.getStepSize() > 0) {
					// Set Step Description text
					ITextComponent stepDesc = QuestReader.Utils.getStepDesc(this.quest.getId(), this.currentStep = 0);
					if (stepDesc.getContents().equals(""))
						stepDesc = new TranslationTextComponent(IdkMod.MODID + ".step.no_desc");

					this.stepDescScrollPanel.setTextList(stepDesc);
					this.objectiveListScrollPanel.setObjectives(this.quest.getId(), this.currentStep, this.quest.getStep(currentStep).getObjectives());
				}
			}));
		});

		int titleHeight = getTitleHeight(titleLines);
		this.addWidget(this.descScrollPanel = new TextVerticalScrollPanel(x + 160, y + 12 + titleHeight + 3, 103, getDescVisibleHeight(titleHeight), new ArrayList<>(), Color.GRAY, false));

		this.addButton(this.stepsRightArrowBtn = new TinyButton(x + xSize - 15 - 10, y + 12, TinyButton.Icon.RIGHT_ARROW, (buttton) -> {})); // -15 to offset from the right end of the page, and -10 for the button width
		this.addButton(this.stepsLeftArrowBtn = new TinyButton(x + xSize - 15 - 21, y + 12, TinyButton.Icon.LEFT_ARROW, (buttton) -> {})); // -21 because of the 2 buttons' width + a pixel to separate the buttons
		this.addWidget(this.stepDescScrollPanel = new TextVerticalScrollPanel(x + 160, y + 25, 103, 38, new ArrayList<>(), Color.GRAY, false)); // 145 + 15 = 160, not 16 because the text renders +1 for x and y. 12 + 10 + 3
		this.addWidget(this.objectiveListScrollPanel = new ObjectiveListScrollPanel(x + 160, y + 68 + 8 + 3, 103, 78, null, 0, new ArrayList<>())); // 68 is the objectives text, 8 is the height of the text, 3 is offset from bottom text to top of this panel

		this.addButton(this.trackButton = new IdkModButton(x + 83, y + ySize - 12 - 16, ClientUtils.fontRenderer.width(trackText) + 8, 16, trackText, (button) -> minecraft.player.getCapability(CapabilityJournal.JOURNAL).ifPresent(iJournal -> {
			if (iJournal.getTrackedQuest() == null || !iJournal.getTrackedQuest().getId().equals(quest.getId())) // if there is no untracked quest or selected quest is not the tracked quest, we track the quest
				PacketHandler.NETWORK.sendToServer(new SetTrackedQuestMessage(quest.getId(), true));
			else
				PacketHandler.NETWORK.sendToServer(new SetTrackedQuestMessage(false));
		}))); // default will be track

		ITextComponent dropText = new TranslationTextComponent(IdkMod.MODID + ".string.drop");
		this.addButton(this.dropButton = new IdkModButton(x + 24, y + ySize - 12 - 16, ClientUtils.fontRenderer.width(dropText) + 8, 16, dropText, (button) -> {
			if (this.quest != null) PacketHandler.NETWORK.sendToServer(new DropQuestMessage(this.quest.getId()));
		}));
	}

	@Override
	public void tick() {
		super.tick();

		this.descScrollPanel.visible = this.rightPageContent == 0;
		this.objectiveListScrollPanel.visible = this.rightPageContent == 1;

		this.stepsLeftArrowBtn.visible = this.quest != null && this.rightPageContent == 1;
		this.stepsRightArrowBtn.visible = this.quest != null && this.rightPageContent == 1;
		this.stepsLeftArrowBtn.active = this.quest != null && this.currentStep > 0;
		this.stepsRightArrowBtn.active = this.quest != null && this.currentStep < this.quest.getStepSize() - 1;
		this.stepDescScrollPanel.visible = this.objectiveListScrollPanel.visible = this.rightPageContent == 1;

		if (quest != null) {
			this.minecraft.player.getCapability(CapabilityJournal.JOURNAL).ifPresent(iJournal -> {
				// Set track/untrack button
				if (iJournal.getTrackedQuest() != null && iJournal.getTrackedQuest().getId().equals(this.quest.getId())) { // if selected quest is tracked, we give player the option to untrack
					this.trackButton.setMessage(untrackText);
					this.trackButton.setWidth(ClientUtils.fontRenderer.width(untrackText) + 8);
				} else { // as long as this quest is not track, no matter if there is already a tracking que st, we give player the option to track this quest or not
					this.trackButton.setMessage(trackText);
					this.trackButton.setWidth(ClientUtils.fontRenderer.width(trackText) + 8);
				}
			});
		}
		this.trackButton.visible = quest != null;
		this.dropButton.visible = quest != null;

		for (IGuiEventListener listener : children())
			if (listener instanceof IScreen)
				((IScreen) listener).tick();
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		int x = (this.width - xSize)/2;
		int y = (this.height - ySize)/2;

		minecraft.getTextureManager().bind(new ResourceLocation("textures/gui/book.png"));
		this.blit(matrixStack, x + 145, y, 20, 1, 146, ySize);

		minecraft.getTextureManager().bind(new ResourceLocation(IdkMod.MODID, "textures/gui/journal.png"));
		this.blit(matrixStack, x, y, 0, 0, 145, ySize);

		/* Draw bookmarks */
		int leftBookmarkX = x - completedWidth - 8; // is the text border on the right, 8 is the bookmark tip width
		int rightBookmarkX = x + xSize + objectivesWidth; // 1 is the border
		int leftBookmarkY = y + 18;
		int rightBookmarkY = leftBookmarkY;

		this.blit(matrixStack, leftBookmarkX, leftBookmarkY, 0, ySize + (!showCompleted ? 12 : 0), 8, 12); // draw tip for ongoing
		this.blit(matrixStack, leftBookmarkX, leftBookmarkY + 14, 0, ySize + (showCompleted ? 12 : 0), 8, 12); // draw tip for completed

		int activeTabOutline = 0xFF3651e1;
		int activeTabBg = 0xFF5c73ec;
		int tabOutline = 0xFF14247b;
		int tabBg = 0xFF2d3e97;

		if (quest != null) {
			this.blit(matrixStack, rightBookmarkX, rightBookmarkY, 9, ySize + (rightPageContent == 0 ? 12 : 0), 8, 12); // draw tip for quest
			this.blit(matrixStack, rightBookmarkX, rightBookmarkY + 14, 9, ySize + (rightPageContent == 1 ? 12 : 0), 8, 12); // draw tip for objectives

			int baseX = x + xSize;
			fill(matrixStack, baseX, rightBookmarkY, rightBookmarkX, rightBookmarkY + 12, rightPageContent == 0 ? activeTabOutline : tabOutline); // draw outline for quest
			fill(matrixStack, baseX, rightBookmarkY + 1, rightBookmarkX, rightBookmarkY + 11, rightPageContent == 0 ? activeTabBg : tabBg); // draw outline for quest
			font.draw(matrixStack, questText, baseX + 1, rightBookmarkY + 2, Color.WHITE.getRGB());

			rightBookmarkY += 14;
			fill(matrixStack, baseX, rightBookmarkY, rightBookmarkX, rightBookmarkY + 12, rightPageContent == 1 ? activeTabOutline : tabOutline); // draw outline for objectives
			fill(matrixStack, baseX, rightBookmarkY + 1, rightBookmarkX, rightBookmarkY + 11, rightPageContent == 1 ? activeTabBg : tabBg); // draw outline for objectives
			font.draw(matrixStack, objectivesText, baseX + 1, rightBookmarkY + 2, Color.WHITE.getRGB());
		}
		activeTabOutline = 0xFF745a36;
		activeTabBg = 0xFF917142;
		tabOutline = 0xFF2a1f0f;
		tabBg = 0xFF382b18;

		leftBookmarkX += 8; // move from tip to drawing background and text

		fill(matrixStack, leftBookmarkX, leftBookmarkY, x, leftBookmarkY + 12, !showCompleted ? activeTabOutline : tabOutline); // draw outline for ongoing
		fill(matrixStack, leftBookmarkX, leftBookmarkY + 1, x, leftBookmarkY + 11, !showCompleted ? activeTabBg : tabBg); // draw background for ongoing
		font.draw(matrixStack, ongoingText, x - ongoingWidth, leftBookmarkY + 2, Color.WHITE.getRGB()); // draw ongoing text

		leftBookmarkY += 14;

		fill(matrixStack, leftBookmarkX, leftBookmarkY, x, leftBookmarkY + 12, showCompleted ? activeTabOutline : tabOutline); // draw outline for completed
		fill(matrixStack, leftBookmarkX, leftBookmarkY + 1, x, leftBookmarkY + 11, showCompleted ? activeTabBg : tabBg); // draw outline for completed
		font.draw(matrixStack, completedText, x - completedWidth, leftBookmarkY + 2, Color.WHITE.getRGB()); // draw completed text

		if (quest != null) {
			if (rightPageContent == 0)
				renderQuestPage(matrixStack, mouseX, mouseY, x + 145, y); // +145 to offset to starting X for the right side of the book
			else if (rightPageContent == 1)
				renderObjectivesPage(matrixStack, mouseX, mouseY, x + 145, y);
		}

		super.render(matrixStack, mouseX, mouseY, partialTicks);

		for (IGuiEventListener listener : this.children())
			if (!(listener instanceof Button) && listener instanceof IRenderable)
				((IRenderable) listener).render(matrixStack, mouseX, mouseY, partialTicks);
	}

	public void renderQuestPage(MatrixStack matrixStack, int mouseX, int mouseY, int x, int y) {
		int xCenter = x + (145/2); // we add 145/2 to get the centre of the page

		if (titleLines != null) {
			int titleOffset = y + 12;
			for (int i = 0; i < titleLines.size(); i++) {
				if (i > 0) titleOffset += 1; // spacing for new line
				ClientUtils.renderTextPropertyText(matrixStack, x + 16, titleOffset, titleLines.get(i), Color.BLACK.getRGB(), false);

				titleOffset += 8;
			}
		}

		int stackXOffset = xCenter - 9; // -9 for half of the slot width
		int stackYOffset = y + ySize - 22 - 18; // 22 is the bottom offset, 18 is the height of the slot background
		ItemStack rewardStack = QuestReader.Utils.getQuestReward(this.quest.getId());

		minecraft.getTextureManager().bind(DNASequencerScreen.TEX_GUI);
		this.blit(matrixStack, stackXOffset, stackYOffset, 34, 8, 18, 18); // render slot background
		minecraft.getItemRenderer().renderAndDecorateItem(rewardStack, stackXOffset + 1, stackYOffset + 1); // render item
		minecraft.getItemRenderer().renderGuiItemDecorations(minecraft.font, rewardStack, stackXOffset + 1, stackYOffset + 1); // render text

		ITextComponent rewardText = new TranslationTextComponent(IdkMod.MODID + ".string.reward");
		minecraft.font.draw(matrixStack, rewardText, xCenter - font.width(rewardText)/2f, stackYOffset - 12, Color.DARK_GRAY.getRGB());

		if (mouseX >= stackXOffset && mouseX < stackXOffset + 18 && mouseY >= stackYOffset && mouseY < stackYOffset + 18) {
			this.renderWrappedToolTip(matrixStack, this.getTooltipFromItem(rewardStack), mouseX, mouseY, this.minecraft.font);
			Screen.fill(matrixStack, stackXOffset + 1, stackYOffset + 1, stackXOffset + 17, stackYOffset + 17, 0x80FFFFFF); // renders translucent white square to show is hovering
		}
	}

	public void renderObjectivesPage(MatrixStack matrixStack, int mouseX, int mouseY, int x, int y) {
		minecraft.font.draw(matrixStack,
				new TranslationTextComponent(IdkMod.MODID + ".quest.step_count", this.currentStep + 1, quest.getStepSize()).withStyle(TextFormatting.BOLD),
				x + 16, y + 13, Color.BLACK.getRGB()); // 12 is step button, since it has height 10, and text has height 8, we add 1 = 13

		minecraft.font.draw(matrixStack, new TranslationTextComponent(IdkMod.MODID + ".string.objectives").withStyle(TextFormatting.UNDERLINE), x + 16, y + 68, Color.DARK_GRAY.getRGB());
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button) {
		int x = (this.width - xSize)/2;
		int y = (this.height - ySize)/2;

		int ongoingOffset = y + 18;
		int completedOffset = ongoingOffset + 14;

		if (mouseX >= x - completedWidth - 6 && mouseX < x) { // check for left bookmarks
			if (mouseY >= ongoingOffset && mouseY < ongoingOffset + 12 && showCompleted) { // we check for ongoing
				setPropertiesDefault(false);

				return true;
			} else if (mouseY >= completedOffset && mouseY < completedOffset + 12 && !showCompleted) { // we check for completed
				setPropertiesDefault(true);

				return true;
			}
		} else if (mouseX >= x + xSize && mouseX < x + xSize + objectivesWidth + 6) { // 7 = 6 (tip width) + 1 (left text mragin)
			if (mouseY >= ongoingOffset && mouseY < ongoingOffset + 12 && rightPageContent != 0) { // we check for quest, same Y as ongoing
				this.rightPageContent = 0;
			} else if (mouseY >= completedOffset && mouseY < completedOffset + 12 && rightPageContent != 1) { // we check for completed, same Y as completed
				this.rightPageContent = 1;
			}
		}
		return super.mouseClicked(mouseX, mouseY, button);
	}

	@Override
	public boolean mouseScrolled(double mouseX, double mouseY, double delta) {
		for (IGuiEventListener listener : children())
			if (listener.mouseScrolled(mouseX, mouseY, delta)) return true;
		return super.mouseScrolled(mouseX, mouseY, delta);
	}

	@Override
	public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
		for (IGuiEventListener listener : children())
			if (listener.keyPressed(keyCode, scanCode, modifiers)) return true;
		return super.keyPressed(keyCode, scanCode, modifiers);
	}

	@Override
	public boolean keyReleased(int keyCode, int scanCode, int modifiers) {
		for (IGuiEventListener listener : children())
			if (listener.keyReleased(keyCode, scanCode, modifiers)) return true;
		return false;
	}

	@Override
	public boolean isPauseScreen() {
		return false;
	}

	public void setPropertiesDefault(boolean showCompleted) {
		minecraft.player.getCapability(CapabilityJournal.JOURNAL).ifPresent(iJournal -> {
			setPropertiesDefault(showCompleted, showCompleted ? iJournal.getCompletedQuests() : iJournal.getOngoingQuests());
		});
	}

	public void setPropertiesDefault(boolean showCompleted, List<Quest> quests) {
		this.showCompleted = showCompleted;
		this.quest = null;
		this.rightPageContent = 0;
		this.titleLines = null;
		this.descScrollPanel.setTextList(new ArrayList<>());
		this.stepDescScrollPanel.setTextList(new ArrayList<>());
		this.questListScrollPanel.setQuestList(quests);
	}

	public int getTitleHeight(List<ITextProperties> titleLines) {
		if (this.quest == null) return 0;

		return titleLines.size() * 8 + (titleLines.size() - 1); // 8 pixel for each line, and 1 pixel for line spacing in between each line, so maxLine - 1 to get the line spacing
	}

	public int getDescVisibleHeight(int titleHeight) {
		if (this.quest == null) return 0;

		return 124 - 12 - titleHeight - 4;// 158 is the maxY for description panel, 12 is offset from top to title Y, 4 is from between bottom of title to top of description panel
	}
}
