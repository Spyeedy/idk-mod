package com.spyeedy.idkmod.client.gui;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.spyeedy.idkmod.client.gui.widget.QuestListScrollPanel;
import com.spyeedy.idkmod.client.gui.widget.TextHorizontalScrollPanel;
import com.spyeedy.idkmod.client.gui.widget.TextVerticalScrollPanel;
import com.spyeedy.idkmod.client.gui.widget.button.IdkModButton;
import com.spyeedy.idkmod.common.IdkMod;
import com.spyeedy.idkmod.common.capability.journal.CapabilityJournal;
import com.spyeedy.idkmod.common.capability.journal.Quest;
import com.spyeedy.idkmod.common.capability.journal.QuestReader;
import com.spyeedy.idkmod.common.network.PacketHandler;
import com.spyeedy.idkmod.common.network.AcceptQuestMessage;
import net.minecraft.client.gui.IGuiEventListener;
import net.minecraft.client.gui.IRenderable;
import net.minecraft.client.gui.screen.IScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;

import java.awt.*;
import java.util.ArrayList;

public class QuestBoardScreen extends Screen {

	// TODO Quest Board components, scroll panel for available quests to pick (greyed out quests if already picked or completed or marked as non-repeatable, should have title only)
	public static final ResourceLocation TEX = new ResourceLocation(IdkMod.MODID, "textures/gui/quest_board.png");

	private Quest selectedQuest = null;
	private boolean isQuestInJournal = false;

	private int xSize = 256;
	public static final int ySize = 110;

	private TextHorizontalScrollPanel titleScrollPanel;
	private TextVerticalScrollPanel descScrollPanel;
	private IdkModButton acceptBtn;

	public QuestBoardScreen() {
		super(new TranslationTextComponent(IdkMod.MODID + ".quest_board"));
	}

	@Override
	protected void init() {
		super.init();

		int i = (this.width - xSize) / 2;
		int j = (this.height - ySize) / 2;

		this.selectedQuest = null;

		this.addWidget(new QuestListScrollPanel(118, 80, i + 8, j + 8, new ArrayList<>(QuestReader.getQuests().values()), (selectedIdx1) -> {
			this.selectedQuest = (Quest) QuestReader.getQuests().values().toArray()[selectedIdx1];
			this.titleScrollPanel.setTextContent(QuestReader.Utils.getQuestTitle(this.selectedQuest.getId()));
			this.descScrollPanel.setTextList(QuestReader.Utils.getQuestDesc(this.selectedQuest.getId()));
		}));
		this.addWidget(this.titleScrollPanel = new TextHorizontalScrollPanel(i + 131, j + 8, 105, 8, ""));
		this.addWidget(this.descScrollPanel = new TextVerticalScrollPanel(i + 131, j + 34, 105, 46, new ArrayList<>()));

		ITextComponent text = new TranslationTextComponent(IdkMod.MODID + ".string.accept");
		int stringWidth = font.width(text);
		this.addButton(this.acceptBtn = new IdkModButton(i + 249 - stringWidth - 8, j + 87, stringWidth + 8, 16, text, new TranslationTextComponent(IdkMod.MODID + ".quest.can_accept"), (button) -> {
			if (this.selectedQuest != null)
				PacketHandler.NETWORK.sendToServer(new AcceptQuestMessage(this.selectedQuest.getId()));
		}));
	}

	@Override
	public void tick() {
		super.tick();

		for (IGuiEventListener listener : children())
			if (listener instanceof IScreen)
				((IScreen) listener).tick();

		this.isQuestInJournal = false;
		if (this.selectedQuest != null) {
			minecraft.player.getCapability(CapabilityJournal.JOURNAL).ifPresent(iJournal -> {
				for (Quest quest : iJournal.getQuests()) {
					if (quest.getId().equals(selectedQuest.getId())) {
						this.isQuestInJournal = true;
						break;
					}
				}
			});
		}

		this.acceptBtn.active = this.selectedQuest != null && !isQuestInJournal;
		this.acceptBtn.setIfInactiveShouldTooltip(this.selectedQuest != null);

		if (this.acceptBtn.toolTipMsg instanceof TranslationTextComponent) {
			if (!this.isQuestInJournal && ((TranslationTextComponent) this.acceptBtn.toolTipMsg).getKey().contains("cannot_accept")) { // if boolean says no selected quest in journal and tooltip is "cannot_accept"
				this.acceptBtn.toolTipMsg = new TranslationTextComponent(IdkMod.MODID + ".quest.can_accept");
			} else if (this.isQuestInJournal && ((TranslationTextComponent) this.acceptBtn.toolTipMsg).getKey().contains("can_accept")) { // if boolean says selected quest in journal and tooltip is "can_accept"
				this.acceptBtn.toolTipMsg = new TranslationTextComponent(IdkMod.MODID + ".quest.cannot_accept");
			}
		}
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		int x = (this.width - xSize) / 2;
		int y = (this.height - ySize) / 2;

		this.minecraft.getTextureManager().bind(TEX);
		this.blit(matrixStack, x, y, 0, 0, xSize, ySize);

		for (IGuiEventListener listener : children())
			if (!(listener instanceof Button) && listener instanceof IRenderable)
				((IRenderable) listener).render(matrixStack, mouseX, mouseY, partialTicks);

		ITextComponent rewardText = new TranslationTextComponent(IdkMod.MODID + ".string.reward");
		int textX = x + 130;
		int stackYOffset = y + 85; // 22 is the bottom offset, 18 is the height of the slot background

		minecraft.font.drawShadow(matrixStack, rewardText, textX, stackYOffset + 9 - 5, Color.WHITE.getRGB()); // y: +9 to get slot center height, then -5 since text will have shadow

		int stackXOffset = textX + font.width(rewardText) + 4;

		minecraft.getTextureManager().bind(DNASequencerScreen.TEX_GUI);
		this.blit(matrixStack, stackXOffset, stackYOffset, 34, 8, 18, 18); // render slot background

		if (this.selectedQuest == null) return;

		ItemStack rewardStack = QuestReader.Utils.getQuestReward(this.selectedQuest.getId());
		minecraft.getItemRenderer().renderAndDecorateItem(rewardStack, stackXOffset + 1, stackYOffset + 1); // render item
		minecraft.getItemRenderer().renderGuiItemDecorations(minecraft.font, rewardStack, stackXOffset + 1, stackYOffset + 1); // render text
		if (mouseX >= stackXOffset && mouseX < stackXOffset + 18 && mouseY >= stackYOffset && mouseY < stackYOffset + 18) {
			this.renderWrappedToolTip(matrixStack, this.getTooltipFromItem(rewardStack), mouseX, mouseY, this.minecraft.font);
			Screen.fill(matrixStack, stackXOffset + 1, stackYOffset + 1, stackXOffset + 17, stackYOffset + 17, 0x80FFFFFF); // renders translucent white square to show is hovering
		}

		super.render(matrixStack, mouseX, mouseY, partialTicks);
	}

	@Override
	public boolean isPauseScreen() {
		return false;
	}

	@Override
	public boolean mouseScrolled(double mouseX, double mouseY, double delta) {
		for (IGuiEventListener listener : children())
			if (listener.mouseScrolled(mouseX, mouseY, delta)) return true;
		return super.mouseScrolled(mouseX, mouseY, delta);
	}
}