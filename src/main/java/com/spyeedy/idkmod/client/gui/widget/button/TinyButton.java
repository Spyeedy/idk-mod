package com.spyeedy.idkmod.client.gui.widget.button;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.chat.NarratorChatListener;
import net.minecraft.util.text.ITextComponent;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class TinyButton extends IdkModButton {

	@Nullable
	private final Icon icon;
	private static final int uvY = 55;

	public TinyButton(int x, int y, @Nonnull Icon icon, IPressable pressable) {
		super(x, y, 10, 10, NarratorChatListener.NO_TITLE, pressable);
		this.icon = icon;
	}

	public TinyButton(int x, int y, @Nonnull Icon icon, ITextComponent toolTipMsg, IPressable pressedAction) {
		this(x, y, NarratorChatListener.NO_TITLE, icon, toolTipMsg, pressedAction);
	}

	public TinyButton(int x, int y, ITextComponent title, @Nullable Icon icon, ITextComponent toolTipMsg, IPressable pressedAction) {
		super(x, y, 10, 10, title, toolTipMsg, pressedAction);
		this.icon = icon;
	}

	@Override
	protected void renderBg(MatrixStack matrixStack, Minecraft minecraft, int mouseX, int mouseY) {
		minecraft.getTextureManager().bind(TEX);
		this.blit(matrixStack, this.x, this.y, 0, uvY + getYImage(isHovered) * this.height, this.width, this.height);

		if (this.icon != null) {
			this.blit(matrixStack, this.x + 2, this.y + 2, this.width + (this.icon.getX() * 6), uvY + this.icon.getY() * 6, 6, 6);
		}
	}

	public enum Icon {
		ADD_STEP(0, 0), ADD_OBJECTIVE(1, 0), DOWN_ARROW(2, 0), UP_ARROW(3, 0), LEFT_ARROW(4, 0), RIGHT_ARROW(5, 0), CROSS(6, 0), PENCIL(7, 0), SAVE(8, 0);

		private final int x;
		private final int y;

		Icon(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public int getX() {
			return x;
		}

		public int getY() {
			return y;
		}
	}
}
